# Projects of Yari Nys

In this repository, I keep track of (completed) university-related projects together with some personal projects I worked on. Most of the listed projects were realized in a team, their contributions to certain projects can be seen in "contributions". This repository will be regularly updated.

**Note**: in some commits, the author will be Atlas, Leopard__77, or just Yari (Nys). This is because some projects were made with friends or come from GitHub, GroupT GitLab, ... where different usernames had to be used.

## 1. Dashboard_Backend (personal + friends)
The project, made in Spring, communicates with a database to serve data on an API. Furthermore, data is parsed from other APIs and stored in the database.

## 2. Distributed_Applications (GroupT) 
Another project was made in Spring. The goal of this project was to explore the possibilities in Spring. Furthermore, we had to combine these technologies into a project. Other team members included Pieter Touquet & Siebren Michiels.

## 3. EE5_microcontroller (GroupT)
The goal was to create an IoT-related project that monitors a plant. EE5_microcontroller is completely written in C with the espressif framework (based on FreeRTOS). The microcontroller served data on endpoints, took measurements from different sensors, could authenticate users, and could even perform feedback based on measurements (e.g. opening the roof of the enclosure, by using a servo, if it was too hot for the plant). The microcontroller part of the project was completely written by me.

## 4. Machine_Learning (GroupT)
A project that is written in python and with the help of the Keras library. We developed a machine-learning model to recognize traffic signs. Other team members included Hendrik Janter.

## 5.Software_Development (GroupT)
Software development was a course in the third year of my bachelor's. Completely written in C and included multithreading, self-designed buffers, and interaction with SQL database,.... Software development was an individual project at GroupT.

## 6. Game-Launcher (Personal)
A game launcher to launch games from Steam, Epic Games, ... on **Linux**. Comparable with Lutris. It makes use of the Qt framework and is written in C++. (This project is still in development). 

## 7. Arch Linux installer (Personal)
Collection of config files and custom written bash scripts. Made to more easily (re)install Arch Linux with certain desktop environments or window managers such as KDE Plasma and Hyprland.

## 8. Master thesis (in collaboration with Hendrik Janter and Aetherspace)
The title of the thesis was: "Machine Learning Applications in the On-Board Computer of a CubeSat". We used TensorFlow/Keras to develop machine learning models which are able to detect anomalies in the solarpanels of CubeSats. To implement these models on microcontrollers we used X-CUBE-AI, a library developped by STMicroelectronics. Finally, to explore the concept of hardware-software co-design, we implemented one neural network model (autoencoders) on an FPGA by making use of the [HLS4ML](https://fastmachinelearning.org/hls4ml/) library and Vivado+Vitis.

## Miscellaneous
In my free time I like to game and get games working on (Arch) Linux therefore I have experience the following tools:
+ [Wine](https://www.winehq.org/) (a compatibility layer capable of running Windows applications on several POSIX-compliant operating systems, such as Linux,)
+ [DXVK](https://github.com/doitsujin/dxvk) (A Vulkan-based translation layer for Direct3D 9/10/11 which allows running 3D applications on Linux using Wine.)
+ [Wine-GE](https://github.com/GloriousEggroll/wine-ge-custom) (a WINE based on/forked from the most recent bleeding-edge proton experimental wine repo by GloriousEggroll)
+ [VK3D](https://github.com/HansKristian-Work/vkd3d-proton) (vkd3d-proton is a fork of VKD3D, which aims to implement the full Direct3D 12 API on top of Vulkan. The project serves as the development effort for Direct3D 12 support in Proton.)

