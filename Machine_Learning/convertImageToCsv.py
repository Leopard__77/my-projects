import os
import numpy as np
from PIL import Image

dataTrafficSigns = 'DataTrafficSigns.csv'
yLabels = "yLabels.csv"
yBinary = "yBinary.csv"
pathToTrainDir = "./Data/Train/"
# pathToTrainDir = "C:\\\\Users\\Hendrik Janter\\Coursera\\Coursera\\Verkeersborden\\Data\\Train\\"

dataTrafficSignsTest = 'DataTrafficSignsTest.csv'
yLabelsTest = "yLabelsTest.csv"
yLabelsTestBinary = "yLabelsTestBinary.csv"
pathToTestDir = "./Data/Test/"
# pathToTrainDir = "C:\\\\Users\\Hendrik Janter\\Coursera\\Coursera\\Verkeersborden\\Data\\Test\\"
nrOfClasses = 43


def makeCSVData(pathToDataDir, fileNameData, fileNameLabels, fileNameLabelsBinary):
    # remake files
    if os.path.exists(fileNameData):
        os.remove(fileNameData)
    if os.path.exists(fileNameLabels):
        os.remove(fileNameLabels)
    if os.path.exists(fileNameLabelsBinary):
        os.remove(fileNameLabelsBinary)

    labels = []
    for sign in range(43):
        print("traffic sign: " + str(sign))
        for f in os.listdir(pathToDataDir + str(sign)):
            image = Image.open(pathToDataDir + str(sign) + "/" + f, 'r')
            # image = Image.open(pathToTrainDir + str(sign) + "\\" + f,'r')
            image = image.resize((30, 30), Image.ANTIALIAS)
            arr = np.array(image.convert("L"))
            with open(fileNameData, 'a') as csvfile:
                np.savetxt(csvfile, np.column_stack(arr.reshape(-1)), '%5u', delimiter=",")
            labels.append(sign)

    numpyLabels = np.array(labels)
    np.savetxt(fileNameLabels, numpyLabels, '%5u', delimiter=",")
    binaryLabels = np.zeros(shape=(len(labels), 43))
    for x in range(numpyLabels.size):
        binaryLabels[x, labels[x]] = 1
    np.savetxt(fileNameLabelsBinary, binaryLabels, '%5u', delimiter=",")


makeCSVData(pathToTrainDir, dataTrafficSigns, yLabels, yBinary)


def makeCSVTest(pathToTestDir, fileNameDataTest, fileNameLabelsTest, fileNameLabelsTestBinary):
    if os.path.exists(fileNameDataTest):
        os.remove(fileNameDataTest)
    if os.path.exists(fileNameLabelsTest):
        os.remove(fileNameLabelsTest)
    if os.path.exists(fileNameLabelsTestBinary):
        os.remove(fileNameLabelsTestBinary)
    classIds = np.loadtxt("./Data/Test.csv", usecols=6, skiprows=1, delimiter=",", dtype=int)
    fileNames = np.loadtxt("./Data/Test.csv", usecols=7, skiprows=1, delimiter=",", dtype=str)
    yLabels = []

    csvfile = open(fileNameDataTest, 'a')
    for f in os.listdir(pathToTestDir):
        image = Image.open(pathToTestDir + "/" + f, 'r')
        image = image.resize((30, 30), Image.ANTIALIAS)
        arr = np.array(image.convert("L"))
        np.savetxt(csvfile, np.column_stack(arr.reshape(-1)), '%5u', delimiter=",")
        for index in range(fileNames.size):
            if fileNames[index] == ("Test/"+f):
                yLabels.append(classIds[index])
    csvfile.close()
    np.savetxt(fileNameLabelsTest, np.array(yLabels), '%5u', delimiter=",")
    binaryLabels = np.zeros(shape=(len(yLabels), 43))
    for x in range(len(yLabels)):
        binaryLabels[x, yLabels[x]] = 1
    np.savetxt(fileNameLabelsTestBinary, binaryLabels, '%5u', delimiter=",")
makeCSVTest(pathToTestDir, dataTrafficSignsTest, yLabelsTest, yLabelsTestBinary)
