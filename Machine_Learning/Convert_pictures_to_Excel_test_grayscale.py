import os
import xlsxwriter
from PIL import Image

workbook = xlsxwriter.Workbook('DataTrafficSignsTestGrayScale.xlsx', {'constant_memory': True})
workbook.use_zip64()
worksheet = workbook.add_worksheet("TrafficSignsRGBDataGrayScale")

row = 0

for f in os.listdir("C:\\\\Users\\Hendrik Janter\\Coursera\\Coursera\\Verkeersborden\\Data\\Test\\"):
    column = 0
    image = Image.open("C:\\\\Users\\Hendrik Janter\\Coursera\\Coursera\\Verkeersborden\\Data\\Test\\" + f, 'r')
    image = image.resize((30, 30), Image.ANTIALIAS)
    data = list(image.getdata())
    for element in data:
        el = round(element[0]*0.299 + element[1]*0.587 + element[2]*0.114)
        worksheet.write(row, column, el)
        column += 1
    del image
    row += 1

workbook.close()
