Link to dataset: https://www.kaggle.com/meowmeowmeowmeowmeow/gtsrb-german-traffic-sign

# Machine Learning Project

# RGB
| Iterations | Nodes in hidden layer| lambda | Prediction Train |Prediction Test |Time elapsed |
|------------|----------------------|--------|------------------|----------------|-------------|
| 2000       |  1000                | 1      | 98%              | 85%            | +/- 14400s  |
| 500        |  1000                | 0      | 86%              | 74%            | 3313s       |
| 100        |  500                 | 0      | 40%              | 37%            | 318s        |


# Grayscale
| Iterations | Nodes in hidden layer| lambda | Prediction Train |Prediction Test |Time elapsed |
|------------|----------------------|--------|------------------|----------------|-------------|
| 2000         |  1000               | 1     | 98%              | 85%            | 7400s       |
| 500        |  1000                | 0      | 89%              | 76%            | 2075s       |
| 100        |  500                 | 0      | 26%              | 24%            | 137s        |

