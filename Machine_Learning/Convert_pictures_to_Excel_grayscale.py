import os
import xlsxwriter
from PIL import Image

workbook = xlsxwriter.Workbook('DataTrafficSignsGrayScale.xlsx', {'constant_memory': True})
workbook.use_zip64()
worksheet = workbook.add_worksheet("TrafficSignsRGBDataGraysScale")

row = 0

for sign in range(43):
    print("traffic sign: " + str(sign))
    for f in os.listdir("C:\\\\Users\\Hendrik Janter\\Coursera\\Coursera\\Verkeersborden\\Data\\Train\\" + str(sign)):
        worksheet.write(row, 900, sign)
        column = 0
        image = Image.open("C:\\\\Users\\Hendrik Janter\\Coursera\\Coursera\\Verkeersborden\\Data\\Train\\" + str(sign) + "\\" + f, 'r')
        image = image.resize((30, 30), Image.ANTIALIAS)
        data = list(image.getdata())
        for element in data:
            el = round(element[0]*0.299 + element[1]*0.587 + element[2]*0.114)
            worksheet.write(row, column, el)
            column += 1
        del image
        row += 1

workbook.close()
