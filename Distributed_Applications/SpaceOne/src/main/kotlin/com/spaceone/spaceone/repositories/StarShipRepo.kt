package com.spaceone.spaceone.repositories

import com.spaceone.spaceone.entities.items.StarShip

interface StarShipRepo : ItemRepo<StarShip> {

}