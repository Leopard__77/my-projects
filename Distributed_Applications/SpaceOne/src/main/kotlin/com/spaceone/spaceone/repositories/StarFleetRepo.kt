package com.spaceone.spaceone.repositories

import com.spaceone.spaceone.entities.items.StarShip
import com.spaceone.spaceone.entities.persons.StarFleet

interface StarFleetRepo : PersonRepo<StarFleet> {

}