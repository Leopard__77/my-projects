package com.spaceone.spaceone.soap.webservice

import javax.xml.bind.annotation.*


@XmlAccessorType (XmlAccessType.FIELD)
@XmlType(name = "", propOrder = ["name"])
//@XmlRootElement(name = "getCountryRequest")
@XmlRootElement(namespace="http://spring.io/guides/gs-producing-web-service", name="getFlightRequest")
class GetFlightRequest {

    @XmlElement(required = true)
    var name: String = "flight"

}