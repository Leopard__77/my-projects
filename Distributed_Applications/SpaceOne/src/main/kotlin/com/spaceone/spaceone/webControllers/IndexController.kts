package com.spaceone.spaceone.webControllers

import com.spaceone.spaceone.entities.SpaceFlight
import com.spaceone.spaceone.entities.persons.Passenger
import com.spaceone.spaceone.configs.rabbitMQ.Producer
import com.spaceone.spaceone.repositories.SpaceFlightRepo
import com.spaceone.spaceone.security.CustomUserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import org.springframework.web.context.request.WebRequest
import java.text.SimpleDateFormat
import java.util.*
import javax.validation.Valid


@Controller
@RequestMapping("/")
class IndexController {
    @Autowired
    lateinit var userService: CustomUserService

    @Autowired
    lateinit var spaceFlightRepo: SpaceFlightRepo

    @GetMapping("/")
    fun index(model: Model): String {
        println("Date:"+ Date())
        val nextFlightDepar : Date? = spaceFlightRepo.findDistinctTopByDepartureTimeIsGreaterThanOrderByDepartureTime(Date())?.departureTime
        if(nextFlightDepar != null){
            model.addAttribute("endDate", SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(spaceFlightRepo.findDistinctTopByDepartureTimeIsGreaterThanOrderByDepartureTime(Date())?.departureTime.toString()))
        }
        return "index"
    }

    @GetMapping("/flights")
    fun showFlights(model : Model) : String{
        model.addAttribute("flights", spaceFlightRepo.findAllProjectedBy())
        return "flights"
    }

    @GetMapping("/flights/moreInfo")
    fun showMoreInfo(model: Model, @RequestParam(value = "flightId", required = false) flightId: Int) : String{
        val flight : Optional<SpaceFlight> = spaceFlightRepo.findById(flightId)
        model.addAttribute("selectedFlight", flight.get())
        return "moreInfo"
    }

    @GetMapping("/login")
    fun login(request : WebRequest, model : Model): String {
        model.addAttribute("activePage", "login");
        return "login"
    }

    @GetMapping("/register")
    fun register(request : WebRequest, model : Model) : String {
        model.addAttribute("passenger", Passenger())
        return "registration"
    }

    @PostMapping("/register")
    fun registerUserAccount(@Valid passenger: Passenger, bindingResult : BindingResult, model: Model): String {
        if(bindingResult.hasErrors()){
            model.addAttribute("passenger", passenger)
            return "registration"
        }
        try {
            userService.registerNewUser(passenger)
        }
        catch (e : Exception){
            println(e)
            bindingResult.rejectValue("email", "passenger.email", "An account with that email already exists");
            model.addAttribute("passenger", passenger)
            return "registration"
        }
        return "redirect:/"
}
}

