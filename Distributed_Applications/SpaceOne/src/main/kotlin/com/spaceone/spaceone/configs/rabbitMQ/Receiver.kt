package com.spaceone.spaceone.configs.rabbitMQ

import org.springframework.amqp.core.Binding
import org.springframework.amqp.core.BindingBuilder
import org.springframework.amqp.core.Queue
import org.springframework.amqp.core.TopicExchange
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class Receiver {
    private var queue : String = "Queue"
    private var topic : String = "Topic"


    @Bean
    fun createQueue() : Queue {
        return Queue(queue, false)
    }

    @Bean
    fun createTopicExchange() : TopicExchange{
        return  TopicExchange(topic)
    }

    @Bean
    fun bindTopicQueue(queue : Queue, topicExchange: TopicExchange) : Binding{
        return BindingBuilder.bind(queue).to(topicExchange).with("test.#")
    }

    @RabbitListener(queues = ["Queue"])
    fun receiveMessage(message: String) {
        println(message)
    }
}