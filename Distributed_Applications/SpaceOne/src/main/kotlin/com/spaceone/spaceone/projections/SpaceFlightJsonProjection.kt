package com.spaceone.spaceone.projections

import com.spaceone.spaceone.entities.SpaceFlight
import com.spaceone.spaceone.entities.items.Planet
import java.util.Date
import org.springframework.data.jpa.repository.JpaRepository;
interface SpaceFlightJsonProjection {
    val id : Int
    val departureTime : Date
    val arrivalTime : Date
    val destination : Planet
    interface Planet{
        val id : Int
    }
    val price : Int

}