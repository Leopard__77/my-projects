package com.spaceone.spaceone.entities.persons


import com.spaceone.spaceone.entities.Booking
import com.spaceone.spaceone.entities.SpaceFlight
import javax.persistence.Entity
import javax.persistence.ManyToMany
import javax.persistence.OneToMany

@Entity

class Passenger : Person() {
//    @ManyToMany(mappedBy = "bookedBy")
//    lateinit var bookings : Set<Booking>

    @OneToMany(mappedBy = "bookedBy")
    var booking : Set<Booking>? = null
}