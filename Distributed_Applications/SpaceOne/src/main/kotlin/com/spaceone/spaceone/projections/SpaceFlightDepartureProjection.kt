package com.spaceone.spaceone.projections

import java.util.*

interface SpaceFlightDepartureProjection {
    val departureTime : Date
}