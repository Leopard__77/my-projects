package com.spaceone.spaceone.soap.webservice

import com.spaceone.spaceone.soap.model.Flight
import javax.xml.bind.annotation.*



@XmlAccessorType (XmlAccessType.FIELD)
//@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = ["flight"])
@XmlRootElement(name = "getFlightResponse")
class GetFlightResponse {
    fun flight(findFlight: Flight) {
        this.flight = findFlight
    }
    @XmlElement(required = true)
    lateinit var  flight: Flight
}