package com.spaceone.spaceone.configs.rabbitMQ


import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class Producer {

    @Autowired
    lateinit var rabbitmqTemp: RabbitTemplate

    fun sendMessage(id: Int, quarterNr: Int) {
        rabbitmqTemp.convertAndSend("Topic", "test.#", "Passenger with ID $id has booked quarter number $quarterNr")
    }
}