package com.spaceone.spaceone.repositories

import com.spaceone.spaceone.projections.SpaceFlightProjection
import com.spaceone.spaceone.entities.SpaceFlight
import com.spaceone.spaceone.projections.SpaceFlightDepartureProjection
import com.spaceone.spaceone.projections.SpaceFlightJsonProjection
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.*

interface SpaceFlightRepo : CrudRepository<SpaceFlight, Int> {
    fun findAllProjectedBy() : List<SpaceFlightProjection>
    fun findDistinctTopByDepartureTimeIsGreaterThanOrderByDepartureTime(date : Date) : SpaceFlightDepartureProjection?

    fun findAllProjectedJsonBy():List<SpaceFlightJsonProjection>
}