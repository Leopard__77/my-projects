package com.spaceone.spaceone.entities

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import com.spaceone.spaceone.entities.items.Planet
import com.spaceone.spaceone.entities.items.StarShip
import com.spaceone.spaceone.entities.persons.Passenger
import java.io.Serializable
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id")
class SpaceFlight : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id : Int = 0

    @Temporal(TemporalType.TIMESTAMP)
    lateinit var departureTime : Date

    @Temporal(TemporalType.TIMESTAMP)
    lateinit var arrivalTime : Date

    @ManyToOne
    @JoinColumn(name = "destination_id", referencedColumnName = "id", nullable = false)
    var destination : Planet? = null

    @ManyToOne
    @JoinColumn(name = "starship_id", referencedColumnName = "id", nullable = false)
    lateinit var starShip : StarShip

    @NotNull
    @Column(nullable = false)
    var price : Int? = null

    @OneToMany(mappedBy = "spaceFlight")
    var booking : Set<Booking>? = null

    @NotNull
    @Column(nullable = false)
    var availableQuarters : Int? = null
}