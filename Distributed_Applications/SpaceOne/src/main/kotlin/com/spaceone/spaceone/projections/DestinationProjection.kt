package com.spaceone.spaceone.projections

import java.util.*

interface DestinationProjection {
    val id : Int
    val distance : Int
    val name : String
    val pictureUrl : String

    val description : String

}