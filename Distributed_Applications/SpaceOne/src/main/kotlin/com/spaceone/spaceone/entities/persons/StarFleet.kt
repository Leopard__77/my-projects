package com.spaceone.spaceone.entities.persons

import com.spaceone.spaceone.entities.items.StarShip
import org.jetbrains.annotations.NotNull
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.JoinColumn
import javax.persistence.OneToOne

@Entity
class StarFleet : Person() {
    @NotNull
    @Column(nullable = false)
    var currentRank : String = ""

    @OneToOne
    @JoinColumn(name = "starship_id", referencedColumnName = "id")
    lateinit var  commanderOfStarship: StarShip
}