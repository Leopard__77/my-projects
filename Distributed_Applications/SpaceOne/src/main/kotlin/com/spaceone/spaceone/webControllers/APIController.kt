package com.spaceone.spaceone.webControllers

import com.spaceone.spaceone.projections.DestinationProjection
import com.spaceone.spaceone.projections.SpaceFlightJsonProjection
import com.spaceone.spaceone.repositories.PlanetRepo
import org.springframework.web.bind.annotation.RestController
import com.spaceone.spaceone.repositories.SpaceFlightRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.server.ResponseStatusException


@RestController
class APIController {
    @Autowired
    lateinit var spaceFlightRepo: SpaceFlightRepo
    @Autowired
    lateinit var planetRepo: PlanetRepo

    @GetMapping("/api/spaceflights",produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun getAllSpaceFlights(): List<SpaceFlightJsonProjection> {
        return spaceFlightRepo.findAllProjectedJsonBy()
    }
    @GetMapping("/api/destinations", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun getAllDestinations(): List<DestinationProjection> {

        return planetRepo.findAllProjectedJsonBy()
    }

    @GetMapping("/api/destinations/{id}", produces = arrayOf(MediaType.APPLICATION_JSON_VALUE))
    fun getDestination(@PathVariable id: Int): DestinationProjection {
        val destination = planetRepo.findProjectedJsonPlanetById(id)?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "This destination does not exist")

        return destination
    }


}