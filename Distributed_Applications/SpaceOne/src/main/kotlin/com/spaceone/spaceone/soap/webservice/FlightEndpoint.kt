package com.spaceone.spaceone.soap.webservice


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.ws.server.endpoint.annotation.Endpoint
import org.springframework.ws.server.endpoint.annotation.PayloadRoot
import org.springframework.ws.server.endpoint.annotation.RequestPayload
import org.springframework.ws.server.endpoint.annotation.ResponsePayload



@Endpoint
class FlightEndpoint @Autowired constructor(private val flightRepository: FlightRepository) {
    @PayloadRoot(namespace = FlightEndpoint.Companion.NAMESPACE_URI, localPart = "getFlightRequest")
    @ResponsePayload
    fun getCountry(@RequestPayload request: GetFlightRequest): GetFlightResponse {
        val response = GetFlightResponse()
        response.flight = flightRepository.findFlight(request.name!!)
        return response
    }

    companion object {
        private const val NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service"
    }


}