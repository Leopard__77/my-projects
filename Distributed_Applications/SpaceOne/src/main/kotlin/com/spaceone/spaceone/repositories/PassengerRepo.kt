package com.spaceone.spaceone.repositories

import com.spaceone.spaceone.entities.persons.Passenger
import javax.validation.constraints.Email

interface PassengerRepo : PersonRepo<Passenger> {
    //fun findByName(name: String?): List<Passenger?>?
    override fun findByEmail(email: String) : Passenger?
}