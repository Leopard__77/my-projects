package com.spaceone.spaceone





import com.spaceone.spaceone.email.service.EmailSenderService
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Mark this class an injectable component so that the Spring environment will create
 * an instance of this class when it starts up.
 */
@Component
class Timer {

    private val logger = LoggerFactory.getLogger(Timer::class.java)
    private final val datum: String = "2022-05-01"
    val date = SimpleDateFormat("yyyy-MM-dd").parse(datum)

    /**
     * This @Schedule annotation run every 5 seconds in this case. It can also
     * take a cron like syntax.
     * Elke eerste dag van de maand om 11u15  TO DO SOMETHING (Timer Works perfect)
     * See https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/scheduling/support/CronSequenceGenerator.html
     */
    @Scheduled(cron = "0 10 11 1 * ?") //Elke eerste dag van de maand om 11u15  TO DO SOMETHING (Timer Works perfect)
    fun scheduleTimerAtFirstDayOfTheMonth(){
        EmailSenderService.sendmailviaRest();
        logger.info("The time is now ${DateTimeFormatter.ISO_LOCAL_TIME.format(LocalDateTime.now())}")
        EmailSenderService.sendmailviaRest();

    }

}