package com.spaceone.spaceone.entities.persons

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import org.jetbrains.annotations.NotNull
import java.io.Serializable
import javax.persistence.*
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty

@MappedSuperclass
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id")
open class Person : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id : Int = 0

    @NotEmpty(message = "First name can not be empty")
    @Column(nullable = false,name ="firstName") //The @Column annotation is defined as a part of the Java Persistence API specification.
    var firstName : String? = null

    @NotEmpty(message = "Last name can not be empty")
    @Column(nullable = false,name ="lastName") //The @Column annotation is defined as a part of the Java Persistence API specification.
    var lastName : String? = null

    @Column(nullable = false,name ="credits")
    var credits : Int = 0

    @Email(message = "Invalid email")
    @NotEmpty(message = "Email cannot be empty!")
    @Column(nullable = false,name ="email")
    var email : String? = null

    @NotEmpty(message = "Password can not be empty")
    var password: String? = null
}