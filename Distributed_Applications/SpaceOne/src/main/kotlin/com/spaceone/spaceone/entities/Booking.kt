package com.spaceone.spaceone.entities

import com.spaceone.spaceone.entities.items.Planet
import com.spaceone.spaceone.entities.persons.Passenger
import org.springframework.beans.factory.annotation.Value
import java.io.Serializable
import javax.persistence.*
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@Entity
class Booking : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id : Int = 0

    @ManyToOne
    @JoinColumn(name = "passenger_id", referencedColumnName = "id", nullable = false)
    var bookedBy : Passenger? = null

    @ManyToOne
    @JoinColumn(name = "space_flight_id", referencedColumnName = "id", nullable = false)
    var spaceFlight : SpaceFlight? = null

    @NotNull
    var quarterNr : Int? = null
}