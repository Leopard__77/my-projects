package com.spaceone.spaceone.repositories

import com.spaceone.spaceone.entities.Booking
import com.spaceone.spaceone.entities.SpaceFlight
import org.springframework.data.repository.CrudRepository

interface BookingRepo : CrudRepository<Booking, Int> {
    //if a row return -> quarters already taken
    fun findTopByQuarterNrAndSpaceFlight(quarterNr : Int?, spaceFlight: SpaceFlight) : List<Booking>
}