package com.spaceone.spaceone.security

import com.spaceone.spaceone.entities.persons.Passenger
import com.spaceone.spaceone.repositories.PassengerRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service
@Transactional
class CustomUserDetailsService : UserDetailsService{
    @Autowired
    lateinit var passengerRepo: PassengerRepo

    override fun loadUserByUsername(email: String): UserDetails {
        val existingPassenger: Passenger = passengerRepo.findByEmail(email)
            ?: throw Exception("No user found with username: $email")
        val grantedAuthorities: MutableSet<GrantedAuthority> = HashSet()
            grantedAuthorities.add(SimpleGrantedAuthority("USER"))
            val enabled = true
            val accountNonExpired = true
            val credentialsNonExpired = true
            val accountNonLocked = true
            return User(
                existingPassenger.email, existingPassenger.password, enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, grantedAuthorities
            )
        }
}