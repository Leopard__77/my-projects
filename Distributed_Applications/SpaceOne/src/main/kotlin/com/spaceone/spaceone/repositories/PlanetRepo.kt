package com.spaceone.spaceone.repositories


import com.spaceone.spaceone.entities.items.Planet
import com.spaceone.spaceone.projections.DestinationProjection


interface PlanetRepo : ItemRepo<Planet> {

    fun findAllProjectedJsonBy():List<DestinationProjection>
    fun findProjectedJsonPlanetById(id: Int):DestinationProjection?
}