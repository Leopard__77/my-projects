package com.spaceone.spaceone.repositories

import com.spaceone.spaceone.entities.items.Item
import com.spaceone.spaceone.entities.persons.Passenger
import com.spaceone.spaceone.entities.persons.Person
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.NoRepositoryBean

@NoRepositoryBean
interface PersonRepo<T : Person> : CrudRepository<T, Int>{
    fun findByEmail(email : String) : Passenger?
}