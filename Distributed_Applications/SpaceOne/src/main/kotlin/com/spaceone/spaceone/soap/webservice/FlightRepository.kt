package com.spaceone.spaceone.soap.webservice

import com.spaceone.spaceone.projections.SpaceFlightProjection
import com.spaceone.spaceone.soap.model.Flight
import com.spaceone.spaceone.soap.model.Currency
import com.spaceone.spaceone.repositories.SpaceFlightRepo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.util.Assert
import java.util.*
import javax.annotation.PostConstruct
import kotlin.collections.HashMap


@Component
class FlightRepository {
    @Autowired
    lateinit var spaceFlightRepo: SpaceFlightRepo
    @PostConstruct
    fun initData() {


        val nextFlight: List<SpaceFlightProjection> =spaceFlightRepo.findAllProjectedBy()

        val flight = Flight("flight", nextFlight.get(0).departureTime.toString(),nextFlight.get(0).destination.name,nextFlight.get(0).price.toString() ,Currency.EUR)

        flights[flight.name] = flight

    }


    fun findFlight(name: String): Flight {
        Assert.notNull(name, "The flight's name must not be null")
        return flights[name]!!
    }
    companion object {
        private val flights: MutableMap<String, Flight> = HashMap<String, Flight>()
    }


}

