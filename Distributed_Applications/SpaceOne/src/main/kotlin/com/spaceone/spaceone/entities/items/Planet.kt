package com.spaceone.spaceone.entities.items

import com.spaceone.spaceone.entities.SpaceFlight
import org.jetbrains.annotations.NotNull
import javax.persistence.*

@Entity
class Planet : Item() {
    @NotNull
    @Column(nullable = false)
    var distance : Int = 0

    @OneToMany(mappedBy = "destination")
    lateinit var destination : Set<SpaceFlight>
}