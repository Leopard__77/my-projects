package com.spaceone.spaceone.soap.model

import javax.xml.bind.annotation.XmlAccessType
import javax.xml.bind.annotation.XmlAccessorType
import javax.xml.bind.annotation.XmlEnum
import javax.xml.bind.annotation.XmlType


/**
 *
 * Java class for currency.
 *
 *
 * The following schema fragment specifies the expected content contained within this class.
 *
 *
 * <pre>
 * &lt;simpleType name="currency">
 * &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 * &lt;enumeration value="GBP"/>
 * &lt;enumeration value="EUR"/>
 * &lt;enumeration value="PLN"/>
 * &lt;/restriction>
 * &lt;/simpleType>
</pre> *
 *
 */
@XmlType(name = "currency")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlEnum
enum class Currency {
    GBP, EUR, PLN;

    fun value(): String {
        return name
    }

    companion object {
        fun fromValue(v: String): Currency {
            return valueOf(v)
        }
    }
}