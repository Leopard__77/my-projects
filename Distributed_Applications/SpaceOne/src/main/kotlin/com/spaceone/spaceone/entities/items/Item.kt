package com.spaceone.spaceone.entities.items

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonTypeId
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import org.jetbrains.annotations.NotNull
import org.springframework.data.repository.NoRepositoryBean
import org.springframework.validation.annotation.Validated
import java.io.Serializable
import java.util.StringJoiner
import javax.persistence.*


@MappedSuperclass //one table per (child)class
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator::class,
    property = "id")
open class Item : Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id : Int = 0

    @NotNull //The @NotNull annotation is defined in the Bean Validation specification
    @Column(nullable = false) //The @Column annotation is defined as a part of the Java Persistence API specification.
    var name : String? = null

    var pictureUrl : String = ""

    @Column(columnDefinition = "TEXT")
    var description : String = ""
}