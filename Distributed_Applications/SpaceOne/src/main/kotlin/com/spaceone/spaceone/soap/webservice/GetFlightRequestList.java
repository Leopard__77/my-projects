package com.spaceone.spaceone.soap.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(namespace="http://spring.io/guides/gs-producing-web-service", name="getFlightRequestList")
public class GetFlightRequestList {

}
