package com.spaceone.spaceone

import com.spaceone.spaceone.configs.rabbitMQ.Receiver
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import javax.xml.soap.*
import javax.xml.transform.TransformerFactory
import javax.xml.transform.stream.StreamResult

@SpringBootApplication
class SpaceOneApplication

fun main(args: Array<String>) {
    runApplication<SpaceOneApplication>(*args)
}
