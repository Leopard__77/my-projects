package com.spaceone.spaceone.configs

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Description
import org.springframework.webflow.config.AbstractFlowConfiguration
import org.springframework.webflow.definition.registry.FlowDefinitionRegistry
import org.springframework.webflow.engine.builder.ViewFactoryCreator
import org.springframework.webflow.engine.builder.support.FlowBuilderServices
import org.springframework.webflow.executor.FlowExecutor
import org.springframework.webflow.mvc.builder.MvcViewFactoryCreator
import org.springframework.webflow.mvc.servlet.FlowHandlerAdapter
import org.springframework.webflow.mvc.servlet.FlowHandlerMapping
import org.thymeleaf.spring5.SpringTemplateEngine
import org.thymeleaf.spring5.webflow.view.AjaxThymeleafViewResolver
import org.thymeleaf.spring5.webflow.view.FlowAjaxThymeleafView
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver


@Configuration
class WebFlowConfig : AbstractFlowConfiguration() {
    @Bean
    fun flowRegistry(): FlowDefinitionRegistry {
        return flowDefinitionRegistryBuilder
            .setBasePath("classpath:flows") //
            .addFlowLocationPattern("/**/*-flow.xml")
            .setFlowBuilderServices(flowBuilderServices())
            .build()
    }

    @Bean
    fun flowExecutor(): FlowExecutor {
        return getFlowExecutorBuilder(flowRegistry()).build()
    }

    @Bean
    fun flowBuilderServices(): FlowBuilderServices {
        return flowBuilderServicesBuilder //
            .setViewFactoryCreator(mvcViewFactoryCreator()).build()
    }

    @Bean
    fun flowHandlerMapping(): FlowHandlerMapping {
        val handlerMapping = FlowHandlerMapping()
        handlerMapping.order = -1
        handlerMapping.flowRegistry = flowRegistry()
        return handlerMapping
    }

    //
    @Bean
    fun flowHandlerAdapter(): FlowHandlerAdapter {
        val handlerAdapter = FlowHandlerAdapter()
        handlerAdapter.flowExecutor = flowExecutor()
        handlerAdapter.saveOutputToFlashScopeOnRedirect = true
        return handlerAdapter
    }

    @Bean
    fun mvcViewFactoryCreator(): ViewFactoryCreator {
        val factoryCreator = MvcViewFactoryCreator()
        factoryCreator.setViewResolvers(listOf(thymeleafViewResolver()))
        factoryCreator.setUseSpringBeanBinding(true)
        return factoryCreator
    }

    @Bean
    @Description("Thymeleaf AJAX view resolver for Spring WebFlow")
    fun thymeleafViewResolver(): AjaxThymeleafViewResolver {
        val viewResolver = AjaxThymeleafViewResolver()
        viewResolver.setViewClass(FlowAjaxThymeleafView::class.java)
        viewResolver.templateEngine = templateEngine()
        viewResolver.characterEncoding = "UTF-8"
        return viewResolver
    }

    @Bean
    @Description("Thymeleaf template resolver serving HTML 5")
    fun templateResolver(): ClassLoaderTemplateResolver {
        val templateResolver = ClassLoaderTemplateResolver()
        templateResolver.prefix = "templates/"
        templateResolver.isCacheable = false
        templateResolver.suffix = ".html"
        templateResolver.setTemplateMode("HTML")
        templateResolver.characterEncoding = "UTF-8"
        return templateResolver
    }

    @Bean
    @Description("Thymeleaf template engine with Spring integration")
    fun templateEngine(): SpringTemplateEngine {
        val templateEngine = SpringTemplateEngine()
        templateEngine.setTemplateResolver(templateResolver())
        return templateEngine
    }
}
