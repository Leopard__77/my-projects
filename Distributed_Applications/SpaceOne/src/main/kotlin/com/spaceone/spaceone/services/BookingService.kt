package com.spaceone.spaceone.services

import com.spaceone.spaceone.configs.rabbitMQ.Producer
import com.spaceone.spaceone.entities.Booking
import com.spaceone.spaceone.entities.SpaceFlight
import com.spaceone.spaceone.entities.persons.Passenger
import com.spaceone.spaceone.repositories.BookingRepo
import com.spaceone.spaceone.repositories.PassengerRepo
import com.spaceone.spaceone.repositories.SpaceFlightRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class BookingService {

    @Autowired
    lateinit var spaceFlightRepo: SpaceFlightRepo

    @Autowired
    lateinit var passengerRepo: PassengerRepo

    @Autowired
    lateinit var bookingRepo: BookingRepo

    @Autowired
    lateinit var producer: Producer

    fun createBooking(flightId : Int) : Booking {
        val booking : Booking = Booking()
        booking.spaceFlight = spaceFlightRepo.findById(flightId).get()
        booking.bookedBy = passengerRepo.findByEmail(SecurityContextHolder.getContext().authentication.name)
        return booking
    }


    @Transactional
    fun confirmBooking(booking : Booking) : Boolean{
        return if(booking.bookedBy?.credits!! >= booking.spaceFlight?.price!! && (booking.spaceFlight!!.availableQuarters?.minus(
                1
            ))!! >= 0 && booking.quarterNr!! >=0 && booking.quarterNr!! < booking.spaceFlight!!.availableQuarters!!
        ){
            if(bookingRepo.findTopByQuarterNrAndSpaceFlight(booking.quarterNr, booking.spaceFlight!!).isEmpty()){
                bookingRepo.save(booking)

                val spaceFlight : SpaceFlight = booking.spaceFlight!!
                spaceFlight.availableQuarters = booking.spaceFlight!!.availableQuarters!! - 1
                spaceFlightRepo.save(spaceFlight)

                val passenger : Passenger = booking.bookedBy!!
                passenger.credits = booking.bookedBy!!.credits - booking.spaceFlight!!.price!!
                passengerRepo.save(passenger)
                producer.sendMessage(passenger.id, booking.quarterNr!!)
                true
            }
            else{
                false
            }

        } else{
            false
        }
    }
}