package com.spaceone.spaceone.security

import com.spaceone.spaceone.entities.persons.Passenger
import com.spaceone.spaceone.repositories.PassengerRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional


@Service
@Transactional
class CustomUserService {
    @Autowired
    lateinit var passengerRepo: PassengerRepo

    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    fun registerNewUser(newPassenger: Passenger){
        if(newPassenger.email?.let { passengerRepo.findByEmail(it) } != null){
                throw Exception("An user already exists with this email: " + newPassenger.email)
        }
        newPassenger.password = passwordEncoder.encode(newPassenger.password)
        passengerRepo.save(newPassenger)
    }
}