package com.spaceone.spaceone.email.service

import lombok.RequiredArgsConstructor
import lombok.extern.slf4j.Slf4j
import org.springframework.stereotype.Service
import java.io.IOException
import java.util.*
import javax.mail.*
import javax.mail.internet.*


@Service
@RequiredArgsConstructor
@Slf4j
class EmailSenderService (
    ) {
    companion object {
        @Throws(AddressException::class, MessagingException::class, IOException::class)
        fun sendmailviaRest() {
            val props = Properties()
            props.put("mail.smtp.auth", "true")
            props.put("mail.smtp.starttls.enable", "true")
            props.put("mail.smtp.host", "smtp.gmail.com")
            props.put("mail.smtp.port", "587")
            val session: Session = Session.getInstance(props, object : Authenticator() {
                override fun getPasswordAuthentication(): PasswordAuthentication {
                    return PasswordAuthentication("distributedapplicationsgt@gmail.com", "Secret99")
                }
            })
            val msg: Message = MimeMessage(session)
            msg.setFrom(InternetAddress("distributedapplicationsgt@gmail.com", false))
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse("siebren.michiels@hotmail.com"))
            msg.setSubject("Space one invitation")
            msg.setHeader("Welcome to Space one","text/html")
            msg.setContent("hello", "text/html")
            val messageBodyPart = MimeBodyPart()
            messageBodyPart.setContent("Welcome aboard", "text/html")
            val multipart: Multipart = MimeMultipart()
            multipart.addBodyPart(messageBodyPart)

            Transport.send(msg)
        }
    }

}