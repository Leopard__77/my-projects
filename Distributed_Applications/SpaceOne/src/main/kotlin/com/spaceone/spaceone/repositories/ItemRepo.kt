package com.spaceone.spaceone.repositories

import com.spaceone.spaceone.entities.items.Item
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.NoRepositoryBean

@NoRepositoryBean
interface ItemRepo<T : Item> : CrudRepository<T, Int> {
}