package com.spaceone.spaceone.soap.model

import javax.xml.bind.annotation.*


@XmlAccessorType(XmlAccessType.FIELD) //FIELD
@XmlType(name = "flight", propOrder =["name", "tijdstip", "planet","price", "currency"] )//["name", "population", "capital", "currency"]
class Flight(name: String, tijdstip: String, planet: String,price:String, currency: com.spaceone.spaceone.soap.model.Currency) {

    @XmlElement(required = true)
    val name= name

    @XmlElement(required = true)
    val tijdstip = tijdstip

    @XmlElement(required = true)
    val planet = planet

    @XmlElement(required = true)
    val price = price

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    var currency = currency


   /* fun getTheCurrency(): Currency {
        return currency
    }*/


}