package com.spaceone.spaceone.entities.items

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import com.spaceone.spaceone.entities.SpaceFlight
import com.spaceone.spaceone.entities.persons.StarFleet
import org.jetbrains.annotations.NotNull
import javax.persistence.*

enum class Status {
    OPERATIONAL, MAINTENANCE, DECOMMISSIONED
}

@Entity

class StarShip : Item() {

    @NotNull
    @Column(nullable = false)
   var classType : String = ""

    @NotNull
    @Column(nullable = false)
    var tag : String = ""

    @Enumerated(EnumType.STRING)
    var status : Status = Status.OPERATIONAL

    @OneToOne(mappedBy = "commanderOfStarship")
    lateinit var commander : StarFleet

    @OneToMany(mappedBy = "starShip")
    lateinit var spaceFlight: List<SpaceFlight>

    var nrOfQuarters : Int? = null
}