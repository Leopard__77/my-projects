USE UniMatrix01;
INSERT INTO planet (name, distance, picture_url, description) VALUE
    ('Earth', 0,'http://images.mmorpg.com/images/galleries/full/142010/13d21d68-4a73-41d9-908f-2f3ccc08572a.jpg', 'Earth, also known as Sol III or Terra, was the inhabited third planet of the Sol system. Earth was the homeworld of the Humans and the Voth, among others, and was the capital planet of the United Federation of Planets and the home of Starfleet Headquarters from 2161 to 3089.'),
    ('Qo\'\'noS',0,'https://gamepedia.cursecdn.com/sto_gamepedia/f/f0/Qo%27noS.jpg', 'Qo\'\'noS, alternatively spelled as Q\'\'onoS, also known as Kling or the Klingon homeworld, and transliterated to Kronos in English, was an inhabited class M planet in the Qo\'\'noS system, the homeworld of the warp-capable Klingon species, and the capital planet of the Klingon Empire.'),
    ('Ni\'\'Var',0,'https://i.pinimg.com/originals/db/ba/6f/dbba6fcf1f04ca91e793df3f8ce11e1d.jpg', 'Ni\'\'Var, formerly Vulcan, was an inhabited M-class planet in the Vulcan system of the Alpha Quadrant. It had no moons, but appeared to have close planetary companions. It was the homeworld of the Vulcans, a warp-capable humanoid species. '),
    ('Bajor', 0,'https://sto.gamepedia.com/media/sto.gamepedia.com/thumb/f/f0/Bajor_remastered.png/1200px-Bajor_remastered.png', 'Bajor was an inhabited planet of the Bajoran system. This system was located in the Bajor sector, in the Alpha Quadrant. This was the largest planet in the system and had five moons, including Derna and Jeraddo. The planet was the homeworld of the Bajorans, a warp-capable humanoid species. After decades of Cardassian rule, the planet regained its independence and became affiliated with the United Federation of Planets in 2369.');

INSERT INTO star_ship (name, class_type, tag, picture_url, description, nr_of_quarters) VALUE
    ('Enterprise', 'NX-class', 'NX-01', 'https://3.bp.blogspot.com/-xisxcSKXqFo/U03oRkhm1LI/AAAAAAAAZQg/IQzV55Er3ww/s1600/Drex+Files+Enterprise+NX-01+Earth.jpg', 'One of the most important starships in interstellar history, Enterprise (NX-01) was the culmination of the NX Project. The NX-01 was the first NX-class starship, launched by the United Earth Starfleet in 2151.', 100),
    ('U.S.S. Enterprise', 'Constitution-class', 'NCC-1701-A', 'http://3.bp.blogspot.com/-Oi0wOlJj84M/ToqZZ7242CI/AAAAAAAAAD0/8q5xRKFtBb8/s1600/final1.jpg', 'The USS Enterprise (NCC-1701-A) was a Constitution-class vessel operated by Starfleet in the 23rd century. It was the second Federation starship to bear the name Enterprise. After the Enterprise-A was decommissioned and mothballed, it was succeeded by the USS Enterprise-B.', 200),
    ('U.S.S. Enterprise', 'Galaxy-class ', 'NCC-1701-D' , 'https://cdnb.artstation.com/p/assets/images/images/001/314/605/4k/michael-long-1701d-1-l.jpg?1444197715', 'The USS Enterprise (NCC-1701-D) was a 24th century Federation Galaxy-class starship operated by Starfleet, and the fifth Federation starship to bear the name Enterprise. During its career, the Enterprise served as the Federation flagship and was in service from 2363 to 2371. ', 20),
    ('U.S.S. Voyager', 'Intrepid-class', 'NCC-74656', 'http://vignette4.wikia.nocookie.net/memory-gamma/images/2/25/USS_Voyager_B.jpg/revision/latest/scale-to-width-down/2000?cb=20160315205005', 'The USS Voyager (NCC-74656) was a 24th century Federation Intrepid-class starship operated by Starfleet from 2371 to 2378. The vessel was famous for completing an unscheduled seven-year journey across the Delta Quadrant between 2371 and 2378, which was the first successful exploration of that quadrant by the Federation. It was the first ship to bear the name Voyager with this registry. ', 5),
    ('U.S.S. Defiant', 'Defiant-class', 'NX-74205', 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/i/008092c9-f5ad-4ec9-8a01-fd4082386a56/d9ta7k0-54458792-6215-4075-b821-2a4523ef2e6f.png', 'The USS Defiant (NX-74205) was a 24th century Federation Defiant-class starship operated by Starfleet. This was the prototype of the class and the second Federation ship known to bear the name Defiant. ', 10);

INSERT INTO star_fleet (first_name, last_name, starship_id, current_rank, credits, email) VALUE
    ('Jonathan', 'Archer', 1, 'Admiral', 577, 'jonathan.archer@starflet.com'),
    ('James', 'T. Kirk', 2, 'Captain',199, 'james.t.kirk@starflet.com'),
    ('Jean-Luc', 'Picard', 3, 'Admiral',984, 'jean-luc.picard@starflet.com'),
    ('Kathryn', 'Janeway', 4, 'Vice Admiral',979, 'kathryn.janeway@starflet.com'),
    ('Benjamin', 'Sisko', 5, 'Captain',638, 'benjamin.sisko@starflet.com');

INSERT INTO space_flight (arrival_time, departure_time, destination_id, starship_id, price, available_quarters) VALUE
    ('2022-05-30 08:00:00', '2022-07-28 20:30:00', 1, 2, 500, 10),
    ('2022-05-31 10:00:00', '2022-08-28 08:30:00', 1, 3, 750, 20),
    ('2022-06-1 18:00:00', '2022-07-31 05:15:00', 1, 1, 1500, 50),
     ('2022-06-4 01:00:00', '2022-07-1 08:30:00', 2, 1, 2000, 3),
    ('2022-06-10 15:00:00', '2022-07-1 21:30:00', 4, 5, 4000, 1),
    ('2022-06-15 12:00:00', '2022-06-30 08:00:00', 3, 5,500, 0),
    ('2022-07-1 15:00:00', '2022-07-1 19:30:00', 4, 4, 200, 50),
    ('2022-06-30 16:00:00', '2022-07-21 01:30:00', 1, 4, 600, 100);



