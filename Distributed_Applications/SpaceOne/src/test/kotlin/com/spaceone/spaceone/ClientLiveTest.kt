package com.spaceone.spaceone

import com.spaceone.spaceone.soapspring.client.CountryClient
import com.spaceone.spaceone.soapspring.client.CountryClientConfig
import com.spaceone.spaceone.soapspring.model.Country
import com.spaceone.spaceone.soapspring.simpleclient.ASimpleSOAPClient
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.support.AnnotationConfigContextLoader


@RunWith(SpringJUnit4ClassRunner::class)
@ContextConfiguration(classes = arrayOf(CountryClientConfig::class), loader = AnnotationConfigContextLoader::class)
class ClientLiveTest {
    @Autowired
    var client: CountryClient? = null
    //lateinit var client: CountryClient
    @Test
    fun givenCountryService_whenCountryPoland_thenCapitalIsWarsaw() {
        val response = client!!.getCountry("Spain")
        println("Hello "+response.country)
        assertEquals("Madrid", response.country.capital)
    }
/*    @Test
    fun givenCountryService_whenCountryPoland_thenCapitalIsWarsawdd() {
        val response= ASimpleSOAPClient().ASimpleSOAPClient("http://localhost:8085/ws/countries.wsdl")

        println("Hello "+response)
        //assertEquals("Madrid", response.country.capital)
    }*/


}