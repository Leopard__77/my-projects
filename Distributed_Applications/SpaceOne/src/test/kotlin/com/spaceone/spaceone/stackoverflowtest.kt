package com.spaceone.spaceone

import org.assertj.core.api.Assertions
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import javax.xml.soap.*
import javax.xml.transform.TransformerFactory
import javax.xml.transform.stream.StreamResult


@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class stackoverflowtest {
    @Throws(Exception::class)
    private fun createSOAPRequest(): SOAPMessage {
        val messageFactory: MessageFactory = MessageFactory.newInstance()
        val soapMessage: SOAPMessage = messageFactory.createMessage()
        val soapPart = soapMessage.soapPart

        // SOAP Envelope
        val envelope = soapPart.envelope
        envelope.addNamespaceDeclaration("web", "http://www.webserviceX.NET")

        // SOAP Body
        val soapBody: SOAPBody = envelope.body
        val soapBodyElem: SOAPElement = soapBody.addChildElement("GetCitiesByCountry", "web")
        val soapBodyElem1 = soapBodyElem.addChildElement("CountryName", "web")
        soapBodyElem1.addTextNode("Brazil")
        val header: MimeHeaders = soapMessage.mimeHeaders
        header.setHeader("SOAPAction", "http://www.webserviceX.NET/GetCitiesByCountry")
        soapMessage.saveChanges()

        // Check the input
        println("Request SOAP Message = ")
        soapMessage.writeTo(System.out)
        return soapMessage
    }

    /**
     * Method used to print the SOAP Response
     */
    @Throws(java.lang.Exception::class)
    private fun printSOAPResponse(soapResponse: SOAPMessage) {
        val transformerFactory = TransformerFactory.newInstance()
        val transformer = transformerFactory.newTransformer()
        val sourceContent = soapResponse.soapPart.content
        println("\nResponse SOAP Message = ")
        val result = StreamResult(System.out)
        transformer.transform(sourceContent, result)
    }
    fun whenSendRequest_thenResponseIsNotNullHELLO() {
        val soapConnectionFactory = SOAPConnectionFactory.newInstance()
        val soapConnection = soapConnectionFactory.createConnection()

        val url = "http://www.webservicex.com/globalweather.asmx"

        val soapResponse = soapConnection.call(createSOAPRequest(), url)

        printSOAPResponse(soapResponse);

        soapConnection.close()

    }


}