package com.spaceone.spaceone

import com.spaceone.spaceone.soapspring.client.CountryClient
import junit.framework.TestCase.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.oxm.jaxb.Jaxb2Marshaller
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner


//@RunWith(SpringRunner::class)
//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner::class)
class ApplicationIntegrationTest {
    private val marshaller = Jaxb2Marshaller()

    @Autowired
    lateinit var client: CountryClient

    @Test
    fun givenCountryService_whenCountryPoland_thenCapitalIsWarsaw() {
        val response = client.getCountry("Spain")
        assertEquals("Madrid", response.country.capital)
    }

    /*@LocalServerPort
    private val port = 8085
    @Before
    @Throws(Exception::class)
    fun init() {
        marshaller.setPackagesToScan(ClassUtils.getPackageName(GetCountryRequest::class.java))
        marshaller.afterPropertiesSet()
    }

    @Test
    fun whenSendRequest_thenResponseIsNotNull() {
        val ws = WebServiceTemplate(marshaller)
        val request = GetCountryRequest()
        request.name = "Spain"
        Assertions.assertThat(ws.marshalSendAndReceive("http://localhost:8085/ws", request)).isNotNull
    }*/
}