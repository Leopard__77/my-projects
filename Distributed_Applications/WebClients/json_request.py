import requests


# Press the green button in the gutter to run the script.

def get_destination_id(json_flight_obj):
    return str(json_flight_obj["destination"]["id"])


if __name__ == '__main__':
    response = requests.get("http://localhost:8085/api/spaceflights")
    if response.status_code == 200:
        get_destination_id(response.json()[0])  # get first flight
        destination_response = requests.get("http://localhost:8085/api/destinations/"+get_destination_id(response.json()[0]))
        print(destination_response.json())
