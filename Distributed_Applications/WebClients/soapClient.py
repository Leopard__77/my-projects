# Made by Pieter Touquet on 26 May


import requests

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # SOAP request URL
    url = "http://localhost:8085/ws"

    # structured XML
    payload = """<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
                      xmlns:gs="http://spring.io/guides/gs-producing-web-service">
        <soapenv:Header/>
        <soapenv:Body>
            <gs:getFlightRequest>
                <gs:name>flight</gs:name>
            </gs:getFlightRequest>
        </soapenv:Body>
    </soapenv:Envelope>"""
    # headers
    headers = {
        'Content-Type': 'text/xml; charset=utf-8'
    }
    # POST request
    response = requests.request("POST", url, headers=headers, data=payload)

    # prints the response
    print(response.text)
    print(response)
