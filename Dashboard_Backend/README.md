# API CALLS

## List servers
```
[GET] [localhost:8080/api/servers](localhost:8080/api/servers)

Returns list of all registered servers
```

## Delete server
```
[POST] [localhost:8080/api/server/delete?id=](localhost:8080/api/server/delete?id=)

Delete server with id
```

## Edit name server
```
[POST] [localhost:8080/api/server/editname?id= &name=](localhost:8080/api/servers)

Edit name of server
```

## List potds
```
[GET] [localhost:8080//api/nasa/potds](localhost:8080/api/nasa/potds)

returns all available potds 
```

## Get Image potd
```
[GET] [localhost:8080/api/nasa/potd/image?id=](localhost:8080/api/nasa/potd/image?id=)

Give ID, returns image of potd 
```

## List rovers
```
[GET] [localhost:8080/api/nasa/rovers](localhost:8080/api/nasa/rovers)

Returns list of rovers with info & photos
```

## Register
```
[Post] [localhost:8080/api/nasa/rovers]

[Body example] 

    {
        "userName": "dev2",
        "password": "test",
        "registerCode": "CPT"
    }

registerCode needs to be the same as defined in the application.properties
Register an user
```


## Authenticate
```
[Post] [localhost:8080/api/authenticate]

[Body example] 

    {
        "userName": "dev",
        "password": "test"
    }

[Return example]

    {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhdXRoMCIsImV4cCI6MTYyODQ3MzM2NywiaWF0IjoxNjI4NDM3MzY3LCJ1c2VybmFtZSI6ImRldiJ9.vRg4ddGhOghkdGslDoHCWOBXNvH1JQ-Z3dMxvOhETkk"
    }

Authenticate an user
```

## Updates requires a CRONJOB to refresh database

## Creating docker image
Create jar file
```
./mvnw clean package 
```
build and run docker images
```
docker-compose up --build
```