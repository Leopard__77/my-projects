package com.dashboard_backhand.dashboard_backend.repositories;

import com.dashboard_backhand.dashboard_backend.models.server.Server;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ServerRepository extends MongoRepository<Server, String> {
}
