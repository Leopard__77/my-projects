package com.dashboard_backhand.dashboard_backend.repositories;

import com.dashboard_backhand.dashboard_backend.models.potd.Potd;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface PotdRepository extends MongoRepository<Potd, String> {
    Optional<Potd> findById(String id);
    List<Potd> findAll(Sort sort);

}
