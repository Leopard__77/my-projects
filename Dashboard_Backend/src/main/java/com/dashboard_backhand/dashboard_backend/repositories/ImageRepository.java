package com.dashboard_backhand.dashboard_backend.repositories;

import com.dashboard_backhand.dashboard_backend.models.server.Image;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ImageRepository extends MongoRepository<Image, String> {
}
