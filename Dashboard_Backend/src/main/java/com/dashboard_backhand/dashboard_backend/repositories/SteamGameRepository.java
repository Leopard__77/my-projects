package com.dashboard_backhand.dashboard_backend.repositories;

import com.dashboard_backhand.dashboard_backend.models.Steam.SteamGame;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface SteamGameRepository extends MongoRepository<SteamGame, String> {
    Optional<SteamGame> findSteamGameByGameId(String game_id);
}
