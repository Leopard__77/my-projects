package com.dashboard_backhand.dashboard_backend.repositories;

import com.dashboard_backhand.dashboard_backend.models.rover.Rover;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoverRepository extends MongoRepository<Rover, String> {
    Optional<Rover> findByName(String name);
}
