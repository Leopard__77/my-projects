package com.dashboard_backhand.dashboard_backend.repositories;


import com.dashboard_backhand.dashboard_backend.models.auth.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findUserByUsername(String username);
}