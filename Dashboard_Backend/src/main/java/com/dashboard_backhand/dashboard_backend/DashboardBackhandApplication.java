package com.dashboard_backhand.dashboard_backend;

import com.dashboard_backhand.dashboard_backend.models.ConfigProperties;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.GatewayDiscordClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties(ConfigProperties.class)
public class DashboardBackhandApplication {

    public static void main(String[] args) {
        SpringApplication.run(DashboardBackhandApplication.class, args);
    }

}
