package com.dashboard_backhand.dashboard_backend;

import com.dashboard_backhand.dashboard_backend.config.BotConfiguration;
import com.dashboard_backhand.dashboard_backend.models.ConfigProperties;
import com.dashboard_backhand.dashboard_backend.models.Steam.SteamGame;
import com.dashboard_backhand.dashboard_backend.models.Steam.SteamNewsList;
import com.dashboard_backhand.dashboard_backend.models.Steam.SteamNewsWrapper;
import com.dashboard_backhand.dashboard_backend.models.auth.AuthRequest;
import com.dashboard_backhand.dashboard_backend.models.auth.Register;
import com.dashboard_backhand.dashboard_backend.models.auth.Token;
import com.dashboard_backhand.dashboard_backend.models.auth.User;
import com.dashboard_backhand.dashboard_backend.models.discord.DiscordChannels;
import com.dashboard_backhand.dashboard_backend.models.discord.SendMessage;
import com.dashboard_backhand.dashboard_backend.models.potd.Potd;
import com.dashboard_backhand.dashboard_backend.models.rover.Rover;
import com.dashboard_backhand.dashboard_backend.models.server.*;
import com.dashboard_backhand.dashboard_backend.rabbitmq.EventConsumer;
import com.dashboard_backhand.dashboard_backend.repositories.*;
import com.dashboard_backhand.dashboard_backend.utils.JWTUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import discord4j.common.util.Snowflake;
import discord4j.core.object.entity.channel.MessageChannel;
import io.netty.util.internal.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import reactor.core.publisher.Flux;
import io.github.matafokka.bbcode_converter.BBCodeConverter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("api")
public class ApiCalls {

    @Autowired
    private JWTUtils jwtUtils;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private ServerRepository serverRepository;
    @Autowired
    private PotdRepository potdRepository;
    @Autowired
    private RoverRepository roverRepository;
    @Autowired
    private ImageRepository imageRepository;
    @Autowired
    private ConfigProperties configProperties;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BotConfiguration botConfiguration;
    @Value("${register.code}")
    private String registerCode;
    private final EventConsumer eventListener;

    @Autowired
    ApiCalls(EventConsumer eventListener) {
        this.eventListener = eventListener;
    }


    @GetMapping(value = "/registerserver")
    public Server addServer(@RequestParam(value = "name") String name) {
        Server server = new Server();
        server.setName(name);
        serverRepository.save(server);
        return server;
    }

    @PostMapping(value = "/server/editname")
    public ResponseEntity<String> changeNameServer(@RequestParam(value = "id") String id, @RequestParam(value = "name") String name) {
        try {
            Optional<Server> serverOptional = serverRepository.findById(id);
            if (serverOptional.isPresent()) {
                Server server = serverOptional.get();
                server.setName(name);
                serverRepository.save(server);
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping(value = "/server/delete")
    public ResponseEntity<String> deleteServer(@RequestParam(value = "id") String id) {
        try {
            Optional<Server> serverOptional = serverRepository.findById(id);
            if (serverOptional.isPresent()) {
                Server server = serverOptional.get();
                serverRepository.delete(server);
                return ResponseEntity.ok().build();
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }


    @RequestMapping("/servers")
    public List<Server> getServers() {
        return serverRepository.findAll();
    }

    @GetMapping(value = "/nasa/potd/image")
    public @ResponseBody
    ResponseEntity<byte[]> getImageFile(@RequestParam(value = "id") String id) {
        try {
            Optional<Image> imageObj = imageRepository.findById(id);
            if (imageObj.isPresent()) {
                byte[] image = imageObj.get().getImage();
                return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(image);
            }
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping(value = "/nasa/potds")
    public List<Potd> getPotdInfo() {
        return potdRepository.findAll(Sort.by(Sort.Direction.ASC, "date"));
    }

    @GetMapping(value = "/server/wol")
    public String sentPacket(@RequestParam(value = "id") String id) {
        Optional<Server> server = serverRepository.findById(id);
        Nic nic = server.get().getNics().get(1);

        WakeOnLan wakeOnLan = new WakeOnLan();
        return wakeOnLan.sendWOL("192.168.0.255", nic.getAddresses().get("MAC"));
    }

    @GetMapping(value = "/nasa/rovers")
    public List<Rover> getRovers() {
        return roverRepository.findAll();
    }

    @CrossOrigin
    @PostMapping("/authenticate")
    public Token generateToken(@RequestBody AuthRequest authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword())
            );
        } catch (Exception ex) {
            throw new Exception("invalid username/password");
        }
        return new Token(jwtUtils.generateToken(authRequest.getUserName()));
    }

    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @PostMapping(value = "/register")
    public HttpStatus registerUser(@RequestBody Register register) {
        if (register.getRegisterCode().equals(registerCode)) {
            userRepository.save(new User(register.getUserName(), passwordEncoder().encode(register.getPassword()), "1"));
            return HttpStatus.OK;
        } else {
            return HttpStatus.FORBIDDEN;
        }

    }

    @GetMapping(value = "/discord/channels")
    public List<DiscordChannels> getChannels(@RequestParam(value = "id") String id) {
        return botConfiguration.gatewayDiscordClient().getGuildChannels(Snowflake.of(id))
                .map(guildChannel -> new DiscordChannels(guildChannel.getName(), guildChannel.getType().getValue()))
                .collectList()
                .block();
    }

    @PostMapping(value = "/discord/send")
    public HttpStatus sendMessage(@RequestBody SendMessage sendMessage) {
        botConfiguration.gatewayDiscordClient().getChannelById(Snowflake.of(sendMessage.getTextChannelId()))
                .cast(MessageChannel.class)
                .flatMap(channel -> channel.createMessage(sendMessage.getMessage()))
                .block();
        return HttpStatus.OK;
    }

    @Autowired
    private SteamGameRepository steamGameRepository;

    @PostMapping(value = "/steam/add")
    public ResponseEntity<String> addGame(@RequestParam(value = "id") String id, @RequestParam(value = "name") String name) {
        try {
            Optional<SteamGame> steamGamedb = steamGameRepository.findSteamGameByGameId(id);
            if (steamGamedb.isEmpty()) {
                SteamGame steamGame = new SteamGame(id, name);
                RestTemplate restTemplate = new RestTemplate();
                String steam_api_url = String.format("https://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid=%s&count=50&format=json", steamGame.gameId);
                SteamNewsWrapper steamNewsWrapper = restTemplate.getForObject(steam_api_url, SteamNewsWrapper.class);
                steamGame.setLogoUrl(String.format("https://cdn.akamai.steamstatic.com/steam/apps/%s/header.jpg", id));
                steamGameRepository.save(steamGame);
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.ok().build();
            }
        } catch (HttpClientErrorException httpClientErrorException) {
            return ResponseEntity.notFound().build();
        }

    }

    public BBCodeConverter BBCodeParser(){
        BBCodeConverter conv = new BBCodeConverter(true);
        conv.addSimpleTag("[b]","<b>");
        conv.addSimpleTag("\n", "<br>");
        conv.addSimpleTag("[/b]","</b>");
        conv.addSimpleTag("[h3]","<h3>");
        conv.addSimpleTag("[/h3]","</h3>");
        conv.addSimpleTag("[h2]","<h2>");
        conv.addSimpleTag("[/h2]","</h2>");
        conv.addSimpleTag("[list]","<ul>");
        conv.addSimpleTag("[/list]","</ul>");
        conv.addSimpleTag("[*]","<li>");
        conv.addComplexTag("[url=", "<a href=","]",">","[/url]", "</a>" );
        conv.addSimpleTag("[img]{STEAM_CLAN_IMAGE}", "<img src= 'https://cdn.akamai.steamstatic.com/steamcommunity/public/images/clans");
        conv.addSimpleTag("[/img]", "'</img>");
        return conv;
    }

    @GetMapping(value = "/steam/news")
    public List<SteamGame> getNews() {
        List<SteamGame> steamGames = steamGameRepository.findAll();
        RestTemplate restTemplate = new RestTemplate();
        BBCodeConverter bbCodeConverter = BBCodeParser();
        steamGames.forEach( (game) -> {
            String steam_api_url = String.format("https://api.steampowered.com/ISteamNews/GetNewsForApp/v0002/?appid=%s&count=5&format=json", game.gameId);
            SteamNewsWrapper steamNewsWrapper = restTemplate.getForObject(steam_api_url, SteamNewsWrapper.class);
            if (steamNewsWrapper != null) {
                SteamNewsList steamNewsList = steamNewsWrapper.getAppnews();
                steamNewsList.getNewsitems().forEach( (newsItem) -> {
                    if(newsItem.getFeed_type().equals("1")){
                        newsItem.setContents(bbCodeConverter.toHtml(newsItem.getContents()));
                    }
                });
                game.setSteamNewsList(steamNewsList.getNewsitems());
            }
        });
        return steamGames;
    }

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE, value = "/consume")
    public Flux<Event> consume(@RequestParam(value = "id") String id) {
        return eventListener.consume().map(ApiCalls::convertEvent).filter(event -> event.getId().equals(id));
    }

    public static Event convertEvent(String string) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Event event = mapper.readValue(string, Event.class);
            event.setDate(new Date());
            return event;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE, value = "/consume/processes")
    public Flux<Proces[]> consumeProcesses() {
        return eventListener.consumeProcesses().map(ApiCalls::convertProcesses);
    }

    public static Proces[] convertProcesses(String string) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(string, Proces[].class);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

}
