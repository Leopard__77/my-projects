package com.dashboard_backhand.dashboard_backend.rabbitmq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.rabbitmq.AcknowledgableDelivery;
import reactor.rabbitmq.Receiver;

import java.time.Duration;

import static com.dashboard_backhand.dashboard_backend.rabbitmq.EventConfig.QUEUEPROCESSES;

@Service
public class EventConsumer {

    private final Receiver receiver;

    @Autowired
    EventConsumer(Receiver receiver) {
        this.receiver = receiver;
    }

    public Flux<String> consume() {
        return receiver
                .consumeManualAck(QUEUEPROCESSES)
                .doOnNext(AcknowledgableDelivery::ack)
                .delayElements(Duration.ofSeconds(1))
                .flatMap(delivery -> Mono.just(new String(delivery.getBody())));
    }

    public Flux<String> consumeProcesses() {
        return receiver
                .consumeManualAck(QUEUEPROCESSES)
                .doOnNext(AcknowledgableDelivery::ack)
                .delayElements(Duration.ofSeconds(1))
                .flatMap(delivery -> Mono.just(new String(delivery.getBody())));
    }
}