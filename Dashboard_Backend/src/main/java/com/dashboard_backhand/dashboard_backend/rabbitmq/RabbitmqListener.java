package com.dashboard_backhand.dashboard_backend.rabbitmq;

import com.dashboard_backhand.dashboard_backend.models.ConfigProperties;
import com.dashboard_backhand.dashboard_backend.models.server.Server;
import com.dashboard_backhand.dashboard_backend.repositories.ServerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

@Component
public class RabbitmqListener {
    @Autowired
    ServerRepository serverRepository;

    @Autowired
    ConfigProperties configProperties;

    ObjectMapper objectMapper = new ObjectMapper();



    @Bean
    Queue queue() {
        return new Queue(configProperties.getRabbitmqConfig().getQueue(), false);
    }

    @Bean
    Queue queueEvents() {
        return new Queue(configProperties.getRabbitmqConfig().getQueueEvents(), false);
    }

    @Bean
    Queue queueProcesses() {
        return new Queue(configProperties.getRabbitmqConfig().getQueueProcesses(), false);
    }

    @Bean
    DirectExchange directExchange() {
        return new DirectExchange(configProperties.getRabbitmqConfig().getExchange());
    }

    @Bean
    Binding binding(Queue queue, DirectExchange directExchange) {
        return BindingBuilder.bind(queue).to(directExchange).with(configProperties.getRabbitmqConfig().getRoutingKey());
    }

    @Bean
    Binding binding_events(@Qualifier("queueEvents") Queue queue, DirectExchange directExchange) {
        return BindingBuilder.bind(queue).to(directExchange).with(configProperties.getRabbitmqConfig().getRoutingKeyEvents());
    }

    @Bean
    Binding binding_processes(@Qualifier("queueProcesses") Queue queue, DirectExchange directExchange) {
        return BindingBuilder.bind(queue).to(directExchange).with(configProperties.getRabbitmqConfig().getRoutingKeyProcesses());
    }

    @RabbitListener(queues = "#{queue.name}")
    public void receive(byte[] message) throws IOException {
        Server server = objectMapper.readValue(message, Server.class);
        Optional<Server> serverDBopt = serverRepository.findById(server.getId());
        if (serverDBopt.isPresent()) {
            Server serverDB = serverDBopt.get();
            server.setName(serverDB.getName());
            serverRepository.save(server);
        }
    }
}
