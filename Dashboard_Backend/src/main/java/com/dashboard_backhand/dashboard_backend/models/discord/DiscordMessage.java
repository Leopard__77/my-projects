package com.dashboard_backhand.dashboard_backend.models.discord;

public class DiscordMessage {

    private String text;
    private long authorId;

    public DiscordMessage(String text, long authorId) {
        this.text = text;
        this.authorId = authorId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }
}
