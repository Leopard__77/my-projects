package com.dashboard_backhand.dashboard_backend.models.rover;

import java.util.List;

public class RoverImageList {
    public List<RoverImage> photos;

    public List<RoverImage> getPhotos() {
        return photos;
    }
    public RoverImageList(){}
}
