package com.dashboard_backhand.dashboard_backend.models.Steam;

import java.util.HashMap;
import java.util.List;

public class SteamNewsItem {
    public String title, contents;
    public String feed_type;

    public String getFeed_type() {
        return feed_type;
    }

    public void setFeed_type(String feed_type) {
        this.feed_type = feed_type;
    }
    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }
}
