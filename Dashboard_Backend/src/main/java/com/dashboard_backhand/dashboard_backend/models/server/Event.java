package com.dashboard_backhand.dashboard_backend.models.server;

import java.util.Date;

public class Event {
    public String memory_usage;
    public Date date;
    public String id;

    public Event(String id, String memory_usage, Date date) {
        this.id=id;
        this.memory_usage = memory_usage;
        this.date = date;
    }

    public String getMemory_usage() {
        return memory_usage;
    }

    public void setMemory_usage(String memory_usage) {
        this.memory_usage = memory_usage;
    }

    public Date getDate() {
        return date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Event() {
    }
}
