package com.dashboard_backhand.dashboard_backend.models.server;

import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.HashMap;
import java.util.List;

public class Server{

    @MongoId(value = FieldType.OBJECT_ID)
    public String id;
    public String name;
    public String os;
    public String bootTime;
    public String NrOfUpdates;
    public HashMap<String, HashMap<String,String>>cpu;
    public List<Nic> nics;
    public List<Disk>disks;

    public Server(String id, String name){
        this.id = id;
        this.name= name;
    }

    public Server() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getOs() {
        return os;
    }

    public HashMap<String, HashMap<String, String>> getCpu() {
        return cpu;
    }

    public List<Nic> getNics() {
        return nics;
    }

    public List<Disk> getDisks() {
        return disks;
    }
}

