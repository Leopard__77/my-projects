package com.dashboard_backhand.dashboard_backend.models.server;

public class Proces {
    private String cpu_percent;
    private String pid;
    private String name;

    public String getCpu_percent() {
        return cpu_percent;
    }

    public void setCpu_percent(String cpu_percent) {
        this.cpu_percent = cpu_percent;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
