package com.dashboard_backhand.dashboard_backend.models.Steam;

import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

public class SteamGame {
    @MongoId(value = FieldType.OBJECT_ID)
    public String id;
    public String gameId;
    public String name;
    public String logoUrl;
    public List<SteamNewsItem> steamNewsList;


    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public void setSteamNewsList(List<SteamNewsItem> steamNewsList) {
        this.steamNewsList = steamNewsList;
    }

    public SteamGame(String gameId, String name) {
        this.gameId = gameId;
        this.name = name;
    }
}
