package com.dashboard_backhand.dashboard_backend.models.server;

import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

public class Image {
    @MongoId(value = FieldType.OBJECT_ID)
    public String id;
    public byte[] image;

    public Image(byte[] image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public byte[] getImage() {
        return image;
    }
}
