package com.dashboard_backhand.dashboard_backend.models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@ConfigurationProperties("api")
public class ConfigProperties {
    private String nasakey;
    private String discordToken;
    @Autowired
    private RabbitmqConfig rabbitmqConfig;

    public String getDiscordToken() {
        return discordToken;
    }

    public void setDiscordToken(String discordToken) {
        this.discordToken = discordToken;
    }



    public RabbitmqConfig getRabbitmqConfig() {
        return rabbitmqConfig;
    }

    public void setRabbitmqConfig(RabbitmqConfig rabbitmqConfig) {
        this.rabbitmqConfig = rabbitmqConfig;
    }

    public String getNasakey() {
        return nasakey;
    }

    public void setNasakey(String nasakey) {
        this.nasakey = nasakey;
    }


}
