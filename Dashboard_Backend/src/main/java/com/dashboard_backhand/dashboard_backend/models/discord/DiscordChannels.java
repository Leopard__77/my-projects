package com.dashboard_backhand.dashboard_backend.models.discord;

public class DiscordChannels {

    private String name;
    private int type;

    public DiscordChannels(String name, int type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
