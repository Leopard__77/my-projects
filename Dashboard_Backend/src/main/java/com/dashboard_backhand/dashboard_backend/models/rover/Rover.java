package com.dashboard_backhand.dashboard_backend.models.rover;

import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.HashMap;
import java.util.List;

public class Rover {
    @MongoId(value = FieldType.OBJECT_ID)
    public String id;
    public String name, landing_date, launch_date, status, max_date;
    public int max_sol,total_photos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImagesIds(HashMap<String, List<String>> imagesIds) {
        this.imagesIds = imagesIds;
    }

    public HashMap<String,List<String>>imagesIds;
    public Rover(){}
}
