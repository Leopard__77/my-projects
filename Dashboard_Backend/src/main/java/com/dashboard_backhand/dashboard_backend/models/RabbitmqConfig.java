package com.dashboard_backhand.dashboard_backend.models;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@ConfigurationProperties("rabbitmq")
public class RabbitmqConfig {
    private String exchange;
    private String queue;
    private String routingKey;
    private String queueEvents;
    private String routingKeyEvents;
    private String queueProcesses;
    private String routingKeyProcesses;

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public String getQueueEvents() {
        return queueEvents;
    }

    public void setQueueEvents(String queueEvents) {
        this.queueEvents = queueEvents;
    }

    public String getRoutingKeyEvents() {
        return routingKeyEvents;
    }

    public void setRoutingKeyEvents(String routingKeyEvents) {
        this.routingKeyEvents = routingKeyEvents;
    }

    public String getQueueProcesses() {
        return queueProcesses;
    }

    public void setQueueProcesses(String queueProcesses) {
        this.queueProcesses = queueProcesses;
    }

    public String getRoutingKeyProcesses() {
        return routingKeyProcesses;
    }

    public void setRoutingKeyProcesses(String routingKeyProcesses) {
        this.routingKeyProcesses = routingKeyProcesses;
    }
}

