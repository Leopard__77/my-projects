package com.dashboard_backhand.dashboard_backend.models.server;

import java.util.HashMap;

public class Nic{
    public String name;
    public HashMap<String,String> addresses;

    public String getName() {
        return name;
    }

    public HashMap<String, String> getAddresses() {
        return addresses;
    }
}

