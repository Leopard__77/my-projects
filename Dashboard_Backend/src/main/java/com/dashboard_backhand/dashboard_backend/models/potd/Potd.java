package com.dashboard_backhand.dashboard_backend.models.potd;

import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

public class Potd{
    @MongoId(value = FieldType.OBJECT_ID)
    public String id;
    public String date,title,explanation,hdurl, media_type, image_id;
    public Potd(){

    }
    public String getId() {
        return id;
    }
    public String getDate() {
        return date;
    }

    public String getHdurl() {
        return hdurl;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }
}
