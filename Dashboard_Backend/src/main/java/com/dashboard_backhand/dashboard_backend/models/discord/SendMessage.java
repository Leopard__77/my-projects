package com.dashboard_backhand.dashboard_backend.models.discord;

public class SendMessage {

    private String message;
    private String textChannelId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getTextChannelId() {
        return textChannelId;
    }

    public void setTextChannelId(String textChannelId) {
        this.textChannelId = textChannelId;
    }
}
