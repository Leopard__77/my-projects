package com.dashboard_backhand.dashboard_backend.config;

import discord4j.core.DiscordClientBuilder;
import discord4j.core.GatewayDiscordClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BotConfiguration {


    @Value("${token}")
    private String token;

    GatewayDiscordClient client = null;

    @Bean
    public GatewayDiscordClient gatewayDiscordClient() {
        return client = DiscordClientBuilder.create(token)
                .build()
                .login()
                .block();

    }
}