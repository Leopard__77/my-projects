package com.dashboard_backhand.dashboard_backend;

import com.dashboard_backhand.dashboard_backend.models.ConfigProperties;
import com.dashboard_backhand.dashboard_backend.models.potd.Potd;
import com.dashboard_backhand.dashboard_backend.models.rover.Rover;
import com.dashboard_backhand.dashboard_backend.models.rover.RoverImage;
import com.dashboard_backhand.dashboard_backend.models.rover.RoverImageList;
import com.dashboard_backhand.dashboard_backend.models.rover.RoverWrapper;
import com.dashboard_backhand.dashboard_backend.models.server.Image;
import com.dashboard_backhand.dashboard_backend.repositories.ImageRepository;
import com.dashboard_backhand.dashboard_backend.repositories.PotdRepository;
import com.dashboard_backhand.dashboard_backend.repositories.RoverRepository;
import com.dashboard_backhand.dashboard_backend.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.*;

@Component
public class ScheduledTasks {
    @Autowired
    PotdRepository potdRepository;

    @Autowired
    ImageRepository imageRepository;
    @Autowired
    UserRepository userRepository;

    @Autowired
    ConfigProperties configProperties;

    @Scheduled(cron = "0 0 8 * * *")
//    @Scheduled(cron = "0 */1 * * * *")
    public void getPotd() throws IOException {
        final String url = String.format("https://api.nasa.gov/planetary/apod?api_key=%s", configProperties.getNasakey());
        RestTemplate restTemplate = new RestTemplate();
        Potd potd = restTemplate.getForObject(url, Potd.class);
        assert potd != null;

        URL imageUrl = new URL(potd.getHdurl());
        BufferedImage bufferedImage = ImageIO.read(imageUrl);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "jpg",byteArrayOutputStream);
        Image image = new Image(byteArrayOutputStream.toByteArray());
        imageRepository.save(image);
        potd.setImage_id(image.getId());
        potdRepository.save(potd);
    }

    @Autowired
    RoverRepository roverRepository;

    @Scheduled(cron = "0 17 17 * * *")
    public void getRover() {

        final List<String> rovers = Arrays.asList("Curiosity", "Spirit", "Opportunity", "Perseverance");
        RestTemplate restTemplate = new RestTemplate();
        for (String roverName: rovers){
            String url = String.format("https://api.nasa.gov/mars-photos/api/v1/manifests/%s?&api_key=%s", roverName,configProperties.getNasakey());
            RoverWrapper roverWrapper = restTemplate.getForObject(url,RoverWrapper.class);
            if(roverWrapper != null){
                Rover rover = roverWrapper.getPhoto_manifest();
                Optional<Rover> RoverRepo = roverRepository.findByName(roverName);
                RoverRepo.ifPresent(value -> rover.setId(value.getId()));

                HashMap<String,List<String>> imagesHashmap = new HashMap<>();
                String image_url = String.format("https://api.nasa.gov/mars-photos/api/v1/rovers/%s/photos?earth_date=%s&api_key=%s",rover.name, rover.max_date, configProperties.getNasakey());
                RoverImageList roverImageList = restTemplate.getForObject(image_url, RoverImageList.class);
                List<String> stringListImages = new ArrayList<>();
                roverImageList.getPhotos().forEach( (n) -> stringListImages.add((n.getImg_src())));
                imagesHashmap.put(rover.max_date, stringListImages);
                rover.setImagesIds(imagesHashmap);

                roverRepository.save(rover);
            }
        }

    }
}
