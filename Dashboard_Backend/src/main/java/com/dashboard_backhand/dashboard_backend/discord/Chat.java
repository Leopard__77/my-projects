package com.dashboard_backhand.dashboard_backend.discord;

import com.dashboard_backhand.dashboard_backend.config.BotConfiguration;
import com.dashboard_backhand.dashboard_backend.models.discord.DiscordMessage;
import discord4j.common.util.Snowflake;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.MessageChannel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;

@RestController
public class Chat {

    @Autowired
    private BotConfiguration botConfiguration;

    public static List<Message> getMessagesOfChannel(MessageChannel channel, int limit) {
        Snowflake now = Snowflake.of(Instant.now());
        return channel.getMessagesBefore(now).sort(Comparator.comparing(Message::getTimestamp)).take(limit).collectList().block();
    }


    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE, value = "/chat")
    Flux<DiscordMessage> chat(@RequestParam(value = "id") String id, @RequestParam(value = "limit") int limit) {
        List<Message> messageList = getMessagesOfChannel(botConfiguration.gatewayDiscordClient().getChannelById(Snowflake.of(id)).cast(MessageChannel.class).block(), limit);
        return Flux.fromStream(messageList.stream().map(message -> new DiscordMessage(message.getContent(), message.getAuthorAsMember().map(member -> member.getId().asLong()).block())))
                .concatWith(
                        botConfiguration.gatewayDiscordClient().getEventDispatcher().on(MessageCreateEvent.class)
                                .map(MessageCreateEvent::getMessage)
                                .filter(message -> message.getChannelId().asString().equals(id))
                                .map(message -> new DiscordMessage(message.getContent(), message.getAuthorAsMember().map(member -> member.getId().asLong()).block()))
                );
    }
}
