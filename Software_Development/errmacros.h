#ifndef __errmacros_h__
#define __errmacros_h__
// afkomstig van voorbeeld code hoorcolleges
#include <errno.h>
#include "./lib/tcpsock.h"
#define SYSCALL_ERROR(err) 									\
		do {												\
			if ( (err) == -1 )								\
			{												\
				perror("Error executing syscall");			\
				exit( EXIT_FAILURE );						\
			}												\
		} while(0)

#define MALLOC_ERROR(err) 									\
		do {												\
			if ( (err) == NULL )								\
			{												\
				perror("Error allocating memory");			\
				exit( EXIT_FAILURE );						\
			}												\
		} while(0)

#define CHECK_MKFIFO(err) 									\
		do {												\
			if ( (err) == -1 )								\
			{												\
				if ( errno != EEXIST )						\
				{											\
					perror("Error executing mkfifo");		\
					exit( EXIT_FAILURE );					\
				}											\
			}												\
		} while(0)
		
#define FILE_OPEN_ERROR(fp) 								\
		do {												\
			if ( (fp) == NULL )								\
			{												\
				perror("File open failed");					\
				exit( EXIT_FAILURE );						\
			}												\
		} while(0)

#define FILE_CLOSE_ERROR(err) 								\
		do {												\
			if ( (err) == -1 )								\
			{												\
				perror("File close failed");				\
				exit( EXIT_FAILURE );						\
			}												\
		} while(0)

#define ASPRINTF_ERROR(err) 								\
		do {												\
			if ( (err) == -1 )								\
			{												\
				perror("asprintf failed");					\
				exit( EXIT_FAILURE );						\
			}												\
		} while(0)

#define FFLUSH_ERROR(err) 								\
		do {												\
			if ( (err) == EOF )								\
			{												\
				perror("fflush failed");					\
				exit( EXIT_FAILURE );						\
			}												\
		} while(0)
#define INCORRECT_ARG(err) 								\
		do {												\
			if (err != 2)								\
			{												\
				perror("incorrect number of arguments");					\
				exit( EXIT_FAILURE );						\
			}                                			\
		} while(0)
#define INCORRECT_PORT(err) 								\
		do {												\
			if (err == 0)								\
			{												\
				perror("Use integer for PORT");					\
				exit( EXIT_FAILURE );						\
			}                                			\
		} while(0)
#define TCP_ERROR_HANDLER(err) 								\
		do {												\
			if (err != TCP_NO_ERROR)								\
			{												\
				perror("Error with TCP");					\
				exit( EXIT_FAILURE );						\
			}                              \
		} while(0)
#define RW_LOCK_ERROR_HANDLER(err) 								\
		do {												\
			if (err != 0)								\
			{												\
				perror("An error occurred with the rwlock"); \
				exit( EXIT_FAILURE );						\
			}                              \
		} while(0)
#define COND_VAR_ERROR_HANDLER(err) 								\
		do {												\
			if (err != 0)								\
			{												\
				perror("An error occurred with the conditional variable"); \
				exit( EXIT_FAILURE );						\
			}                              \
		} while(0)
#define MUTEX_ERROR_HANDLER(err) 								\
		do {												\
			if (err != 0)								\
			{												\
				perror("An error occurred with the mutex"); \
				exit( EXIT_FAILURE );						\
			}                              \
		} while(0)
#define INITIALISATION_ERROR_POLL_FDS(err) 								\
		do {												\
			if (err <= 0)								\
			{												\
				perror("Could not initialize poll fds"); \
				exit( EXIT_FAILURE );						\
			}                              \
		} while(0)
#define SQL_ERROR(err) 								\
		do {												\
			if (err != NULL)								\
			{												\
				perror("An error occurred with SQL"); \
				exit( EXIT_FAILURE );						\
			}                              \
		} while(0)
#endif
