//
// Created by yari on 27/11/20.
//

#define _GNU_SOURCE //The function asprintf() is not yet part of the C Standard https://stackoverflow.com/questions/61306157/unable-to-compile-program-due-to-function-asprintf

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "lib/tcpsock.h"
#include "connmgr.h"
#include <poll.h>
#include "sbuffer.h"

void *callback_copy(void *src_element) {
    connection_info_t *copy = malloc(sizeof(connection_info_t));
    connection_info_t *connection = (connection_info_t *) src_element;
    copy->socket_id = connection->socket_id;
    copy->last_activity = connection->last_activity;
    copy->sensor_id = connection->sensor_id;
    copy->allowed_to_insert_data = connection->allowed_to_insert_data;
    return NULL;

}

void callback_free(void **element) {
    connection_info_t *connection_info = (connection_info_t *) *element;
    free(connection_info);
    connection_info = NULL;
}

int callback_compare(void *x, void *y) {
    connection_info_t *connection_info = (connection_info_t *) x;
    int *fd = (int *) y;
    if (*fd == connection_info->poll_fd) {
        return 0;
    } else {
        return -1;
    }
}

void connmgr_free(server_info_t** server) {
    dpl_free( &( (*server)->list_of_connections ), false);
    tcp_close( &(*server)->socket_id);
    free( (*server)->poll_fds);
    free(*server);
}

void update_server_last_activity(server_info_t *server) {
    server->last_activity = time(NULL);
}

void remove_connection(server_info_t *server, connection_info_t *connection, int connection_index, int poll_index) {
    tcpsock_t *tcpsock = connection->socket_id;
    int tcp_result = (tcp_close(&tcpsock));  //close socket descriptor
    TCP_ERROR_HANDLER(tcp_result);
    dpl_remove_at_index(server->list_of_connections, connection_index, true);
    server->poll_fds[poll_index].fd = -1;
}

int check_if_sensor_is_already_connected(dplist_t *list_of_connections, sensor_data_t data) {
    int index = 0;
    int nr_of_matching_ids_found = 0;
    while (index < dpl_size(list_of_connections)){
        connection_info_t  * connection = dpl_get_element_at_index(list_of_connections, index);
        if (connection->sensor_id == data.id){
            nr_of_matching_ids_found++;
        }
        index++;
    }
    if(nr_of_matching_ids_found == 0){
        return CONN_MGR_FAILURE;
    }
    else{
        return CONN_MGR_SUCCES;
    }
}


int initialise_poll_fds(struct pollfd* poll_fds, int old_nr_of_fds, int new_nr_of_fds){
    if(new_nr_of_fds>old_nr_of_fds){
        for(int i = old_nr_of_fds; i<new_nr_of_fds; i++){
            poll_fds[i].fd = -1;
            poll_fds[i].events = 0;
            DEBUG_ALL_PRINT("SET i:%d to %d",i, poll_fds[i].fd);
        }
        return CONN_MGR_SUCCES;
    }
    else if( new_nr_of_fds<= old_nr_of_fds){
        return NO_INITIALISATION_NEEDED;
    }
    else{
        return CONN_MGR_FAILURE;
    }
}

struct pollfd * reallocate_poll_fds(server_info_t* server){
    struct pollfd * bigger_poll_fds = realloc(server->poll_fds, sizeof(struct pollfd) * FACTOR_FOR_REALLOCTING*100);
    MALLOC_ERROR(bigger_poll_fds);

    int nr_of_old_fds = server->nr_of_fds;
    int nr_of_fds_after_realloc = (server->nr_of_fds) * FACTOR_FOR_REALLOCTING;
    INITIALISATION_ERROR_POLL_FDS(initialise_poll_fds(bigger_poll_fds, nr_of_old_fds, nr_of_fds_after_realloc));
    server->nr_of_fds = nr_of_fds_after_realloc;
    server->poll_fds = bigger_poll_fds;
    return NULL;
}

int check_for_available_poll_fds(server_info_t * server){
    int found = 0;
    int index = 0;
    while (found != 1 && index < server->nr_of_fds){
        if(server->poll_fds[index].fd == -1){
            found=1;
        }
        else{
            index++;
        }
    }
    if(found == 1){
        return index;
    }
    else{
        return -1;
    }
}

int get_index_available_poll_fd(server_info_t *server){
    int index_available_poll_fd = check_for_available_poll_fds(server);
    DEBUG_ALL_PRINT("Index of available poll_fd: %d", index_available_poll_fd);
    if(index_available_poll_fd == -1){
        reallocate_poll_fds(server);
        index_available_poll_fd = check_for_available_poll_fds(server);
        DEBUG_ALL_PRINT("Index of available poll_fd after realloc: %d", index_available_poll_fd);
    }
    return index_available_poll_fd;
}

server_info_t *setup_server(int PORT) {
    tcpsock_t *server = NULL;
    server_info_t *server_info = malloc(sizeof(server_info_t));
    MALLOC_ERROR(server_info);
    struct pollfd *poll_fds = malloc(sizeof(struct pollfd)*NR_OF_FDS_AT_START);
    MALLOC_ERROR(poll_fds);
    INITIALISATION_ERROR_POLL_FDS(initialise_poll_fds(poll_fds, 0, NR_OF_FDS_AT_START));

    int tcp_result = tcp_passive_open(&server, PORT); //create server
    TCP_ERROR_HANDLER(tcp_result);
    tcp_result = tcp_get_sd(server, &(poll_fds[0].fd)); //get socket descriptor
    TCP_ERROR_HANDLER(tcp_result);
    dplist_t *list_of_connections = dpl_create(callback_copy, callback_free, callback_compare);
    
    server_info->nr_of_fds = NR_OF_FDS_AT_START;
    server_info->poll_fds = poll_fds;
    server_info->poll_fds[0].events = POLLIN;    //set requested events POLLIN = data available to read
    server_info->socket_id = server;
    server_info->last_activity = time(NULL);
    server_info->list_of_connections = list_of_connections;
    server_info->server_status = 1;

    DEBUG_ALL_PRINT("Server has started, running on port: %d", PORT);
    return server_info;
}

void connmgr_listen(server_info_t *server, struct sbuffer *buffer) {
    dplist_t *list_of_connections = server->list_of_connections;
    tcpsock_t *sensor_connection = NULL;
    int poll_result, tcp_result;
    while (server->server_status == 1 && get_writer_thread_status(buffer) == 1) {
        poll_result = poll(server->poll_fds, server->nr_of_fds, 0);      //aantal connecties + server(1)
        SYSCALL_ERROR(poll_result);

        if (server->poll_fds[0].revents == POLLIN) {   //server and there is data available to read ->new sensor
            connection_info_t *new_sensor_connection = malloc(sizeof(connection_info_t));
            tcp_result = tcp_wait_for_connection(server->socket_id, &sensor_connection);
            TCP_ERROR_HANDLER(tcp_result);
            int index_of_available_fd = get_index_available_poll_fd(server);
            tcp_result = tcp_get_sd(sensor_connection, &(server->poll_fds[index_of_available_fd].fd));
            TCP_ERROR_HANDLER(tcp_result);

            server->poll_fds[index_of_available_fd].events = POLLIN;
            new_sensor_connection->socket_id = sensor_connection;
            new_sensor_connection->poll_fd = server->poll_fds[index_of_available_fd].fd;
            new_sensor_connection->last_activity = time(NULL);
            dpl_insert_at_index(list_of_connections, (void *) new_sensor_connection, dpl_size(list_of_connections), false);  //add new sensor at the end of the list
            connection_info_t *newest_sensor = dpl_get_element_at_index(list_of_connections, dpl_size(list_of_connections)); //get last added sensor
            newest_sensor->sensor_id = 0; //otherwise "conditional jump or move depends on uninitialised value(s)" on line 108
            newest_sensor->allowed_to_insert_data = 0;
            DEBUG_ALL_PRINT("New sensor made a connection. Number of connected sensors: %d", dpl_size(list_of_connections));
            update_server_last_activity(server);
        }
        for (int poll_index = 1; poll_index < server->nr_of_fds; ++poll_index) {
            int connection_index = dpl_get_index_of_element(list_of_connections, &server->poll_fds[poll_index].fd);
            connection_info_t *current_connection = dpl_get_element_at_index(list_of_connections, connection_index);

            if (current_connection != NULL && current_connection->last_activity + TIMEOUT < time(NULL)) {
                DEBUG_ALL_PRINT("Sensor with ID: %"PRIu16 " timed out", current_connection->sensor_id);
                server->poll_fds[poll_index].fd = -1;
                remove_connection(server, current_connection, connection_index, poll_index);
                update_server_last_activity(server);
            }
            else if ( current_connection != NULL && server->poll_fds[poll_index].revents == POLLIN) {   //sensor sends data
                sensor_data_t data;
                // read sensor ID
                int bytes = sizeof(data.id);
                tcp_receive(current_connection->socket_id, (void *) &data.id, &bytes);
                // read temperature
                bytes = sizeof(data.value);
                tcp_receive(current_connection->socket_id, (void *) &data.value, &bytes);
                // read timestamp
                bytes = sizeof(data.ts);
                tcp_result = tcp_receive(current_connection->socket_id, (void *) &data.ts, &bytes);
                update_server_last_activity(server);
                current_connection->last_activity = data.ts;
                char *log_buffer;
                if ((tcp_result == TCP_NO_ERROR) && bytes != 0) {
                    if (current_connection->sensor_id == 0 && check_if_sensor_is_already_connected(list_of_connections, data) == CONN_MGR_FAILURE) { //check if connection already has a sensor id defined
                        current_connection->sensor_id = data.id;
                        current_connection->allowed_to_insert_data = 1;
                        asprintf(&log_buffer, "A new sensor node with ID %"PRIu16" has opened a new connection", current_connection->sensor_id);
                        write_log(log_buffer);
                        free(log_buffer);
                    }
                    if (current_connection->allowed_to_insert_data == 1) {
                        sensor_data_t *malloced_data = malloc(sizeof(sensor_data_t));
                        MALLOC_ERROR(malloced_data);
                        malloced_data->id = data.id;
                        malloced_data->ts = data.ts;
                        malloced_data->value = data.value;
                        malloced_data->read_by_sensor_db = 0;
                        malloced_data->read_by_datamgr = 0;
                        DEBUG_ALL_PRINT("sensor id = %"PRIu16" - temperature = %f - timestamp = %ld", malloced_data->id, malloced_data->value, (long int) malloced_data->ts);
                        sbuffer_insert(buffer, malloced_data);
                    }
                    else {
                        DEBUG_ALL_PRINT("Sensor with ID: %"PRIu16 " is already connected", data.id);
                        asprintf(&log_buffer, "A sensor with ID: %"PRIu16 "is already connected", data.id);
                        write_log(log_buffer);
                        free(log_buffer);
                        remove_connection(server, current_connection, connection_index, poll_index);
                    }
                }
                else if (tcp_result == TCP_CONNECTION_CLOSED) {
                    DEBUG_ALL_PRINT("Sensor with ID: %"PRIu16" closed the connection", current_connection->sensor_id);
                    asprintf(&log_buffer, "The sensor node with ID %"PRIu16" has closed the connection", current_connection->sensor_id);
                    write_log(log_buffer);
                    free(log_buffer);
                    remove_connection(server, current_connection, connection_index, poll_index);
                    DEBUG_ALL_PRINT("Remaining sensors %d", dpl_size(list_of_connections));

                }
                else if (tcp_result != TCP_NO_ERROR) {
                    DEBUG_ALL_PRINT("An error occurred: %d\n", tcp_result);
                    TCP_ERROR_HANDLER(tcp_result);
                }
            }
        }
        if (server->last_activity + TIMEOUT < time(NULL)) {
            server->server_status = 0;
            server->poll_fds[0].fd = -1;
            DEBUG_ALL_PRINT("Server timed out and is shutting down...");
        }
    }
}


