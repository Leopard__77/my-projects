#define _GNU_SOURCE //The function asprintf() is not yet part of the C Standard https://stackoverflow.com/questions/61306157/unable-to-compile-program-due-to-function-asprintf
#include <sys/types.h>
#include <wait.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <pthread.h>
#include "config.h"
#include "connmgr.h"
#include "sbuffer.h"
#include "datamgr.h"
#include "sensor_db.h"
#include <sqlite3.h>
#include "main.h"

logging_info_t logging_info;

typedef struct {
    int PORT;
    sbuffer_t * buffer;
    FILE*sensor_map;
} thread_args_t;

void setup_log_proces(void){
    int result;
    result = mkfifo(FIFO_NAME, 06660);
    CHECK_MKFIFO(result);
}

void log_proces (){
    FILE *fp_reader;
    setup_log_proces();
    char *str_result;
    char recv_buf[LOG_WRITER_BUFFER];
    fp_reader = fopen(FIFO_NAME, "r");
    FILE_OPEN_ERROR(fp_reader);
    int status_log_process = 1;
    do
    {
        str_result = fgets(recv_buf, LOG_WRITER_BUFFER, fp_reader);
        if ( str_result != NULL )
        {
            fprintf(stdout, "%s", recv_buf);
            if (strstr(recv_buf, END_OF_LOG_MESSAGE) != NULL){
                status_log_process = 0;
            }
        }
    } while (str_result != NULL && status_log_process == 1);
    DEBUG_ALL_PRINT("Child process exiting ...");
    int result = fclose( fp_reader );
    FILE_CLOSE_ERROR(result);
    exit(0);
}

void write_log(char message[]){
    char *send_buffer = NULL;
    time_t timer = time(NULL);
    struct tm *tm_info = localtime(&timer);
    char buffer[LOG_WRITER_BUFFER];
    strftime(buffer, LOG_WRITER_BUFFER, "%H:%M:%S", tm_info);
    logging_info.sequence++;
    asprintf( &send_buffer,"%d. (%s) %s\n", logging_info.sequence, buffer, message);
    pthread_mutex_lock(&logging_info.mutex);
    int result = fputs( send_buffer, logging_info.writer_fp );
    pthread_mutex_unlock(&logging_info.mutex);
    DEBUG_ALL_PRINT("%s", send_buffer);
    if ( result == EOF )
    {
        DEBUG_ALL_PRINT("Error writing data to fifo");
        exit( EXIT_FAILURE );
    }
    FFLUSH_ERROR(fflush(logging_info.writer_fp));
    free( send_buffer );
}



void *thread_connmgr(void* arg){

    thread_args_t *thread_args = (thread_args_t*) arg;
    server_info_t * server = setup_server(thread_args->PORT);
    connmgr_listen(server, thread_args->buffer);
    connmgr_free(&server);
    set_writer_thread_status(thread_args->buffer, 0);
    DEBUG_ALL_PRINT("Connection mgr ended: %d", get_writer_thread_status(thread_args->buffer));
    return NULL;
}

void*thread_datamgr(void*arg){
    thread_args_t *thread_args = (thread_args_t*) arg;

    datamgr_calculate_avg_from_buffer(thread_args->sensor_map, thread_args->buffer);
    datamgr_free();
    DEBUG_ALL_PRINT("Data mgr ended");
    return NULL;
}

void* thread_sensor_db(void*args){
    thread_args_t *thread_args = (thread_args_t*) args;
    char clear_up = 0;
    DBCONN* db = init_connection(clear_up);
    if(db == NULL){ //connection failed
        set_writer_thread_status(thread_args->buffer, 0);
        set_datamgr_thread_status(thread_args->buffer, 0);
    }
    else {
        insert_sensor_from_buffer(db, thread_args->buffer);
    }
    disconnect(db);
    DEBUG_ALL_PRINT("Sensor_db ended");
    return NULL;
}


int main (int argc, char*argv[] ) {
    pid_t child_pid;
    int child_exit_status;
    pthread_mutex_init(&(logging_info.mutex), NULL);
    child_pid = fork();
    SYSCALL_ERROR(child_pid);
    if (child_pid == 0){
        log_proces();
    }
    else {

        INCORRECT_ARG(argc);
        int PORT = atoi(argv[1]);
        INCORRECT_PORT(PORT);
        logging_info.writer_fp = fopen(FIFO_NAME, "w");
        logging_info.sequence = 0;
        FILE_OPEN_ERROR(logging_info.writer_fp);
        FILE *sensor_map = fopen("room_sensor.map", "r");
        FILE_OPEN_ERROR(sensor_map);
        thread_args_t thread_args;
        thread_args.PORT = PORT;
        sbuffer_t *buffer;
        sbuffer_init(&buffer);
        thread_args.buffer = buffer;
        thread_args.sensor_map = sensor_map;
        pthread_t thread_connmgr_ID, thread_datamgr_ID, thread_sensor_db_ID;
        void *thread_connmgr_result, *thread_datamgr_result,*thread_sensor_db_result;

        //create the 3 threads
        set_datamgr_thread_status(buffer, 1);
        set_writer_thread_status(buffer, 1);
        pthread_create(&thread_connmgr_ID, NULL, thread_connmgr, &thread_args);
        pthread_create(&thread_datamgr_ID, NULL, thread_datamgr, &thread_args);
        pthread_create(&thread_sensor_db_ID, NULL, thread_sensor_db, &thread_args);
        //close threads

        pthread_join(thread_datamgr_ID, &thread_datamgr_result);
        pthread_join(thread_sensor_db_ID, &thread_sensor_db_result);
        pthread_join(thread_connmgr_ID, &thread_connmgr_result);
        write_log(END_OF_LOG_MESSAGE);
        DEBUG_ALL_PRINT("Buffer size: %d", get_buffer_size(buffer));

        SYSCALL_ERROR( waitpid(child_pid, &child_exit_status, 0));
        if ( WIFEXITED(child_exit_status))
        {
            DEBUG_ALL_PRINT("Log process %d terminated with exit status %d\n", child_pid, WEXITSTATUS(child_exit_status));
        }
        else
        {
            DEBUG_ALL_PRINT("Log process %d terminated abnormally with status %d\n", child_pid, WEXITSTATUS(child_exit_status));
        }
        FILE_CLOSE_ERROR(fclose(sensor_map));
        FILE_CLOSE_ERROR(fclose(logging_info.writer_fp));
        sbuffer_free(&buffer);
        exit(EXIT_SUCCESS);
   }

}
