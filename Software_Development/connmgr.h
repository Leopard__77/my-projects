//
// Created by yari on 27/11/20.
//

#ifndef __TCP_SERVER_H__
#define __TCP_SERVER_H__

#include <stdlib.h>
#include <stdio.h>
#include <poll.h>
#include <time.h>
#include "config.h"
#include "./lib/tcpsock.h"
#include "sbuffer.h"
#ifndef TIMEOUT
#error TIMEOUT not set
#endif

#define CONN_MGR_SUCCES 1
#define CONN_MGR_FAILURE (-1)
#define NO_INITIALISATION_NEEDED 0
#define NR_OF_FDS_AT_START 2
#define FACTOR_FOR_REALLOCTING 2

typedef struct{
    struct pollfd *poll_fds;
    time_t last_activity;
    tcpsock_t* socket_id;
    dplist_t * list_of_connections;
    int nr_of_fds;
    int server_status;
} server_info_t;

typedef struct{
    int poll_fd;
    time_t last_activity;
    tcpsock_t* socket_id;
    sensor_id_t sensor_id;
    int allowed_to_insert_data;
} connection_info_t;
server_info_t * setup_server(int PORT);
void connmgr_listen(server_info_t * server, sbuffer_t * buffer);
void connmgr_free(server_info_t ** server);
#endif //LAB7_TCP_SEVER_H
