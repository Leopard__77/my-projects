/**
 * \author Yari Nys
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "dplist.h"

/*
 * definition of error codes
 * */
#define DPLIST_NO_ERROR 0
#define DPLIST_MEMORY_ERROR 1 // error due to mem alloc failure
#define DPLIST_INVALID_ERROR 2 //error due to a list operation applied on a NULL list 

#ifdef DEBUG
#define DEBUG_PRINTF(...) 									                                        \
        do {											                                            \
            fprintf(stderr,"\nIn %s - function %s at line %d: ", __FILE__, __func__, __LINE__);	    \
            fprintf(stderr,__VA_ARGS__);								                            \
            fflush(stderr);                                                                         \
                } while(0)
#else
#define DEBUG_PRINTF(...) (void)0
#endif


#define DPLIST_ERR_HANDLER(condition, err_code)                         \
    do {                                                                \
            if ((condition)) DEBUG_PRINTF(#condition " failed\n");      \
            assert(!(condition));                                       \
        } while(0)


/*
 * The real definition of struct list / struct node
 */

struct dplist_node {
    dplist_node_t *prev, *next;
    void *element;
};

struct dplist {
    dplist_node_t *head;

    void *(*element_copy)(void *src_element);

    void (*element_free)(void **element);

    int (*element_compare)(void *x, void *y);
};

int adjust_index(int index, dplist_t * list){
        int adjusted_index = 0;
        int size = dpl_size(list);
        if(index < 0){
                adjusted_index = 0;
        }
        else if (index > size){
                adjusted_index = size;
        }
        else{
                adjusted_index = index;
        }
        return adjusted_index;
}


dplist_t *dpl_create(// callback functions
        void *(*element_copy)(void *src_element),
        void (*element_free)(void **element),
        int (*element_compare)(void *x, void *y)
) {
    dplist_t *list;
    list = malloc(sizeof(struct dplist));
    DPLIST_ERR_HANDLER(list == NULL, DPLIST_MEMORY_ERROR);
    list->head = NULL;
    list->element_copy = element_copy;
    list->element_free = element_free;
    list->element_compare = element_compare;
    return list;
}

void dpl_free(dplist_t **list, bool free_element) {

    //TODO: add your code here
	if(*list == NULL){ //aka null list
		*list = NULL;
	}
	else{
		dplist_node_t * node = (*list)->head,* prev_node;
		while(node != NULL ){
			prev_node = node;
			node = node->next;
			if(free_element){
				(*list)->element_free(&prev_node->element);
			}
			free(prev_node);
		}
	}
	free(*list);
	*list = NULL;

}

dplist_t *dpl_insert_at_index(dplist_t *list, void *element, int index, bool insert_copy) {

    //TODO: add your code here
    dplist_node_t *ref_at_index, *list_node;
    if (list == NULL) return NULL;

    list_node = malloc(sizeof(dplist_node_t));
    DPLIST_ERR_HANDLER(list_node == NULL, DPLIST_MEMORY_ERROR);
    if(insert_copy){
    	if(element == NULL){list_node->element = element;}
    	else{
		list_node->element = list->element_copy(element);
	}
    }
    else{
    	list_node->element = element;
	}
    // pointer drawing breakpoint
    if (list->head == NULL) { // covers case 1
        list_node->prev = NULL;
        list_node->next = NULL;
        list->head = list_node;
        // pointer drawing breakpoint
    } else if (index <= 0) { // covers case 2
        list_node->prev = NULL;
        list_node->next = list->head;
        list->head->prev = list_node;
        list->head = list_node;
        // pointer drawing breakpoint
    } else {
        ref_at_index = dpl_get_reference_at_index(list, index);
        assert(ref_at_index != NULL);
        // pointer drawing breakpoint
        if (index < dpl_size(list)) { // covers case 4
            list_node->prev = ref_at_index->prev;
            list_node->next = ref_at_index;
            ref_at_index->prev->next = list_node;
            ref_at_index->prev = list_node;
            // pointer drawing breakpoint
        } else { // covers case 3
            assert(ref_at_index->next == NULL);
            list_node->next = NULL;
            list_node->prev = ref_at_index;
            ref_at_index->next = list_node;
            // pointer drawing breakpoint
        }
    }
    return list;

}

dplist_t *dpl_remove_at_index(dplist_t *list, int index, bool free_element) {

    //TODO: add your code here
        if(list==NULL){
                return NULL;
        }
        else if(list->head==NULL){
                return list;
        }
        else{
                index = adjust_index(index, list);
                dplist_node_t * node = NULL, *node_next = NULL, *node_prev = NULL;
                node = list->head;
                int i = 0;
                while(node->next != NULL && i != index){
                        node_prev = node;
                        node = node->next;
                        if (node->next != NULL){
                                node_next = node->next;
                        }
                        else{
                                node_next = NULL;
                        }
                        i++;
                }
                if(node->prev == NULL && node->next == NULL){
                        list->head = NULL;
                }
                else if(node->prev == NULL){ // eerste element
                        node_next = node->next;
                        list->head = node_next;
                        node_next -> prev = NULL;
                }
                else if(node->next == NULL){ //laatste element
                        node_prev -> next = NULL;
                }
                else{
                        node_prev -> next = node->next;
                        node_next ->prev = node ->prev;
                }
                if(free_element && node->element != NULL){
			list->element_free(&node->element);
                }
                free(node);
                return list;
        }
}

int dpl_size(dplist_t *list) {

    //TODO: add your code here
        int size = 0;
        if(list == NULL){
        	return -1;
        }

        else{
                dplist_node_t * node = list->head;
                while(node != NULL){
                        size++;
                        node = node->next;
                }
                return size;
        }

}

void *dpl_get_element_at_index(dplist_t *list, int index) {

    //TODO: add your code here
       if(list == NULL || list->head == NULL){
                return 0;
        }
        else{
                index = adjust_index(index, list);
                int i = 0;
                dplist_node_t * node = list->head;
                while(node -> next != NULL && i != index){
                        node = node->next;
                        i++;
                }
                return node->element;
        }
}


int dpl_get_index_of_element(dplist_t *list, void *element) {

    //TODO: add your code here
       if(list == NULL) return 0;
       else if(list->head==NULL) return -1;
       else{
                int index = 0;
                dplist_node_t * node;
                node = list->head;
                while(list->element_compare(node->element,element)!=0 && node->next !=NULL){
                        node = node->next;
                        index++;
                }
                if(list->element_compare(node->element, element) == 0){
                        return index;
                }
                else{
                        return -1;
                }
        }

}

dplist_node_t *dpl_get_reference_at_index(dplist_t *list, int index) {

    //TODO: add your code here
    int count;
    dplist_node_t *dummy;
    if (list == NULL || list->head == NULL) return NULL;
    DPLIST_ERR_HANDLER(list == NULL, DPLIST_INVALID_ERROR);
    for (dummy = list->head, count = 0; dummy->next != NULL; dummy = dummy->next, count++) {
        if (count >= index) return dummy;
    }
    return dummy;
}

dplist_node_t *check_if_ref_in_list(dplist_t*list, dplist_node_t *reference){
		dplist_node_t * node = list->head;
                while(node!=NULL){
                        if(node == reference){
                                return node;
                        }
                        node = node->next;
                }
                return node;
}


void *dpl_get_element_at_reference(dplist_t *list, dplist_node_t *reference) {

    //TODO: add your code here
	if(list == NULL || list->head == NULL || reference == NULL) return NULL;

	else{
		if(check_if_ref_in_list(list, reference) == NULL){
			return NULL;
		}
		else{
			return reference->element;
		}
	}

}

dplist_node_t *dpl_get_first_reference(dplist_t *list){
	if(list == NULL || list->head == NULL) return NULL;
	else{
		return list->head;
	}
}

dplist_node_t *dpl_get_last_reference(dplist_t *list){

        if(list == NULL || list->head == NULL) return NULL;
        else{
                return dpl_get_reference_at_index(list, dpl_size(list)-1);
        }
}


dplist_node_t *dpl_get_next_reference(dplist_t *list, dplist_node_t *reference){
	if(list == NULL || list->head == NULL || reference == NULL) return NULL;
	else{
		dplist_node_t * node = check_if_ref_in_list(list, reference);
		if(node == NULL) return NULL;
		else{
			return node->next;
		}
	}
}

dplist_node_t *dpl_get_previous_reference(dplist_t *list, dplist_node_t *reference){
        if(list == NULL || list->head == NULL || reference == NULL) return NULL;
        else{
                dplist_node_t * node = check_if_ref_in_list(list, reference);
                if(node == NULL) return NULL;
                else{
                        return node->prev;
                }
        }
}


dplist_node_t *dpl_get_reference_of_element(dplist_t *list, void *element){
	if(list==NULL || list->head == NULL) return NULL;
	else{
		dplist_node_t * node = list->head, *node_of_element;
		int found = 0;
                while(node!=NULL || found !=1){
                        if(node->element == element || list->element_compare(node->element, element)==0){
				node_of_element = node;
				found = 1;
                        }
                        node = node->next;
                }
                if(node_of_element == NULL){
			return NULL;
                }
                return node_of_element;

	}
}

int dpl_get_index_of_reference(dplist_t *list, dplist_node_t *reference){
	if(list==NULL || list->head == NULL || reference == NULL) return -1;
	else{
		dplist_node_t *node = list->head;
		int index = 0;
		int found = 0;
		while(node != NULL && found == 0){
			if(node == reference){
				found = 1;
			}
			else{
				node = node->next;
				index++;
			}
		}
		if(node == NULL){
			return -1;
		}
		return index;
	}
}
dplist_t *dpl_insert_at_reference(dplist_t *list, void *element, dplist_node_t *reference, bool insert_copy){
	if(list == NULL || reference == NULL ) return NULL;
	else{
		dplist_node_t *new_node, *ref;
		ref = check_if_ref_in_list(list, reference);
		if(ref == NULL){
			return list;
		}
		else{
			new_node = ref;
			ref = malloc(sizeof(dplist_node_t));
			ref -> element = new_node -> element;
			ref ->prev = new_node;
			ref -> next = new_node->next;
			new_node->next = ref;
			if(insert_copy) new_node->element = list->element_copy(element);
			else{new_node->element = element;}
		}
		return list;
	}
}

dplist_t *dpl_insert_sorted(dplist_t *list, void *element, bool insert_copy){
	if(list == NULL){ return NULL;}
	else{
		dplist_node_t * node =  list->head;
		int index = 0;
		if(element == NULL){
			dpl_insert_at_index(list, element, dpl_size(list), insert_copy);
			return list;
		}
		else{
			while(node != NULL){
				if(node->next != NULL){
					if(list->element_compare(node->element, element) == 1 && list->element_compare((node->next)->element, element) == -1){	//element kleiner dan huidig node element maar groter dan het volgende node element
						index = dpl_get_index_of_element(list, node->element);
						dpl_insert_at_index(list, element, index+1, insert_copy);
					}
					return list;
				}
				else{
					if(list->element_compare(node->element, element) == 1){
						index = dpl_get_index_of_element(list, node->element);
						dpl_insert_at_index(list, element, index+1, insert_copy);
					}
					else{
						dpl_insert_at_index(list,element, 0, insert_copy);
					}
					return list;
				}
			}
			if(node == NULL){
				dpl_insert_at_index(list, element,0 , insert_copy);
                		return list;
			}
		}
		return list;
	}
}

dplist_t *dpl_remove_at_reference(dplist_t *list, dplist_node_t *reference, bool free_element){
	if(list == NULL || list->head == NULL || reference == NULL){
		return NULL;
	}
	else{
		dplist_node_t * ref = check_if_ref_in_list(list, reference);
		if(ref == NULL){
			return list;
		}
		else{
			int ref_index = dpl_get_index_of_reference(list, ref);
			dpl_remove_at_index(list, ref_index, free_element);
			return list;
		}
	}
}

dplist_t *dpl_remove_element(dplist_t *list, void *element, bool free_element){
	if(list == NULL)return NULL;
	else{
		dplist_node_t * node_to_be_removed = dpl_get_reference_of_element(list, element);
		dpl_remove_at_reference(list, node_to_be_removed, free_element);
		return list;
	}
}
