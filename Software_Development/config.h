/**
 * \author Yari Nys
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_
/*#include <stdint.h>*/
#include <time.h>
//stdint for minimum
//inttypes includes stdint and provides also print formats,...
#include <inttypes.h>
#include <pthread.h>
#include <stdlib.h>
#include "./lib/dplist.h"
#include "errmacros.h"
#ifdef DEBUG_ALL
#define DEBUG_ALL_PRINT(message,...)									                        \
            do { fprintf(stderr,"File: %s on line %d:" message "\n",__FILE__, __LINE__, ##__VA_ARGS__);} while (0)
#else
    #define DEBUG_ALL_PRINT(...) (void)0
#endif

#define FIFO_NAME "gateway"
#define LOG_READER_BUFFER 100

#define LOG_WRITER_BUFFER 100
#define END_OF_LOG_MESSAGE "All threads exited, ending log process."


typedef uint16_t sensor_id_t;
typedef double sensor_value_t;
typedef time_t sensor_ts_t;         // UTC timestamp as returned by time() - notice that the size of time_t is different on 32/64 bit machine
typedef double running_avg_t;

typedef struct {
    sensor_id_t id;
    sensor_value_t value;
    sensor_ts_t ts;
    int read_by_datamgr;
    int read_by_sensor_db;
} sensor_data_t;

typedef struct {
    FILE* writer_fp;
    uint16_t sequence;
    pthread_mutex_t mutex;
}logging_info_t;

extern logging_info_t logg_info;
void write_log(char message[]);
#endif /* _CONFIG_H_ */
