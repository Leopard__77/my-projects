

#include <stdio.h>
#include <sqlite3.h>
#include "sensor_db.h"
#include "sbuffer.h"
#include <unistd.h>

DBCONN *init_connection(char clear_up_flag) {
    sqlite3 *db;
    int rc;
    if (AUTO_CREATE_DB == 1){
        rc = sqlite3_open(TO_STRING(DB_NAME), &db);
    }
    else if (AUTO_CREATE_DB == 0){
        rc = sqlite3_open_v2(TO_STRING(DB_NAME), &db, SQLITE_OPEN_READWRITE, NULL);
    }
    if (rc != SQLITE_OK) {
        int attempts = 0;
        while (attempts != NUMBER_OF_DB_CONNECTION_ATTEMPTS && rc != SQLITE_OK) {
            rc = sqlite3_open_v2(TO_STRING(DB_NAME), &db, SQLITE_OPEN_READWRITE, NULL);
            attempts++;
            sleep(5);
        }
        DEBUG_ALL_PRINT("Can't open database: %s; Attempts made: %d", sqlite3_errmsg(db), attempts);
        write_log("Unable to connect to SQL server.");
        return NULL;
    }
    else {
        DEBUG_ALL_PRINT("Opened database successfully");
        write_log("Connection to SQL server established.");
        char *sql, *err_msg;
        sql = "CREATE TABLE IF NOT EXISTS " TO_STRING(TABLE_NAME) " (id INTEGER PRIMARY KEY, sensor_id INTEGER, sensor_value DOUBLE,timestamp TIMESTAMP);";
        rc = sqlite3_exec(db, sql, 0, 0, &err_msg);
        if (rc != SQLITE_OK) {
            DEBUG_ALL_PRINT("SQL error: %s", err_msg);
            sqlite3_free(err_msg);
        } else {
            DEBUG_ALL_PRINT("Table created successfully");
            write_log("New table "TO_STRING(TABLE_NAME)" created.");
        }
        if (clear_up_flag == 1) {
            sql = "IF " TO_STRING(TABLE_NAME) " EXISTS DELETE * FROM "TO_STRING(TABLE_NALE);
            rc = sqlite3_exec(db, sql, 0, 0, &err_msg);
            if (rc != SQLITE_OK) {
                DEBUG_ALL_PRINT("SQL error: %s", err_msg);
                sqlite3_free(err_msg);
            } else {
                DEBUG_ALL_PRINT("Table cleared successfully");
            }
        }
        return db;
    }
}
int find_sensor_all(DBCONN *conn, callback_t f){
    char sql[100], *err_msg;
    snprintf(sql, 100, "SELECT * FROM %s", TO_STRING(TABLE_NAME));
    int rc = sqlite3_exec(conn ,sql, f, 0, &err_msg);
    if(rc != SQLITE_OK){
        fprintf(stderr, "SQL error: %s\n", err_msg);
        sqlite3_free(err_msg);
        return -1;
    }
    return 0;
}


void disconnect(DBCONN *conn) {
    sqlite3_close(conn);
    write_log("Connection to SQL server lost.");
}

int insert_sensor_from_buffer(DBCONN *conn, sbuffer_t * buffer) {
    //https://stackoverflow.com/questions/1711631/improve-insert-per-second-performance-of-sqlite
    char *err_msg = NULL;
    char sql[SQL_BUFFER] = "\0";
    sqlite3_stmt * stmt;
    const char * tail = 0;
    sensor_id_t id;
    sensor_value_t value;
    sensor_ts_t ts;
    sprintf(sql, "INSERT INTO %s VALUES (NULL, @id, @value, @ts)", TO_STRING(TABLE_NAME));
    sqlite3_prepare_v2(conn, sql, SQL_BUFFER, &stmt, &tail);

    sqlite3_exec(conn, "BEGIN TRANSACTION", NULL,  NULL, &err_msg);

    while (get_buffer_size(buffer) > 0 || get_writer_thread_status(buffer) == 1){
        sensor_data_t * data = sbuffer_get_element(buffer, STORAGEMGR);  //get last element from buffer
        if(data != NULL) {
            id = data->id;
            value = data->value;
            ts = data->ts;
            sqlite3_bind_int(stmt, 1, id);
            sqlite3_bind_double(stmt, 2, value);
            sqlite3_bind_int64(stmt, 3, ts);
            sqlite3_step(stmt);
            sqlite3_clear_bindings(stmt);
            sqlite3_reset(stmt);
            set_data_read_status(buffer, data, STORAGEMGR, 1);
        }
    }
    sqlite3_exec(conn, "END TRANSACTION", NULL, NULL, &err_msg);
    sqlite3_finalize(stmt);
    DEBUG_ALL_PRINT("SQL msg: %s", err_msg);
    SQL_ERROR(err_msg);
    return 0;
}



