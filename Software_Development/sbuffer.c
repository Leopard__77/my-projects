/**
 * \author Yari Nys
 */
#define _XOPEN_SOURCE 600   //https://stackoverflow.com/questions/5378778/what-does-d-xopen-source-do-mean
#include <stdlib.h>
#include <stdio.h>
#include "sbuffer.h"
#include <pthread.h>

/**
 * basic node for the buffer, these nodes are linked together to create the buffer
 */
typedef struct sbuffer_node {
    struct sbuffer_node *next;  /**< a pointer to the next node*/
    sensor_data_t *data;         /**< a structure containing the data */
} sbuffer_node_t;

/**
 * a structure to keep track of the buffer
 */
struct sbuffer {
    sbuffer_node_t *head;       /**< a pointer to the first node in the buffer */
    sbuffer_node_t *tail;       /**< a pointer to the last node in the buffer */
    pthread_cond_t data_available;
    pthread_mutex_t mutex;
    int writer_thread_alive;
    int datamgr_thread_alive;
    int nr_of_elements;
    pthread_rwlock_t rwlock;

};

void set_writer_thread_status(sbuffer_t *buffer, int status){
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_wrlock(&buffer->rwlock));
    buffer->writer_thread_alive = status;
    COND_VAR_ERROR_HANDLER(pthread_cond_broadcast(&(buffer)->data_available));
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));

}

int get_writer_thread_status(sbuffer_t* buffer){
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_rdlock(&buffer->rwlock));
    int status = buffer->writer_thread_alive;
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));
    return status;
}

void set_datamgr_thread_status(sbuffer_t *buffer, int status){
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_wrlock(&buffer->rwlock));
    buffer->datamgr_thread_alive = status;
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));


}

int get_datamgr_thread_status(sbuffer_t* buffer){
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_rdlock(&buffer->rwlock));
    int status = buffer->datamgr_thread_alive;
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));
    return status;
}

int sbuffer_init(sbuffer_t **buffer) {
    *buffer = malloc(sizeof(sbuffer_t));
    if (*buffer == NULL) return SBUFFER_FAILURE;
    (*buffer)->head = NULL;
    (*buffer)->tail = NULL;

    MUTEX_ERROR_HANDLER(pthread_mutex_init(&(*buffer)->mutex, NULL));
    COND_VAR_ERROR_HANDLER(pthread_cond_init(&(*buffer)->data_available, NULL));
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_init(&(*buffer)->rwlock, NULL));
    (*buffer)->nr_of_elements = 0;
    (*buffer)->writer_thread_alive = 0;
    (*buffer)->datamgr_thread_alive = 0;
    return SBUFFER_SUCCESS;
}

int get_buffer_size(sbuffer_t * buffer){
    if (buffer == NULL) {
        return SBUFFER_FAILURE;
    }
    else {
        return buffer->nr_of_elements;
    }
}

int sbuffer_free(sbuffer_t **buffer) {
    if ((buffer == NULL) || (*buffer == NULL)) {
        return SBUFFER_FAILURE;
    }
    while ((*buffer)->head) {
        sbuffer_node_t *dummy = (*buffer)->head;
        (*buffer)->head = (*buffer)->head->next;
        free(dummy->data);
        free(dummy);
    }

    MUTEX_ERROR_HANDLER(pthread_mutex_destroy(&(*buffer)->mutex));
    COND_VAR_ERROR_HANDLER(pthread_cond_destroy(&(*buffer)->data_available));
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_destroy(&(*buffer)->rwlock));

    free(*buffer);
    *buffer = NULL;
    return SBUFFER_SUCCESS;
}
int sbuffer_remove(sbuffer_t *buffer) {


   /* if (buffer == NULL) return SBUFFER_FAILURE;
    if (buffer->head == NULL) return SBUFFER_NO_DATA;*/

    RW_LOCK_ERROR_HANDLER(pthread_rwlock_wrlock(&buffer->rwlock));
    if (buffer == NULL){
        RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));
        return SBUFFER_FAILURE;
    }
    else if (buffer->head == NULL){
        RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));
        return SBUFFER_NO_DATA;
    }
    sbuffer_node_t *dummy = buffer->head;
    DEBUG_ALL_PRINT("LOCKING_REMOVE");
    if (dummy->data->read_by_sensor_db == 1 && dummy->data->read_by_datamgr == 1) {
        if (buffer->head == buffer->tail) // buffer has only one node
        {
            buffer->nr_of_elements--;
            buffer->head = buffer->tail = NULL;
            free(dummy->data);
            dummy->data = NULL;
            free(dummy);

        }
        else  // buffer has many nodes empty
        {
            buffer->nr_of_elements--;
            buffer->head = buffer->head->next;
            free(dummy->data);
            dummy->data = NULL;
            free(dummy);

        }
        //return SBUFFER_SUCCESS;
    }
    DEBUG_ALL_PRINT("UNLOCKING_REMOVE");
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));
/*    else{
        return SBUFFER_FAILURE;
    }*/
    return SBUFFER_SUCCESS;
}

int sbuffer_insert(sbuffer_t *buffer, sensor_data_t *data) {
    sbuffer_node_t *dummy;
    if (buffer == NULL) return SBUFFER_FAILURE;
    dummy = malloc(sizeof(sbuffer_node_t));
    if (dummy == NULL) return SBUFFER_FAILURE;
    dummy->data = data;
    dummy->next = NULL;

    RW_LOCK_ERROR_HANDLER(pthread_rwlock_wrlock(&buffer->rwlock));
    DEBUG_ALL_PRINT("LOCKING_INSERT");
    if (buffer->head == NULL || buffer->tail == NULL) // buffer empty then buffer->tail should is also NULL
    {

        buffer->head = buffer->tail = dummy;
        buffer->nr_of_elements++;
        COND_VAR_ERROR_HANDLER(pthread_cond_signal(&(buffer)->data_available));
        DEBUG_ALL_PRINT("UNLOCKING_INSERT");
        RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));
    } else // buffer not empty
    {

        buffer->tail->next = dummy;
        buffer->tail = buffer->tail->next;
        buffer->nr_of_elements++;
        COND_VAR_ERROR_HANDLER(pthread_cond_broadcast(&(buffer)->data_available));
        DEBUG_ALL_PRINT("UNLOCKING_INSERT");
        RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));
    }


    return SBUFFER_SUCCESS;
}
sensor_data_t *sbuffer_get_element(sbuffer_t *buffer, int mgr) {
    MUTEX_ERROR_HANDLER(pthread_mutex_lock(&buffer->mutex));
    while (get_buffer_size(buffer) == 0 && get_writer_thread_status(buffer) == 1) {    //block if buffer empty AND writer thread is still running
        COND_VAR_ERROR_HANDLER(pthread_cond_wait(&buffer->data_available, &buffer->mutex));
    }
    RW_LOCK_ERROR_HANDLER(pthread_mutex_unlock(&buffer->mutex));

    /*if(buffer == NULL){return NULL;}*/

    RW_LOCK_ERROR_HANDLER(pthread_rwlock_rdlock(&buffer->rwlock));
    DEBUG_ALL_PRINT("LOCKING_GET");
    if(buffer == NULL){return NULL;}
    sbuffer_node_t *node = buffer->head;
    int found = 0;
    while (node != NULL && node->data != NULL && found != 1) {
        if (mgr == STORAGEMGR && node->data->read_by_sensor_db == 0) {
            found = 1;
        }
        else if (mgr == DATAMGR && node->data->read_by_datamgr == 0) {
                found = 1;
        }
        else{
            node = node->next;
        }
    }
    DEBUG_ALL_PRINT("UNLOCKING_GET");
    RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));
    sensor_data_t *data = NULL;
    if (node != NULL) {
        data = node->data;
    } else {
        data = NULL;
    }
    return data;
}
void set_data_read_status(sbuffer_t * buffer, sensor_data_t * data, int mgr, int status){
    if(mgr == DATAMGR) {

        RW_LOCK_ERROR_HANDLER(pthread_rwlock_wrlock(&buffer->rwlock));
        DEBUG_ALL_PRINT("LOCKING_READ1");
        data->read_by_datamgr = status;
        DEBUG_ALL_PRINT("UNLOCKING_READ1");
        RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));
    }
    else if(mgr == STORAGEMGR){

        RW_LOCK_ERROR_HANDLER(pthread_rwlock_wrlock(&buffer->rwlock));
        DEBUG_ALL_PRINT("LOCKING_READ2");
        data->read_by_sensor_db = status;
        DEBUG_ALL_PRINT("UNLOCKING_READ2");
        RW_LOCK_ERROR_HANDLER(pthread_rwlock_unlock(&buffer->rwlock));
    }
    sbuffer_remove(buffer);
}

