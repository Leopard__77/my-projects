#define _GNU_SOURCE //The function asprintf() is not yet part of the C Standard https://stackoverflow.com/questions/61306157/unable-to-compile-program-due-to-function-asprintf

#include <stdlib.h>
#include <stdio.h>
#include "datamgr.h"
#include "./lib/dplist.h"
#include <time.h>

#define MEMORY_ERROR "Memory allocation failure" // error due to mem alloc failure
#define INVALID_ERROR "Sensor id is not valid"//error due to sensor not found

typedef struct{
	uint16_t room_id;
	sensor_data_t sensor_data;
	double prev_temps[RUN_AVG_LENGTH];
    running_avg_t running_avg;
}sensor_t;

void* element_copy(void * element);
void element_free(void ** element);
int element_compare(void * x, void * y);
dplist_t *list = NULL;

void * element_copy(void * element) {
	sensor_t* copy = malloc(sizeof (sensor_t));
	sensor_t* sensor = (sensor_t*) element;
	copy->room_id = sensor->room_id;
	copy->sensor_data = sensor->sensor_data;
	for(int i=0; i<RUN_AVG_LENGTH; i++){
		copy->prev_temps[i] = sensor->prev_temps[i];
	}
	return (void*) copy;
}

void element_free(void ** element) {
	sensor_t*sensor = *element;
	free(sensor);
}

int element_compare(void * x, void * y) {
	return ( ( *(uint16_t*)y == ((sensor_t*) x)->sensor_data.id) ? 0 : -1);
}


sensor_t * check_if_sensorid_valid(sensor_id_t sensor_id){
	int index = dpl_get_index_of_element(list, (void*) &sensor_id);
	if(index != -1){
		return (dpl_get_element_at_index(list, index));
	}
	else{
		return NULL;
	}
}

sensor_value_t datamgr_get_avg(sensor_id_t sensor_id){
    return (check_if_sensorid_valid(sensor_id))->running_avg;
}


uint16_t datamgr_get_room_id(sensor_id_t sensor_id){
	return (check_if_sensorid_valid(sensor_id))->room_id;
}

void datamgr_free(){
	dpl_free(&list, true);
}

void datamgr_calculate_avg_from_buffer(FILE *fp_sensor_map, sbuffer_t *buffer) {
    int index = 0;
    uint16_t sensor_id_buffer = 0;
    uint16_t room_id_buffer = 0;
    list = dpl_create(element_copy, element_free, element_compare);
    while ((fscanf(fp_sensor_map,"%"SCNu16"%"SCNu16, &room_id_buffer, &sensor_id_buffer)) >0){    //sensor id en room id zijn 2 bytes //SCN for scan
        sensor_t *new_sensor = malloc(sizeof(sensor_t));
        ERROR_HANDLER(new_sensor == NULL, MEMORY_ERROR);
        new_sensor->room_id = (uint16_t) room_id_buffer;
        new_sensor->sensor_data.id = (uint16_t) sensor_id_buffer;
        for (int i = 0; i < RUN_AVG_LENGTH; i++) {    //prev_temps initialiseren anders report valgrind conditional jump or move depends on uninitialised value(s)
            new_sensor->prev_temps[i] = 0;
        }
        new_sensor->running_avg=0;
        dpl_insert_at_index(list, new_sensor, index, false);
        DEBUG_ALL_PRINT("Sensor: %"PRIu16" and room added", new_sensor->sensor_data.id);
        index++;
    }
    sensor_data_t * buffer_data = NULL;
    sensor_t  *sensor = NULL;
    while ( (get_buffer_size(buffer) > 0 && get_datamgr_thread_status(buffer) == 1) || get_writer_thread_status(buffer) == 1) {
            buffer_data = sbuffer_get_element(buffer, DATAMGR);   //blocked if buffer size == 0 AND writer thread is running
            if (buffer_data != NULL) {
                sensor = check_if_sensorid_valid(buffer_data->id);
                if (sensor != NULL) {
                    double sum_temps = 0.0;
                    for (int i = 0; i < RUN_AVG_LENGTH - 1; i++) {
                        sensor->prev_temps[i] = sensor->prev_temps[i + 1];
                        sum_temps += (sensor->prev_temps[i]);
                    }
                    sensor->prev_temps[RUN_AVG_LENGTH - 1] = buffer_data->value;
                    sensor->sensor_data.value = buffer_data->value;
                    sum_temps += buffer_data->value;
                    if (sensor->prev_temps[0] != 0) {
                        sensor->running_avg = sum_temps / RUN_AVG_LENGTH;
                    } else {
                        sensor->running_avg = 0;
                    }
                    sensor->sensor_data.ts = buffer_data->ts;
                    char *log_buffer;
                    if (sensor->prev_temps[0] != 0) {    //send message when number prev-temps is "full"
                        if (sensor->running_avg < SET_MIN_TEMP) {
                            DEBUG_ALL_PRINT("It's too cold in room %"PRIu16" the average temperature is %f °C!", (uint16_t) datamgr_get_room_id(sensor->sensor_data.id), datamgr_get_avg(sensor->sensor_data.id));
                            asprintf(&log_buffer, "It's too cold in room %"PRIu16" the average temperature is %f °C!", (uint16_t) datamgr_get_room_id(sensor->sensor_data.id), datamgr_get_avg(sensor->sensor_data.id));
                            write_log(log_buffer);
                        } else if (sensor->running_avg > SET_MAX_TEMP) {
                            DEBUG_ALL_PRINT("It's too hot in room %"PRIu16" the average temperature is %f °C!", (uint16_t) datamgr_get_room_id(sensor->sensor_data.id), datamgr_get_avg(sensor->sensor_data.id));
                            asprintf(&log_buffer, "It's too hot in room %"PRIu16" the average temperature is %f °C!", (uint16_t) datamgr_get_room_id(sensor->sensor_data.id), datamgr_get_avg(sensor->sensor_data.id));
                            write_log(log_buffer);

                        }

                    }

                }
                else{
                    char *log_buffer;
                    DEBUG_ALL_PRINT("Received sensor data with invalid sensor node ID: %"PRIu16, buffer_data->id);
                    asprintf(&log_buffer, "Received sensor data with invalid sensor node ID: %"PRIu16, buffer_data->id);
                    write_log(log_buffer);
                }
                set_data_read_status(buffer, buffer_data, DATAMGR, 1);
            }
    }
}
