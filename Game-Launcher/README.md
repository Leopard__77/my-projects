## Game launcher for Linux (maybe Windows)

### 1. Main idea
Launch games on linux from a unified game launcher. An alternative is Lutris but it's rather 
minimalistic. 

### 2. Install requirements
**Arch Linux (KDE Plasma)**
+ C & C++ compiler: `sudo pacman -S gcc`
+ Qt 6: `sudo pacman -S qt6-base`
+ Wine: `sudo pacman -S wine-staging winetricks`
  + Wine dependencies: `sudo pacman -S giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo libxcomposite lib32-libxcomposite libxinerama lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader cups samba dosbox`
+ Drivers: see [InstallingDrivers.md](https://github.com/lutris/docs/blob/master/InstallingDrivers.md)

### 3. Develop Qt project in CLion (not needed)
Normally, project can directly be imported in CLion. Otherwise, check link below.
https://www.jetbrains.com/help/clion/qt-tutorial.html#qt-template

### 4. Extra

 + Add ideas/features by creating issues
