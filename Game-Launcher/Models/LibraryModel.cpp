//
// Created by atlas on 7/13/22.
//

#include "LibraryModel.h"
#include <QSqlRecord>
#include <QSqlError>

LibraryModel::LibraryModel(const QString & tableName):
        librarySqlModel{new QSqlTableModel(this, DBController::getInstance()->getQsqlDB())}
{
    if(DBController::getInstance()->getQsqlDB().isOpen()){
        librarySqlModel->setTable(tableName);
        librarySqlModel->setEditStrategy(QSqlTableModel::OnManualSubmit);
        librarySqlModel->select();
    }
    else{
        qWarning() << "DB is not open";
    }
}

void LibraryModel::submitToDB() {
        if(!librarySqlModel->submitAll()){
            qDebug() << librarySqlModel->lastError().text();
        }
}

int LibraryModel::getModelRowIndex(int gameDBId) {
        QModelIndexList modelIndexList = librarySqlModel->match(
                librarySqlModel->index(0, librarySqlModel->fieldIndex(DB_COL_ID)), Qt::DisplayRole, QVariant(gameDBId));

        if (modelIndexList.size() == 1) {
            return modelIndexList.first().row();
        } else {
            return -1;
        }
}

int LibraryModel::getIdFromRowIndex(int rowIndex) {
    return librarySqlModel->record(rowIndex).field(DB_COL_ID).value().toInt();
}

QString LibraryModel::getTitle(int gameDBId) {
    return librarySqlModel->data(librarySqlModel->index(getModelRowIndex(gameDBId), librarySqlModel->fieldIndex(DB_COL_TITLE)), Qt::DisplayRole).toString();
}

QString LibraryModel::getPicUrl(int gameDBId) {
    return librarySqlModel->data(librarySqlModel->index(getModelRowIndex(gameDBId), librarySqlModel->fieldIndex(DB_COL_PIC_URL)), Qt::DisplayRole).toString();
}
