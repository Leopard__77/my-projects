//
// Created by atlas on 7/13/22.
//

#ifndef GAMELAUNCHER_LIBRARYMODEL_H
#define GAMELAUNCHER_LIBRARYMODEL_H

#include <QSqlTableModel>
#include <QSqlRecord>
#include <QSqlField>
#include "../Controllers/DBController.h"
#include <QWidget>

class LibraryModel : public QWidget {
    Q_OBJECT
public:
    LibraryModel(const QString & tableName);

    QSqlTableModel * getLibraryModel(){return librarySqlModel;};
    int getModelRowIndex(int gameDBId);
    int getIdFromRowIndex(int rowIndex);

    QString getTitle(int gameDBId);
    QString getPicUrl(int gameDBId);
    void submitToDB();

private:
    QSqlTableModel * librarySqlModel = nullptr;
};


#endif //GAMELAUNCHER_LIBRARYMODEL_H
