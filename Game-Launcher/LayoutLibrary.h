//
// Created by atlas on 22/07/22.
//

#ifndef GAMELAUNCHER_LAYOUTLIBRARY_H
#define GAMELAUNCHER_LAYOUTLIBRARY_H

#include <QLayout>
#include <QStyle>

class LayoutLibrary : public QLayout{
public:
    LayoutLibrary(QWidget *parent, int margin = -1, int hSpacing = -1, int vSpacing = -1);
    LayoutLibrary(int margin = -1, int hSpacing = -1, int vSpacing = -1);
    ~LayoutLibrary();

    void addItem(QLayoutItem *item) override;
    int horizontalSpacing() const;
    int verticalSpacing() const;
    Qt::Orientations expandingDirections() const override;
    bool hasHeightForWidth() const override;
    int heightForWidth(int) const override;
    int count() const override;
    QLayoutItem *itemAt(int index) const override;
    QSize minimumSize() const override;
    void setGeometry(const QRect &rect) override;
    QSize sizeHint() const override;
    QLayoutItem *takeAt(int index) override;

private:
    int doLayout(const QRect &rect, bool testOnly) const;
    int smartSpacing(QStyle::PixelMetric pm) const;

    QList<QLayoutItem *> itemList;
    int m_hSpace;
    int m_vSpace;
};


#endif //GAMELAUNCHER_LAYOUTLIBRARY_H
