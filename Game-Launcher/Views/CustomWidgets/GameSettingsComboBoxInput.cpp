//
// Created by atlas on 7/29/22.
//

#include "GameSettingsComboBoxInput.h"
#include "../../Controllers/DBController.h"
GameSettingsComboBoxInput::GameSettingsComboBoxInput(QString labelText):
        qGridLayout(this)
{
    qLabel.setFixedWidth(120);
    qLabel.setWordWrap(true);
    qLabel.setText(labelText.replace("_", " "));

    std::for_each(DBController::getInstance()->getFSRModes().begin(), DBController::getInstance()->getFSRModes().end(),[this] (QPair<int, QString> const & keyPair){
        qComboBox.addItem(keyPair.second, keyPair.first);
    });

    qGridLayout.addWidget(&qLabel, 0, 0);
    qGridLayout.addWidget(&qComboBox, 0, 1);
    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    this->setLayout(&qGridLayout);

}
