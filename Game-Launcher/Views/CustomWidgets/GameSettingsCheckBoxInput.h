//
// Created by atlas on 7/14/22.
//

#ifndef GAMELAUNCHER_GAMESETTINGSCHECKBOXINPUT_H
#define GAMELAUNCHER_GAMESETTINGSCHECKBOXINPUT_H


#include <QString>
#include <QLabel>
#include <QCheckBox>
#include <QHBoxLayout>

class GameSettingsCheckBoxInput : public QWidget {
    Q_OBJECT
public:
    GameSettingsCheckBoxInput(QString labelText);
    QLabel & getQLabel(){return qLabel;};
    QCheckBox & getQCheckBox(){return qCheckBox;};
private:
    QLabel qLabel;
    QCheckBox qCheckBox;
    QHBoxLayout qhBoxLayout;
};


#endif //GAMELAUNCHER_GAMESETTINGSCHECKBOXINPUT_H
