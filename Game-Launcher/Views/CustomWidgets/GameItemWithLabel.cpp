//
// Created by atlas on 7/14/22.
//

#include "GameItemWithLabel.h"

GameItemWithLabel::GameItemWithLabel(QWidget * parent, const QString& title, const QString& picUrl):
        gameTitle{QLabel(title, this)},
        gamePic{QLabel("", this)}
{
    this->setParent(parent);
    qvBoxLayout.addWidget(&gamePic);
    qvBoxLayout.addWidget(&gameTitle);
    qvBoxLayout.setAlignment(Qt::AlignHCenter);

    gameTitle.setAlignment(Qt::AlignHCenter);
    gameTitle.setStyleSheet("QLabel {color : white; border: 0px;}");

    gamePic.setPixmap(picUrl);
    gamePic.setStyleSheet("QLabel {border: 0px;}");

    this->setLayout(&qvBoxLayout);
    this->qvBoxLayout.setContentsMargins(3,3,3,3);
    this->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    this->setStyleSheet("QFrame:hover {border: 3px solid #8c57ff;}");
}

void GameItemWithLabel::mousePressEvent(QMouseEvent *event)
{
    if(event->type() == QEvent::MouseButtonPress){
        if(event->button() == Qt::RightButton){
            qDebug("Right mouse button was clicked");
            emit openGameSettings();
        }
        else if(event->button() == Qt::LeftButton){
            qDebug("left mouse button was clicked");
            emit showGameInfo();
        }
    }
    else if(event->button() == Qt::LeftButton && event->type()==QEvent::MouseButtonDblClick){
        qDebug("left mouse button was double clicked");
        emit launchGame();
    }
}

void GameItemWithLabel::updateView(const QString &title, const QString &picUrl) {
    gameTitle.setText(title);
    gamePic.setPixmap(picUrl);
}







