//
// Created by atlas on 7/14/22.
//

#include "GameSettingsCheckBoxInput.h"

GameSettingsCheckBoxInput::GameSettingsCheckBoxInput(QString labelText):
        qLabel(this),
        qCheckBox("", this)
{
    qLabel.setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
    qLabel.setFixedWidth(120);
    qLabel.setText(labelText.replace("_", " "));
    qLabel.setWordWrap(true);
    qhBoxLayout.addWidget(&qLabel);
    qhBoxLayout.addWidget(&qCheckBox);
    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    this->setLayout(&qhBoxLayout);
}
