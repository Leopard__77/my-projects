//
// Created by atlas on 7/14/22.
//

#ifndef GAMELAUNCHER_GAMESETTINGSPATHINPUT_H
#define GAMELAUNCHER_GAMESETTINGSPATHINPUT_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

class GameSettingsPathInput : public QWidget{
    Q_OBJECT

public:
    GameSettingsPathInput(QString labelText);
public slots:
    void openFileManager();

    QLabel & getQLabel(){return qLabel;};
    QLineEdit & getQLineEditPath(){return qLineEditPath;};
private:
    QGridLayout qGridLayout;
    QLabel qLabel;
    QLineEdit qLineEditPath;
    QPushButton qPushButtonOpenFileManager;

};


#endif //GAMELAUNCHER_GAMESETTINGSPATHINPUT_H
