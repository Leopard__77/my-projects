//
// Created by atlas on 7/14/22.
//

#include "GameSettingsFileInput.h"

#include <QIcon>
#include <QFileDialog>
#include <QSizePolicy>
GameSettingsFileInput::GameSettingsFileInput(QString labelText):
        qLabel (this),
        qLineEditPath(this),
        qPushButtonOpenFileManager(QIcon("../folder-open.png"), "", this)
{
    qLabel.setFixedWidth(120);
    qLabel.setWordWrap(true);
    qLabel.setText(labelText.replace("_", " "));
    qGridLayout.addWidget(&qLabel, 0, 0);
    qGridLayout.addWidget(&qLineEditPath, 0, 1);
    qGridLayout.addWidget(&qPushButtonOpenFileManager, 0, 2);
    this->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    this->setLayout(&qGridLayout);

    QObject::connect(&qPushButtonOpenFileManager, &QPushButton::clicked, this, &GameSettingsFileInput::openFileManager);
}

void GameSettingsFileInput::openFileManager() {
    this->qLineEditPath.setText(QFileDialog::getOpenFileName(this));
}
