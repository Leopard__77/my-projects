//
// Created by atlas on 7/14/22.
//

#ifndef GAMELAUNCHER_GAMESETTINGSFILEINPUT_H
#define GAMELAUNCHER_GAMESETTINGSFILEINPUT_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

class GameSettingsFileInput : public QWidget{
    Q_OBJECT

public:
    GameSettingsFileInput(QString labelText);
public slots:
    void openFileManager();

    QLabel & getQLabel(){return qLabel;};
    QLineEdit & getQLineEditPath(){return qLineEditPath;};
private:
    QGridLayout qGridLayout;
    QLabel qLabel;
    QLineEdit qLineEditPath;
    QPushButton qPushButtonOpenFileManager;

};


#endif //GAMELAUNCHER_GAMESETTINGSFILEINPUT_H
