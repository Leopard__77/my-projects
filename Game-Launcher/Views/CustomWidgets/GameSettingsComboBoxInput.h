//
// Created by atlas on 7/29/22.
//

#ifndef GAMELAUNCHER_GAMESETTINGSCOMBOBOXINPUT_H
#define GAMELAUNCHER_GAMESETTINGSCOMBOBOXINPUT_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QComboBox>

class GameSettingsComboBoxInput : public QWidget {
public:
    GameSettingsComboBoxInput(QString labelText);

    QComboBox & getQComboBox(){return qComboBox;};

private:
    QGridLayout qGridLayout;
    QLabel qLabel;
    QComboBox qComboBox;
};


#endif //GAMELAUNCHER_GAMESETTINGSCOMBOBOXINPUT_H
