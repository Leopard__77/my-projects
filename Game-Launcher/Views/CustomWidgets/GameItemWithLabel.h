//
// Created by atlas on 7/14/22.
//

#ifndef GAMELAUNCHER_GAMEITEMWITHLABEL_H
#define GAMELAUNCHER_GAMEITEMWITHLABEL_H

#include <QLabel>
#include <QVBoxLayout>
#include <QMouseEvent>
#include <QModelIndex>
class GameItemWithLabel : public QFrame{
    Q_OBJECT
public:
    GameItemWithLabel(QWidget * parent, const QString& title, const QString& picUrl);

    void updateView(const QString& title, const QString& picUrl);

signals:
    void openGameSettings();
    void launchGame();
    void showGameInfo();


protected:
    void mousePressEvent(QMouseEvent* event);
private:
    QLabel gameTitle;
    QLabel gamePic;
    QVBoxLayout qvBoxLayout;
};


#endif //GAMELAUNCHER_GAMEITEMWITHLABEL_H
