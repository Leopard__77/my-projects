//
// Created by atlas on 7/13/22.
//

// You may need to build the project (run Qt uic code generator) to get "ui_MainWindow.h" resolved

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "../Controllers/DBController.h"

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
        ui(new Ui::MainWindow),
        layoutLibrary{new LayoutLibrary(5,5,5)}{
    ui->setupUi(this);
    ui->widget_2->setLayout(layoutLibrary);

}

MainWindow::~MainWindow() {
    delete ui;
}

LayoutLibrary *MainWindow::getGridLayout() {
    return layoutLibrary;
}

QPushButton *MainWindow::getPushButtonAddGame(){return this->ui->pushButtonAddGame;}

void MainWindow::closeEvent(QCloseEvent *event) {
    DBController::getInstance()->getQsqlDB().close();
}

