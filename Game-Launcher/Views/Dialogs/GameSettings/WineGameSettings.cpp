//
// Created by atlas on 22/07/22.
//

// You may need to build the project (run Qt uic code generator) to get "ui_WineGameSettings.h" resolved

#include "WineGameSettings.h"
#include "../../../Views/CustomWidgets/GameSettingsPathInput.h"
#include "../../../Views/CustomWidgets/GameSettingsCheckBoxInput.h"
#include "../../../Views/CustomWidgets/GameSettingsComboBoxInput.h"
#include "../../CustomWidgets/GameSettingsFileInput.h"

void WineGameSettings::displaySettings(int rowIndexOfGame) {
    dataWidgetMapper = new QDataWidgetMapper(this);
    dataWidgetMapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);

    dataWidgetMapper->setModel(libraryModel->getLibraryModel());
    GameSettingsFileInput * titleEdit = new GameSettingsFileInput(DB_COL_TITLE);
    GameSettingsFileInput * picEdit = new GameSettingsFileInput(DB_COL_PIC_URL);
    GameSettingsFileInput * wineExecEdit = new GameSettingsFileInput(DB_COL_WINE_EXEC_PATH);
    GameSettingsPathInput * winePrefEdit = new GameSettingsPathInput(DB_COL_WINE_PREF_PATH);
    GameSettingsFileInput * gameExec = new GameSettingsFileInput(DB_COL_GAME_EXEC_PATH);
    GameSettingsCheckBoxInput * gamemode = new GameSettingsCheckBoxInput(DB_COL_GAMEMODE);
    GameSettingsComboBoxInput * amdFsr = new GameSettingsComboBoxInput(DB_COL_AMD_FSR);
    GameSettingsCheckBoxInput * mangoHud = new GameSettingsCheckBoxInput(DB_COL_MANGOHUD);
    GameSettingsCheckBoxInput * dxvkAsync = new GameSettingsCheckBoxInput(DB_COL_DXVK_ASYNC);
    GameSettingsFileInput * runBeforeLaunch = new GameSettingsFileInput(DB_COL_PRE_BASH_SCRIPT_PATH);
    GameSettingsFileInput * runAfterLaunch = new GameSettingsFileInput(DB_COL_POST_BASH_SCRIPT_PATH);


    dataWidgetMapper->addMapping(&titleEdit->getQLineEditPath(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_TITLE));
    dataWidgetMapper->addMapping(&picEdit->getQLineEditPath(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_PIC_URL));
    dataWidgetMapper->addMapping(&wineExecEdit->getQLineEditPath(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_WINE_EXEC_PATH));
    dataWidgetMapper->addMapping(&winePrefEdit->getQLineEditPath(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_WINE_PREF_PATH));
    dataWidgetMapper->addMapping(&gameExec->getQLineEditPath(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_GAME_EXEC_PATH));
    dataWidgetMapper->addMapping(&gamemode->getQCheckBox(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_GAMEMODE));
    dataWidgetMapper->addMapping(&amdFsr->getQComboBox(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_AMD_FSR), "currentIndex");
    dataWidgetMapper->addMapping(&mangoHud->getQCheckBox(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_MANGOHUD));
    dataWidgetMapper->addMapping(&dxvkAsync->getQCheckBox(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_DXVK_ASYNC));
    dataWidgetMapper->addMapping(&runBeforeLaunch->getQLineEditPath(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_PRE_BASH_SCRIPT_PATH));
    dataWidgetMapper->addMapping(&runAfterLaunch->getQLineEditPath(), libraryModel->getLibraryModel()->fieldIndex(DB_COL_POST_BASH_SCRIPT_PATH));

    ui->gridLayout->addWidget(titleEdit);
    ui->gridLayout->addWidget(picEdit);
    ui->gridLayout->addWidget(wineExecEdit);
    ui->gridLayout->addWidget(winePrefEdit);
    ui->gridLayout->addWidget(gameExec);
    ui->gridLayout->addWidget(gamemode);
    ui->gridLayout->addWidget(amdFsr);
    ui->gridLayout->addWidget(mangoHud);
    ui->gridLayout->addWidget(dxvkAsync);
    ui->gridLayout->addWidget(runBeforeLaunch);
    ui->gridLayout->addWidget(runAfterLaunch);

    QObject::connect(this->ui->cancelButton, &QPushButton::clicked, this, &GameSettings::cancelSettings);
    QObject::connect(this->ui->saveButton, &QPushButton::clicked, this, &GameSettings::gameSettingsChanged);

    if(rowIndexOfGame == -1){
        currentRowIndex = libraryModel->getLibraryModel()->rowCount();
        libraryModel->getLibraryModel()->insertRows(currentRowIndex, 1);
        dataWidgetMapper->setCurrentIndex(currentRowIndex);
        ui->deleteButton->setDisabled(true);
    }
    else{
        dataWidgetMapper->setCurrentIndex(rowIndexOfGame);
        currentRowIndex = dataWidgetMapper->currentIndex();
        QObject::connect(this->ui->deleteButton, &QPushButton::clicked, this, &GameSettings::deleteGame);
    }
}
