//
// Created by atlas on 8/7/22.
//

#include "SteamGameSettings.h"
#include "../../../Controllers/SteamAPIController.h"
#include <QIntValidator>

void SteamGameSettings::displaySettings(int rowIndexOfGame) {
    dataWidgetMapper = new QDataWidgetMapper(this);
    dataWidgetMapper->setSubmitPolicy(QDataWidgetMapper::ManualSubmit);

    dataWidgetMapper->setModel(libraryModel->getLibraryModel());
    titleEdit = new GameSettingsFileInput(DB_COL_TITLE);
    picEdit = new GameSettingsFileInput(DB_COL_PIC_URL);
    steamIDEdit = new GameSettingsFileInput(DB_COL_STEAM_ID);
    steamIDEdit->getQLineEditPath().setValidator(new QIntValidator(this));

    dataWidgetMapper->addMapping(&titleEdit->getQLineEditPath(),
                                 libraryModel->getLibraryModel()->fieldIndex(DB_COL_TITLE));
    dataWidgetMapper->addMapping(&picEdit->getQLineEditPath(),
                                 libraryModel->getLibraryModel()->fieldIndex(DB_COL_PIC_URL));
    dataWidgetMapper->addMapping(&steamIDEdit->getQLineEditPath(),
                                 libraryModel->getLibraryModel()->fieldIndex(DB_COL_STEAM_ID));

    ui->gridLayout->addWidget(titleEdit);
    ui->gridLayout->addWidget(picEdit);
    ui->gridLayout->addWidget(steamIDEdit);


    QObject::connect(this->ui->cancelButton, &QPushButton::clicked, this, &GameSettings::cancelSettings);
    QObject::connect(this->ui->saveButton, &QPushButton::clicked, this, &GameSettings::gameSettingsChanged);
    QObject::connect(this->ui->fetchDataFromSteamButton, &QPushButton::clicked, this, &SteamGameSettings::fetchDataFromSteam);


    if (rowIndexOfGame == -1) {
        currentRowIndex = libraryModel->getLibraryModel()->rowCount();
        libraryModel->getLibraryModel()->insertRows(currentRowIndex, 1);
        dataWidgetMapper->setCurrentIndex(currentRowIndex);
        ui->deleteButton->setDisabled(true);
    } else {
        dataWidgetMapper->setCurrentIndex(rowIndexOfGame);
        currentRowIndex = dataWidgetMapper->currentIndex();
        QObject::connect(this->ui->deleteButton, &QPushButton::clicked, this, &GameSettings::deleteGame);
    }
}

void SteamGameSettings::fetchDataFromSteam() {
    SteamAPIController * apiController = new SteamAPIController();
    QObject::connect(apiController, &SteamAPIController::updateGameData, this, &SteamGameSettings::updateInputFields);
    if(!steamIDEdit->getQLineEditPath().text().isEmpty()){
       apiController->getGameData(steamIDEdit->getQLineEditPath().text().toInt());
    }

}

void SteamGameSettings::updateInputFields(const QString &title, const QString &pictureUrl, const int &steamID) {
    titleEdit->getQLineEditPath().setText(title);
    picEdit->getQLineEditPath().setText(pictureUrl);
    steamIDEdit->getQLineEditPath().setText(QString::number(steamID));
}
