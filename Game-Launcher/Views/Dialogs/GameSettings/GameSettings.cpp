//
// Created by atlas on 7/13/22.
//

// You may need to build the project (run Qt uic code generator) to get "ui_GameSettings.h" resolved

#include "GameSettings.h"
#include "ui_GameSettings.h"

GameSettings::GameSettings(QWidget *parent, LibraryModel * libraryModel) :
        QDialog(parent),
        ui(new Ui::GameSettings),
        libraryModel{libraryModel}{
    ui->setupUi(this);
}

GameSettings::~GameSettings() {
    delete ui;
}

void GameSettings::gameSettingsChanged() {
    if(dataWidgetMapper->submit()){
        libraryModel->submitToDB();
        emit updateView(libraryModel->getIdFromRowIndex(currentRowIndex));
    }
    else{
        qDebug() << "Submitting to model failed!";
    }
    this->close();
}

void GameSettings::deleteGame() {
    if(!libraryModel->getLibraryModel()->removeRow(currentRowIndex)){
        qDebug() << "Failed to delete row";
    }
    else{
        libraryModel->submitToDB();
        emit deleteView();
        this->close();
    }
}

void GameSettings::cancelSettings() {
    libraryModel->getLibraryModel()->revertAll();
    this->close();
}





