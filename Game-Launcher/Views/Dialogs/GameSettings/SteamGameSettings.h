//
// Created by atlas on 8/7/22.
//

#ifndef GAMELAUNCHER_STEAMGAMESETTINGS_H
#define GAMELAUNCHER_STEAMGAMESETTINGS_H

#include "GameSettings.h"
#include "ui_GameSettings.h"
#include "../../CustomWidgets/GameSettingsFileInput.h"

class SteamGameSettings : public GameSettings {
Q_OBJECT

public:
    using GameSettings::GameSettings;
    void displaySettings(int rowIndexOfGame);
    void fetchDataFromSteam();

    GameSettingsFileInput * getTitleEdit(){return titleEdit;};
    GameSettingsFileInput * getPicEdit(){return picEdit;};
    GameSettingsFileInput * getSteamIDEdit(){return steamIDEdit;};

public slots:
    void updateInputFields(const QString & title, const QString & pictureUrl, const int & steamID);
private:
    GameSettingsFileInput *titleEdit = nullptr;
    GameSettingsFileInput *picEdit = nullptr;
    GameSettingsFileInput *steamIDEdit = nullptr;
};

#endif //GAMELAUNCHER_STEAMGAMESETTINGS_H
