//
// Created by atlas on 7/13/22.
//

#ifndef GAMELAUNCHER_GAMESETTINGS_H
#define GAMELAUNCHER_GAMESETTINGS_H

#include <QDialog>
#include <QDataWidgetMapper>
#include "../../../Models/LibraryModel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class GameSettings; }
QT_END_NAMESPACE

class GameSettings : public QDialog {
Q_OBJECT

public:
    GameSettings(QWidget *parent = nullptr, LibraryModel * libraryModel = nullptr);
    ~GameSettings() override;
    virtual void displaySettings(int rowIndexOfGame) = 0;

public slots:
    void deleteGame();
    void gameSettingsChanged();
    void cancelSettings();


protected:
    Ui::GameSettings *ui;
    LibraryModel * libraryModel = nullptr;
    int currentRowIndex = -1;
    QDataWidgetMapper * dataWidgetMapper = nullptr;

    signals:
        void updateView(int gameDBId);
        void deleteView();
};


#endif //GAMELAUNCHER_GAMESETTINGS_H
