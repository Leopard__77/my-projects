//
// Created by atlas on 22/07/22.
//

#ifndef GAMELAUNCHER_WINEGAMESETTINGS_H
#define GAMELAUNCHER_WINEGAMESETTINGS_H

#include "GameSettings.h"
#include "ui_GameSettings.h"

class WineGameSettings : public GameSettings {
Q_OBJECT

public:
    using GameSettings::GameSettings;
    void displaySettings(int rowIndexOfGame);
};


#endif //GAMELAUNCHER_WINEGAMESETTINGS_H
