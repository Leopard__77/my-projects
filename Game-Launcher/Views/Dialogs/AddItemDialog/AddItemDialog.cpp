//
// Created by atlas on 22/07/22.
//

// You may need to build the project (run Qt uic code generator) to get "ui_AddItemDialog.h" resolved

#include "AddItemDialog.h"
#include "ui_AddItemDialog.h"
#include <QFile>

AddItemDialog::AddItemDialog(QWidget *parent) :
        QDialog(parent), ui(new Ui::AddItemDialog) {
    ui->setupUi(this);
}

AddItemDialog::~AddItemDialog() {
    delete ui;
}

QPushButton *AddItemDialog::getCancelButton() {
    return ui->cancelButton;
}

QPushButton *AddItemDialog::getAddSteamGameButton() {
    return ui->addSteamGameButton;
}

QPushButton *AddItemDialog::getAddWineGameButton() {
    return ui->addWineGameButton;
}

QPushButton *AddItemDialog::getAddSteamLibraryButton() {
    return ui->addSteamLibraryButton;
}
