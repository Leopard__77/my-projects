//
// Created by atlas on 22/07/22.
//

#ifndef GAMELAUNCHER_ADDITEMDIALOG_H
#define GAMELAUNCHER_ADDITEMDIALOG_H

#include <QDialog>


QT_BEGIN_NAMESPACE
namespace Ui { class AddItemDialog; }
QT_END_NAMESPACE

class AddItemDialog : public QDialog {
Q_OBJECT

public:
    explicit AddItemDialog(QWidget *parent = nullptr);
    ~AddItemDialog() override;

    QPushButton * getCancelButton();
    QPushButton * getAddSteamGameButton();
    QPushButton * getAddWineGameButton();
    QPushButton * getAddSteamLibraryButton();
private:
    Ui::AddItemDialog *ui;
};


#endif //GAMELAUNCHER_ADDITEMDIALOG_H
