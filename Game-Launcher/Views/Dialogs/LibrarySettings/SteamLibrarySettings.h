//
// Created by atlas on 8/7/22.
//

#ifndef GAMELAUNCHER_STEAMLIBRARYSETTINGS_H
#define GAMELAUNCHER_STEAMLIBRARYSETTINGS_H

#include <QDialog>
#include "../../../Views/CustomWidgets/GameSettingsPathInput.h"
#include "../../../Models/LibraryModel.h"

QT_BEGIN_NAMESPACE
namespace Ui { class SteamLibrarySettings; }
QT_END_NAMESPACE

class SteamLibrarySettings : public QDialog {
Q_OBJECT

public:
    SteamLibrarySettings(QWidget *parent = nullptr, LibraryModel * libraryModel = nullptr);

    ~SteamLibrarySettings() override;

    void displaySettings();
signals:
    void createSteamGameController(int rowIndex);

public slots:
    void scanForGames();
    void addGameToModel(const QString & title, const QString & pictureUrl, const int & steamID);

private:
    Ui::SteamLibrarySettings *ui;
    GameSettingsPathInput * libraryPathEdit = nullptr;
    LibraryModel * libraryModel = nullptr;
};


#endif //GAMELAUNCHER_STEAMLIBRARYSETTINGS_H
