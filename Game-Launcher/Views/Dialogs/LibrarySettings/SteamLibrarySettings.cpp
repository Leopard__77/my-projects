//
// Created by atlas on 8/7/22.
//

// You may need to build the project (run Qt uic code generator) to get "ui_SteamLibrarySettings.h" resolved

#include "SteamLibrarySettings.h"
#include "ui_SteamLibrarySettings.h"
#include "../../../Controllers/SteamAPIController.h"
#include "../../../Controllers/DBController.h"
#include "../../../Controllers/GameControllers/SteamGameController.h"
#include <QDir>

SteamLibrarySettings::SteamLibrarySettings(QWidget *parent, LibraryModel * libraryModel) :
        QDialog(parent),
        ui(new Ui::SteamLibrarySettings),
        libraryModel{libraryModel}{
    ui->setupUi(this);
    if(libraryModel == nullptr){
        qDebug() << "null";
    }
}

SteamLibrarySettings::~SteamLibrarySettings() {
    delete ui;
}

void SteamLibrarySettings::displaySettings() {
    libraryPathEdit = new GameSettingsPathInput("Location of library");
    ui->gridLayout->addWidget(libraryPathEdit);
    QObject::connect(ui->cancelButton, &QPushButton::clicked, this, &QDialog::close);
    QObject::connect(ui->continueButton, &QPushButton::clicked, this, &SteamLibrarySettings::scanForGames);
    this->exec();
}

void SteamLibrarySettings::scanForGames() {
    this->close();
    QDir library(this->libraryPathEdit->getQLineEditPath().text());
    library.setFilter(QDir::Files);
    QStringList filters;
    filters << "appmanifest_*.acf";
    library.setNameFilters(filters);


    foreach(QString game, library.entryList()){
        game.remove("appmanifest_").remove(".acf");
        SteamAPIController * apiController = new SteamAPIController();
        QObject::connect(apiController, &SteamAPIController::updateGameData, this, &SteamLibrarySettings::addGameToModel);
        apiController->getGameData(game.toInt());
    }
}

void SteamLibrarySettings::addGameToModel(const QString & title, const QString & pictureUrl, const int & steamID) {
    int newRowIndex = libraryModel->getLibraryModel()->rowCount();
    libraryModel->getLibraryModel()->insertRows(newRowIndex, 1);
    libraryModel->getLibraryModel()->setData(libraryModel->getLibraryModel()->index(newRowIndex, libraryModel->getLibraryModel()->fieldIndex(DB_COL_TITLE)), title);
    libraryModel->getLibraryModel()->setData(libraryModel->getLibraryModel()->index(newRowIndex, libraryModel->getLibraryModel()->fieldIndex(DB_COL_STEAM_ID)), steamID);
    libraryModel->getLibraryModel()->setData(libraryModel->getLibraryModel()->index(newRowIndex, libraryModel->getLibraryModel()->fieldIndex(DB_COL_PIC_URL)), pictureUrl);
    libraryModel->submitToDB();
    emit createSteamGameController(newRowIndex);
}

