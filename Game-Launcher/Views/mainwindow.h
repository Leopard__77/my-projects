//
// Created by atlas on 7/13/22.
//

#ifndef GAMELAUNCHER_MAINWINDOW_H
#define GAMELAUNCHER_MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
#include <QPushButton>
#include "../LayoutLibrary.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow() override;

    LayoutLibrary * getGridLayout();
    QPushButton* getPushButtonAddGame();;

    void closeEvent(QCloseEvent * event) override;

private:
    Ui::MainWindow *ui;
    LayoutLibrary * layoutLibrary = nullptr;
};


#endif //GAMELAUNCHER_MAINWINDOW_H
