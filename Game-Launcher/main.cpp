#include "Controllers/MainController.h"

#include <QApplication>
#include <QLoggingCategory>
#include <QStyleFactory>
#include <QFile>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QApplication::setFont(QFont("sans-serif", 10));

    QFile file(":/stylesheets/stylesheet.qss");
    file.open(QIODevice::ReadOnly);
    a.setStyleSheet(file.readAll());
    file.close();

    MainController mainController;
    return a.exec();
}
