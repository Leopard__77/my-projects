//
// Created by atlas on 8/7/22.
//

#ifndef GAMELAUNCHER_STEAMGAMECONTROLLER_H
#define GAMELAUNCHER_STEAMGAMECONTROLLER_H


#include "GameController.h"

class SteamGameController : public GameController {
Q_OBJECT
public:
    using GameController::GameController;

public slots:

    void openGameSettings();

    void gameFinished(int, QProcess::ExitStatus);

protected slots:

    void launchGame();
};

#endif //GAMELAUNCHER_STEAMGAMECONTROLLER_H
