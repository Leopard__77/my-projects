//
// Created by atlas on 8/7/22.
//

#include "SteamGameController.h"
#include "../../Views/Dialogs/GameSettings/SteamGameSettings.h"
void SteamGameController::openGameSettings() {
    GameSettings*gameSettings = new SteamGameSettings(nullptr, libraryModel);
    QObject::connect(gameSettings, &GameSettings::updateView, this, &GameController::updateController);
    QObject::connect(gameSettings, &GameSettings::deleteView, this, &GameController::deleteView);
    gameSettings->displaySettings(libraryModel->getModelRowIndex(gameDBId));
    gameSettings->exec();
}

void SteamGameController::launchGame() {
    QSqlRecord settings = libraryModel->getLibraryModel()->record(libraryModel->getModelRowIndex(gameDBId));
    QProcess * gameProcess = new QProcess(this);
    QStringList arguments;

    arguments << "steam://rungameid/"+QString::number(settings.value(DB_COL_STEAM_ID).toInt());
    gameProcess->setProgram("steam");
    gameProcess->setArguments(arguments);
    qDebug() << "steam://rungameid/"+QString::number(settings.value(DB_COL_STEAM_ID).toInt());

    connect(gameProcess, SIGNAL(finished(int , QProcess::ExitStatus )), this, SLOT(gameFinished(int , QProcess::ExitStatus )));
    gameProcess->start();
}

void SteamGameController::gameFinished(int, QProcess::ExitStatus) {

}