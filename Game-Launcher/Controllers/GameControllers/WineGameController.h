//
// Created by atlas on 22/07/22.
//

#ifndef GAMELAUNCHER_WINEGAMECONTROLLER_H
#define GAMELAUNCHER_WINEGAMECONTROLLER_H

#include "GameController.h"
class WineGameController : public GameController {
    Q_OBJECT
public:
    using GameController::GameController;

public slots:
    void openGameSettings();
    void gameFinished(int , QProcess::ExitStatus );

protected slots:

    void launchGame();
};


#endif //GAMELAUNCHER_WINEGAMECONTROLLER_H
