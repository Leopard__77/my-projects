//
// Created by atlas on 21/07/22.
//

#include <QProcess>
#include "GameController.h"
#include <QDir>
#include <QDirIterator>
#include <QDateTime>
GameController::GameController(LibraryModel * libraryModel):
    libraryModel{libraryModel}
{

}

GameController::GameController(int gameDBId, LibraryModel * libraryModel):
        libraryModel{libraryModel},
        gameDBId{gameDBId},
        gameItemWithLabel{new GameItemWithLabel(this, libraryModel->getTitle(gameDBId), libraryModel->getPicUrl(gameDBId))}
{
    QObject::connect(gameItemWithLabel, &GameItemWithLabel::openGameSettings, this, &GameController::openGameSettings);
    QObject::connect(gameItemWithLabel, &GameItemWithLabel::launchGame, this, &GameController::launchGame);
}

void GameController::updateController(int gameID){
    if(gameItemWithLabel == nullptr){
        gameDBId = gameID;
        gameItemWithLabel = new GameItemWithLabel(this, libraryModel->getTitle(gameDBId), libraryModel->getPicUrl(gameDBId));
        QObject::connect(gameItemWithLabel, &GameItemWithLabel::openGameSettings, this, &GameController::openGameSettings);
        QObject::connect(gameItemWithLabel, &GameItemWithLabel::launchGame, this, &GameController::launchGame);
    }
    else{
        gameItemWithLabel->updateView(libraryModel->getTitle(gameDBId), libraryModel->getPicUrl(gameDBId));
    }
}

void GameController::deleteView() {
    gameItemWithLabel->deleteLater();
}

void GameController::backupSaves(const QString& gameSavesDir, const QString& backupDir) {
    if(!gameSavesDir.isNull() && !backupDir.isNull() ){
        QString backupDirPath = backupDir + QDateTime::currentDateTimeUtc().toString() + "/";
        QDir dir;
        dir.mkpath(backupDirPath);

        QDirIterator it(gameSavesDir, QDirIterator::Subdirectories);
        while (it.hasNext()) {
            it.next();
            const auto fileInfo = it.fileInfo();
            if (fileInfo.isDir()) {
                const QString pathNewDir = backupDirPath + fileInfo.baseName();
                dir.mkpath(pathNewDir);
            } else if (fileInfo.isFile()) {
                const QString pathNewFile = backupDirPath + fileInfo.dir().dirName() + "/" + fileInfo.fileName();
                QFile::copy(fileInfo.absoluteFilePath(), pathNewFile);
            }
        }
    }

}