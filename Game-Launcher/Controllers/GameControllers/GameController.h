//
// Created by atlas on 21/07/22.
//

#ifndef GAMELAUNCHER_GAMECONTROLLER_H
#define GAMELAUNCHER_GAMECONTROLLER_H

#include <QObject>
#include "../../Views/CustomWidgets/GameItemWithLabel.h"
#include "../../Models/LibraryModel.h"
#include <QProcess>
class GameController : public QWidget {
    Q_OBJECT

public:
    GameController(LibraryModel * libraryModel);
    GameController(int gameDBId, LibraryModel * libraryModel);
    GameItemWithLabel * getGameItemWithLabel(){return gameItemWithLabel;};
    void backupSaves(const QString& gameSavesDir, const QString& backupDir);
public slots:
    void updateController(int gameDBId);
    virtual void openGameSettings() = 0;
    void deleteView();
    virtual void launchGame() = 0;
    virtual void gameFinished(int , QProcess::ExitStatus ) = 0;

protected:
    int gameDBId = -1;
    GameItemWithLabel * gameItemWithLabel = nullptr;
    LibraryModel * libraryModel = nullptr;
};


#endif //GAMELAUNCHER_GAMECONTROLLER_H
