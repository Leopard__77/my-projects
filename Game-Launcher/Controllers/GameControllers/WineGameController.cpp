//
// Created by atlas on 22/07/22.
//

#include <QProcess>
#include "WineGameController.h"
#include "../../Views/Dialogs/GameSettings/WineGameSettings.h"
#include <QDir>
void WineGameController::openGameSettings() {
    GameSettings * gameSettings = new WineGameSettings(nullptr, libraryModel);
    QObject::connect(gameSettings, &GameSettings::updateView, this, &GameController::updateController);
    QObject::connect(gameSettings, &GameSettings::deleteView, this, &GameController::deleteView);
    gameSettings->displaySettings(libraryModel->getModelRowIndex(gameDBId));
    gameSettings->exec();
}

void WineGameController::launchGame() {
    qDebug() << "Launching game ...";
    QSqlRecord settings = libraryModel->getLibraryModel()->record(libraryModel->getModelRowIndex(gameDBId));
    QProcess * gameProcess = new QProcess(this);
    QStringList arguments;
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
    env.insert("WINEPREFIX", settings.value(DB_COL_WINE_PREF_PATH).toByteArray());
    if(settings.value(DB_COL_MANGOHUD).toBool()){
        env.insert("MANGOHUD", "1");
    }

    env.insert("WINEFSYNC", "1");

    if(settings.value(DB_COL_GAMEMODE).toBool()){
        arguments << settings.value(DB_COL_WINE_EXEC_PATH).toByteArray();
        gameProcess->setProgram("gamemoderun");
    }else{
        gameProcess->setProgram(settings.value(DB_COL_WINE_EXEC_PATH).toByteArray());
    }

    if(settings.value(DB_COL_DXVK_ASYNC).toBool()){
        env.insert("DXVK_ASYNC","1");
    }

    if(settings.value(DB_COL_AMD_FSR).toInt()!=OFF){
        env.insert("WINE_FULLSCREEN_FSR", "1");
        env.insert("WINE_FULLSCREEN_FSR_MODE", DBController::getInstance()->getFSRModes().at(settings.value(DB_COL_AMD_FSR).toInt()).second);
    }

    QFileInfo gameExeInfo(settings.value(DB_COL_GAME_EXEC_PATH).toByteArray());
    gameProcess->setWorkingDirectory(gameExeInfo.absolutePath());
    arguments << gameExeInfo.fileName();
    qDebug() << arguments << env.toStringList();
    gameProcess->setArguments(arguments);
    gameProcess->setProcessEnvironment(env);

    if(!settings.value(DB_COL_PRE_BASH_SCRIPT_PATH).toString().isEmpty()){
        QProcess * beforeGameProcess = new QProcess(this);
        beforeGameProcess->start(settings.value(DB_COL_PRE_BASH_SCRIPT_PATH).toString());
    }

    connect(gameProcess, SIGNAL(finished(int , QProcess::ExitStatus )), this, SLOT(gameFinished(int , QProcess::ExitStatus )));
    gameProcess->start();

}

void WineGameController::gameFinished(int , QProcess::ExitStatus ){
    qDebug() << "Game finsihed";
    QSqlRecord settings = libraryModel->getLibraryModel()->record(libraryModel->getModelRowIndex(gameDBId));
    if(!settings.value(DB_COL_POST_BASH_SCRIPT_PATH).toString().isEmpty()){
            QProcess * beforeGameProcess = new QProcess(this);
            beforeGameProcess->start(settings.value(DB_COL_POST_BASH_SCRIPT_PATH).toString());
    }
    backupSaves(settings.value(DB_COL_GAME_SAVES_PATH).toString(), settings.value(DB_COL_GAME_SAVES_BACKUP_PATH).toString());
}