//
// Created by atlas on 8/7/22.
//

#include "SteamAPIController.h"
#include <QNetworkReply>
#include <QUrlQuery>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QFileInfo>

SteamAPIController::SteamAPIController() :
        manager{new QNetworkAccessManager()} {
        QObject::connect(manager, &QNetworkAccessManager::finished, this, &SteamAPIController::requestFinished);
}

void SteamAPIController::getGameData(int steamID) {
    this->steamID = steamID;
    QUrl url(STEAM_API_URL);
    QUrlQuery query;
    query.addQueryItem("appids", QString::number(steamID));
    url.setQuery(query.query());
    QNetworkRequest networkRequest(url);
    manager->get(networkRequest);
}

void SteamAPIController::requestFinished(QNetworkReply *reply) {

    QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
    QJsonObject gameJsonObj = doc.object().value(QString::number(this->steamID)).toObject();
        if (gameJsonObj.value("success").isBool() && gameJsonObj.value("success").toBool()) {
            QJsonObject gameDataJsonObj = gameJsonObj.value("data").toObject();
            if (!gameDataJsonObj.value("name").isString() || !gameDataJsonObj.value("header_image").isString()) {
                qDebug() << "Can't parse title and/or picture url";
            } else {
                title = gameDataJsonObj.value("name").toString();
                QNetworkAccessManager *pictureManager = new QNetworkAccessManager(this);
                QObject::connect(pictureManager, &QNetworkAccessManager::finished, this, &SteamAPIController::downloadPicture);
                pictureManager->get(QNetworkRequest(QUrl(gameDataJsonObj.value("header_image").toString())));
            }
        }

    reply->deleteLater();
}

void SteamAPIController::downloadPicture(QNetworkReply *reply) {
    QByteArray responseData = reply->readAll();
    QFile file(title + ".jpg");
    file.open(QIODevice::WriteOnly);
    file.write((responseData));
    file.close();
    pictureUrl = QFileInfo(file).absoluteFilePath();
    emit updateGameData(title, pictureUrl, steamID);
    reply->deleteLater();
}