//
// Created by atlas on 7/13/22.
//

#include "MainController.h"
#include <QPushButton>
#include "GameControllers/WineGameController.h"
#include "../Views/Dialogs/AddItemDialog/AddItemDialog.h"
#include "GameControllers/SteamGameController.h"
#include "../Views/Dialogs/LibrarySettings/SteamLibrarySettings.h"


MainController::MainController():
        mainWindow{new MainWindow()},
        wineLibraryModel{new LibraryModel(DB_TABLE_WINE_LIBRARY)},
        steamLibraryModel{new LibraryModel(DB_TABLE_STEAM_LIBRARY)} {

    QObject::connect(this->mainWindow->getPushButtonAddGame(), &QPushButton::clicked, this, &MainController::addItem);
    loadLibraryView();
    mainWindow->show();

}

void MainController::addItem()
{
    AddItemDialog * addItemDialog = new AddItemDialog();

    QObject::connect(addItemDialog->getCancelButton(), &QPushButton::clicked, addItemDialog, &QDialog::close);
    QObject::connect(addItemDialog->getAddWineGameButton(), &QPushButton::clicked, addItemDialog, &QDialog::close);
    QObject::connect(addItemDialog->getAddSteamGameButton(), &QPushButton::clicked, addItemDialog, &QDialog::close);
    QObject::connect(addItemDialog->getAddSteamLibraryButton(), &QPushButton::clicked, addItemDialog, &QDialog::close);

    QObject::connect(addItemDialog->getAddWineGameButton(), &QPushButton::clicked, this, &MainController::addWineGame);
    QObject::connect(addItemDialog->getAddSteamGameButton(), &QPushButton::clicked, this, &MainController::addSteamGame);
    QObject::connect(addItemDialog->getAddSteamLibraryButton(), &QPushButton::clicked, this, &MainController::addSteamLibrary);
    addItemDialog->exec();
}

void MainController::loadLibraryView() {
    for(int rowIndex = 0; rowIndex < wineLibraryModel->getLibraryModel()->rowCount(); rowIndex++){
        WineGameController * wineGameController = new WineGameController(wineLibraryModel->getIdFromRowIndex(rowIndex), wineLibraryModel);
        this->mainWindow->getGridLayout()->addWidget(wineGameController->getGameItemWithLabel());
    }
    for(int rowIndex = 0; rowIndex < steamLibraryModel->getLibraryModel()->rowCount(); rowIndex++){
        SteamGameController * steamGameController = new SteamGameController(steamLibraryModel->getIdFromRowIndex(rowIndex), steamLibraryModel);
        this->mainWindow->getGridLayout()->addWidget(steamGameController->getGameItemWithLabel());
    }
}

void MainController::addWineGame() {
    configureGame(new WineGameController(wineLibraryModel));
}

void MainController::addSteamGame() {
    configureGame(new SteamGameController(steamLibraryModel));
}

void MainController::addSteamLibrary() {
    SteamLibrarySettings * librarySettings = new SteamLibrarySettings(this, steamLibraryModel);
    librarySettings->displaySettings();
    QObject::connect(librarySettings, &SteamLibrarySettings::createSteamGameController, this, &MainController::createSteamController);
}

void MainController::configureGame(GameController *gameController) {
    gameController->openGameSettings();
    if(gameController->getGameItemWithLabel() != nullptr){
        this->mainWindow->getGridLayout()->addWidget(gameController->getGameItemWithLabel());
    }
}

void MainController::createSteamController(int rowIndex) {
    qDebug() << rowIndex;
    SteamGameController * steamGameController = new SteamGameController(steamLibraryModel->getIdFromRowIndex(rowIndex), steamLibraryModel);
    this->mainWindow->getGridLayout()->addWidget(steamGameController->getGameItemWithLabel());
}





