//
// Created by atlas on 7/18/22.
//

#include "DBController.h"
#include <QStringList>
#include <QSqlQuery>
#include <QSqlError>

DBController *DBController::getInstance() {
    static DBController * dbControllerInstance;

    if(dbControllerInstance == nullptr){
        dbControllerInstance = new DBController();
    }
    return dbControllerInstance;
}

DBController::DBController():
    db{QSqlDatabase::addDatabase(DB_DRIVER)}
{
    db.setDatabaseName(DB_NAME);
    db.open();
    initializeDB();
   // db.close();
}

void DBController::initializeDB() {
    QSqlQuery createWineTable(DBController::db);
    QSqlQuery createSteamTable(DBController::db);

    createWineTable.prepare("CREATE TABLE IF NOT EXISTS WineLibrary "
                      "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                      "Title CHAR(30) NOT NULL, "
                      "Picture_Url CHAR(50), "
                      "Wine_executable_path CHAR(100),"
                      "Wine_prefix_path CHAR(100), "
                      "Game_executable_path CHAR(100), "
                      "Gamemode BOOLEAN NOT NULL CHECK (Gamemode IN (0,1)) DEFAULT 0, "
                      "AMD_FSR INTEGER NOT NULL CHECK (AMD_FSR IN (0,1,2,3,4)) DEFAULT 0, "
                      "MangoHud BOOLEAN NOT NULL CHECK (MangoHud IN (0,1)) DEFAULT 0, "
                      "DXVK_ASYNC BOOLEAN NOT NULL CHECK (DXVK_ASYNC IN (0,1)) DEFAULT 0, "
                      "Pre_bash_script_path char(100), "
                      "Post_bash_script_path char(100) "
                      ");");

    createSteamTable.prepare("CREATE TABLE IF NOT EXISTS SteamLibrary "
                            "(ID INTEGER PRIMARY KEY AUTOINCREMENT, "
                            "SteamID INTEGER NOT NULL, "
                            "Title CHAR(30) NOT NULL, "
                            "Picture_Url CHAR(50) "
                            ");");

    if(!createWineTable.exec() || !createSteamTable.exec()) {
        qDebug() << createWineTable.lastError().text();
        qDebug() << createSteamTable.lastError().text();
    }
}



