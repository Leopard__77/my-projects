//
// Created by atlas on 7/18/22.
//

#ifndef GAMELAUNCHER_DBCONTROLLER_H
#define GAMELAUNCHER_DBCONTROLLER_H

#include <QtSql/QSqlDatabase>
#include <QList>
#include <QPair>
#include <QString>


#define DB_NAME "GameLauncher.db"
#define DB_DRIVER "QSQLITE"
#define DB_TABLE_WINE_LIBRARY "WineLibrary"
#define DB_TABLE_STEAM_LIBRARY "SteamLibrary"

#define DB_COL_ID "ID"
#define DB_COL_TITLE "Title"
#define DB_COL_PIC_URL "Picture_Url"
#define DB_COL_WINE_EXEC_PATH "Wine_executable_path"
#define DB_COL_WINE_PREF_PATH "Wine_prefix_path"
#define DB_COL_GAME_EXEC_PATH "Game_executable_path"
#define DB_COL_GAMEMODE "Gamemode"
#define DB_COL_AMD_FSR "AMD_FSR"
#define DB_COL_MANGOHUD "MangoHud"
#define DB_COL_DXVK_ASYNC "DXVK_ASYNC"
#define DB_COL_PRE_BASH_SCRIPT_PATH "Pre_bash_script_path"
#define DB_COL_POST_BASH_SCRIPT_PATH "Post_bash_script_path"
#define DB_COL_STEAM_ID "SteamID"
#define DB_COL_GAME_SAVES_PATH "Game_saves_path"
#define DB_COL_GAME_SAVES_BACKUP_PATH"Game_saves_backup_path"

enum AMD_FSR_MODES {
        OFF,
        ULTRA,
        QUALITY,
        BALANCED,
        PERFORMANCE
};

class DBController {
public:
    static DBController * getInstance();

    QSqlDatabase getQsqlDB(){return db;};

    QList<QPair<int, QString>> & getFSRModes(){return fsrModes;};

private:
    DBController();
    void initializeDB();
    QSqlDatabase db;

    QList<QPair<int, QString>> fsrModes = {
            QPair<int, QString>(OFF, "off"),
            QPair<int, QString>(ULTRA, "ultra"),
            QPair<int, QString>(QUALITY, "quality"),
            QPair<int, QString>(BALANCED, "balanced"),
            QPair<int, QString>(PERFORMANCE, "performance"),
    };

};


#endif //GAMELAUNCHER_DBCONTROLLER_H
