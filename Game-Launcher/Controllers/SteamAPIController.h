//
// Created by atlas on 8/7/22.
//

#ifndef GAMELAUNCHER_STEAMAPICONTROLLER_H
#define GAMELAUNCHER_STEAMAPICONTROLLER_H

#include <QNetworkAccessManager>
#include <QObject>
#include <QLineEdit>
#define STEAM_API_URL "https://store.steampowered.com/api/appdetails?appids="

class SteamAPIController : public QObject{
    Q_OBJECT
public:
    SteamAPIController();
    void getGameData(int steamID);
    signals:
        void updateGameData(const QString & title, const QString & pictureUrl, const int & steamID);

    public slots:
        void requestFinished(QNetworkReply *reply);
        void downloadPicture(QNetworkReply * reply);

private:
    QNetworkAccessManager * manager = nullptr;
    int steamID = -1;
    QString title;
    QString pictureUrl;

};


#endif //GAMELAUNCHER_STEAMAPICONTROLLER_H
