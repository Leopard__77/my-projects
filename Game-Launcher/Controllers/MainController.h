//
// Created by atlas on 7/13/22.
//

#ifndef GAMELAUNCHER_MAINCONTROLLER_H
#define GAMELAUNCHER_MAINCONTROLLER_H


#include "../Views/mainwindow.h"
#include "../Models/LibraryModel.h"
#include "../Views/CustomWidgets/GameItemWithLabel.h"
#include "GameControllers/GameController.h"

class MainController : public QWidget {
Q_OBJECT
public:
    MainController();

public slots:
    void addItem();
    void addWineGame();
    void addSteamGame();
    void addSteamLibrary();
    void createSteamController(int rowIndex);

private:
    MainWindow * mainWindow = nullptr;
    LibraryModel * wineLibraryModel = nullptr;
    LibraryModel * steamLibraryModel = nullptr;

    void configureGame(GameController * gameController);
    void loadLibraryView();

};


#endif //GAMELAUNCHER_MAINCONTROLLER_H
