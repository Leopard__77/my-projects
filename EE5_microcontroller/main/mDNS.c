//
// Created by atlas on 3/22/22.
//
#include "mdns.h"
#include "storage_handler.h"
#include "string.h"
void init_mdns(){

    ESP_ERROR_CHECK(mdns_init());
    char baseName[15] = "esp32-un";
    ESP_LOGI("mDNS", "Hostname: %s", baseName);
    ESP_ERROR_CHECK(mdns_hostname_set(baseName));
    ESP_ERROR_CHECK(mdns_instance_name_set(baseName));
    ESP_ERROR_CHECK(mdns_service_add(baseName, "_http", "_tcp", 80, NULL, 0));
}

void updateHostname(){
    u_int32_t id = 0;
    esp_err_t errId = nvs_get_id(&id);
    if(errId == ESP_OK) {
        char baseName[15];
        snprintf(baseName, sizeof baseName, "esp32-%d", id);
        ESP_ERROR_CHECK(mdns_hostname_set(baseName));
        ESP_ERROR_CHECK(mdns_instance_name_set(baseName));
        ESP_ERROR_CHECK(mdns_service_remove("_http","_tcp"));
        ESP_ERROR_CHECK(mdns_service_add(baseName, "_http", "_tcp", 80, NULL, 0));
    }
}