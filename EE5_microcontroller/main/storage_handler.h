#ifndef EE5_ESP32_STORAGE_HANDLER_H
#define EE5_ESP32_STORAGE_HANDLER_H

#include <nvs.h>
#include "nvs_flash.h"
#include "esp_log.h"

/***
 * @return
 * Creates a handles that is used to store/retrieve data from non-volatile flash memory
 */
nvs_handle_t create_handle();

/***
 * @param value
 * Stores the given id into the non-volatile flash memory
 * Returns ESP_OK if successful, ESP_FAIL in case of an error
 */
esp_err_t nvs_store_id(u_int32_t value);

/***
 * @param id
 * Provide int in which the ID will be stored
 * Returns ESP_OK if successful, ESP_FAIL in case of an error
 */

esp_err_t nvs_get_id(u_int32_t * id);

/***
 * @param refreshToken
 * @param accessToken
 * Provide access & refresh tokens that need to be stored in the non-volatile flash memory
 * Returns ESP_OK if successful, ESP_FAIL in case of an error
 */
esp_err_t nvs_store_tokens(const char * refreshToken, const char * accessToken);

/***
 * @param access_token
 * Provide pointer to a char in which the access token will be stored
 * Returns ESP_OK if successful, ESP_FAIL or ESP_ERR_NVS_INVALID_LENGTH in case of an error
 */
esp_err_t nvs_get_accessToken(char * access_token);


/***
 * Erases all tokens stored in NVS
 */
void erase_tokens();

/***
 * Erases ID from NVS
 */
void erase_id();

esp_err_t nvs_store_thresholds(u_int32_t light, u_int32_t moisture, u_int32_t temp, u_int8_t lightStartTimeHours, u_int8_t lightStartTimeMinutes, u_int8_t lightDurationHours);
esp_err_t nvs_get_thresholds(u_int32_t * light, u_int32_t * moisture, u_int32_t * temp);
esp_err_t nvs_get_time_thresholds( u_int8_t * lightStartTimeHours, u_int8_t * lightStartTimeMinutes, u_int8_t * lightDurationHours);

#endif //EE5_ESP32_STORAGE_HANDLER_H
