//
// Created by atlas on 4/4/22.
//

#ifndef EE5_ESP32_MDNS_H
#define EE5_ESP32_MDNS_H
#include <string.h>
#include "esp_netif_ip_addr.h"
#include "esp_mac.h"
#include "mdns.h"
#include "netdb.h"

void init_mdns();
void updateHostname();
#endif //EE5_ESP32_MDNS_H
