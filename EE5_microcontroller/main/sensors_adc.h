#ifndef EE5_ESP32_SENSORS_ADC_H
#define EE5_ESP32_SENSORS_ADC_H

#include <stdlib.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/adc.h"
#include "driver/mcpwm.h"
#include <driver/gptimer.h>
struct SensorData_t{
    u_int32_t temperature;
    u_int32_t moisture;
    u_int32_t light;
    u_int32_t waterlvl;
    u_int32_t light_led;
};
typedef struct SensorData_t SensorData_t;

_Noreturn void perform_adc(void * iTaskData);
_Noreturn void perform_feedback(void * iTaskData);
_Noreturn void timerTask(void *taskData);
void updateTimer(gptimer_handle_t *gptimer, struct tm *currentTimeHM, struct tm *thresholdTimeHM, const bool *timerValue, const u_int8_t * duration);
void feedback_temperature(const u_int32_t * tempValue, const u_int32_t * tempThreshold);
void feedback_light(const u_int32_t * lightValue, const u_int32_t * lightThreshold);
void feedback_moisture(const u_int32_t * moistureValue, const u_int32_t * moistureThreshold, const u_int32_t * waterlvl);
void stop_pump();
esp_err_t setStatusSensors(int newLvl);
void init_mcpwm_signal(mcpwm_io_signals_t io_signal, mcpwm_timer_t timer, int gpioPin, u_int32_t frequency, float initialDutyCycle);
void init_lights();
#endif //EE5_ESP32_SENSORS_ADC_H
