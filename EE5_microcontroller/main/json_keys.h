/***
 * This header file contains keys in order to prevent hard coding them into the code. These are json keys (used in request) but also keys that are used to store/read data from the NVS
 */

#ifndef EE5_ESP32_JSON_KEYS_H
#define EE5_ESP32_JSON_KEYS_H

#define ACCESSTOKEN_KEY "AccessToken"
#define REFRESHTOKEN_KEY "RefreshToken"
#define PLANTID_KEY "plantId"

#define JSON_THRESHOLD_LIGHT_KEY "lightThreshold"
#define JSON_THRESHOLD_TEMPERATURE_KEY "tempThreshold"
#define JSON_THRESHOLD_MOISTURE_KEY "moistureThreshold"
#define JSON_THRESHOLD_LIGHT_START_TIME "lightStartTime"
#define JSON_THRESHOLD_LIGHT_DURATION "lightHours"

#define NVS_THRESHOLD_LIGHT_KEY "light"
#define NVS_THRESHOLD_TEMPERATURE_KEY "temp"
#define NVS_THRESHOLD_MOISTURE_KEY "moist"
#define NVS_THRESHOLD_START_TIME_HOURS "lstH"
#define NVS_THRESHOLD_START_TIME_MINUTES "lstMin"
#define NVS_THRESHOLD_LIGHT_DURATION "lDur"
#define NVS_ACCESSTOKEN_KEY "AT"
#define NVS_REFRESHTOKEN_KEY "RT"

#define PUMP_STATUS_KEY "Status"
#define LIGHTS_STATUS_KEY "Level"


#endif //EE5_ESP32_JSON_KEYS_H
