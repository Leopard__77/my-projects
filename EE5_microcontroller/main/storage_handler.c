#include "storage_handler.h"
#include "json_keys.h"

static char * NVS_HANDLER_TAG = "NVS Handler";

nvs_handle_t create_handle(){
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );

    nvs_handle_t nvsHandle;
    err = nvs_open("custom_storage", NVS_READWRITE, &nvsHandle);
    if(err == ESP_OK){
        return nvsHandle;
    }
    else{
        return ESP_FAIL;
    }
}

void erase_tokens(){
    nvs_handle_t nvsHandle = create_handle();
    nvs_erase_key(nvsHandle, NVS_ACCESSTOKEN_KEY);
    nvs_erase_key(nvsHandle, NVS_REFRESHTOKEN_KEY);
    nvs_commit(nvsHandle);
    nvs_flash_deinit();
}

void erase_id(){
    nvs_handle_t nvsHandle = create_handle();
    nvs_erase_key(nvsHandle, PLANTID_KEY);
    nvs_commit(nvsHandle);
    nvs_flash_deinit();
}

esp_err_t nvs_store_id(const u_int32_t value){
    nvs_handle_t nvsHandle = create_handle();
    if(nvsHandle != ESP_FAIL){
        esp_err_t err = nvs_set_u32(nvsHandle, PLANTID_KEY, value);
        if(err == ESP_OK){
            nvs_commit(nvsHandle);
            nvs_close(nvsHandle);
            ESP_LOGI(NVS_HANDLER_TAG, "Int Stored");
            return ESP_OK;
        }
        else{
            ESP_LOGW(NVS_HANDLER_TAG, "Unable to set u32");
            nvs_close(nvsHandle);
            return ESP_FAIL;
        }

    }
    else{
        ESP_LOGW(NVS_HANDLER_TAG, "Error with nvs handler");
        return ESP_FAIL;
    }
}

esp_err_t nvs_store_thresholds(const u_int32_t light, const u_int32_t moisture, const u_int32_t temp, const u_int8_t lightStartTimeHours, const u_int8_t lightStartTimeMinutes, const u_int8_t lightDurationHours){
    nvs_handle_t nvsHandle = create_handle();
    esp_err_t err_light, err_moisture, err_temp, err_lightStartHours, err_lightStartMinutes, err_lightDuration;
    if(nvsHandle != ESP_FAIL){
        err_light = nvs_set_u32(nvsHandle, NVS_THRESHOLD_LIGHT_KEY, light);
        err_moisture = nvs_set_u32(nvsHandle, NVS_THRESHOLD_MOISTURE_KEY, moisture);
        err_temp = nvs_set_u32(nvsHandle, NVS_THRESHOLD_TEMPERATURE_KEY, temp);
        err_lightStartHours = nvs_set_u8(nvsHandle, NVS_THRESHOLD_START_TIME_HOURS, lightStartTimeHours);
        err_lightStartMinutes = nvs_set_u8(nvsHandle, NVS_THRESHOLD_START_TIME_MINUTES, lightStartTimeMinutes);
        err_lightDuration = nvs_set_u8(nvsHandle, NVS_THRESHOLD_LIGHT_DURATION, lightDurationHours);
        if(err_moisture == ESP_OK && err_light == ESP_OK && err_temp == ESP_OK && err_lightDuration == ESP_OK && err_lightStartMinutes == ESP_OK && err_lightStartHours == ESP_OK){
            nvs_commit(nvsHandle);
            nvs_close(nvsHandle);
//            ESP_LOGI(NVS_HANDLER_TAG, "New thresholds Stored");
            return ESP_OK;
        }
        else{
            nvs_close(nvsHandle);
            return ESP_FAIL;
        }
    }
    else{
        ESP_LOGW(NVS_HANDLER_TAG, "Error with nvs handler");
        return ESP_FAIL;
    }
}

esp_err_t nvs_get_thresholds(u_int32_t * light, u_int32_t * moisture, u_int32_t * temp){
    nvs_handle_t nvsHandle = create_handle();
    esp_err_t err_light, err_moisture, err_temp;
    if(nvsHandle != ESP_FAIL){
        err_light = nvs_get_u32(nvsHandle, NVS_THRESHOLD_LIGHT_KEY, light);
        err_moisture = nvs_get_u32(nvsHandle, NVS_THRESHOLD_MOISTURE_KEY, moisture);
        err_temp = nvs_get_u32(nvsHandle, NVS_THRESHOLD_TEMPERATURE_KEY, temp);

        nvs_close(nvsHandle);
        if(err_moisture == ESP_OK && err_light == ESP_OK && err_temp == ESP_OK){
//            ESP_LOGI(NVS_HANDLER_TAG, "Thresholds successfully returned");
            return ESP_OK;
        }
        else{
            //ESP_LOGW(NVS_HANDLER_TAG, "Can't get thresholds");
            return ESP_FAIL;
        }
    }
    else{
        ESP_LOGW(NVS_HANDLER_TAG, "Error with nvs handler");
        return ESP_FAIL;
    }
}

esp_err_t nvs_get_time_thresholds(u_int8_t * lightStartTimeHours, u_int8_t * lightStartTimeMinutes, u_int8_t * lightDurationHours){
    nvs_handle_t nvsHandle = create_handle();
    esp_err_t err_lightStartHours, err_lightStartMinutes, err_lightDuration;
    if(nvsHandle != ESP_FAIL){
        err_lightStartHours = nvs_get_u8(nvsHandle, NVS_THRESHOLD_START_TIME_HOURS, lightStartTimeHours);
        err_lightStartMinutes = nvs_get_u8(nvsHandle, NVS_THRESHOLD_START_TIME_MINUTES, lightStartTimeMinutes);
        err_lightDuration = nvs_get_u8(nvsHandle, NVS_THRESHOLD_LIGHT_DURATION, lightDurationHours);

        nvs_close(nvsHandle);
        if(err_lightDuration == ESP_OK && err_lightStartMinutes == ESP_OK && err_lightStartHours == ESP_OK){
            //ESP_LOGI(NVS_HANDLER_TAG, "Time thresholds successfully returned");
            return ESP_OK;
        }
        else{
            //ESP_LOGW(NVS_HANDLER_TAG, "Can't get thresholds");
            return ESP_FAIL;
        }
    }
    else{
        ESP_LOGW(NVS_HANDLER_TAG, "Error with nvs handler");
        return ESP_FAIL;
    }
}

esp_err_t nvs_get_id(u_int32_t * ID){
    nvs_handle_t nvsHandle = create_handle();
    if(nvsHandle != ESP_FAIL){
        esp_err_t err = nvs_get_u32(nvsHandle, PLANTID_KEY, ID);
        nvs_close(nvsHandle);
        if(err == ESP_OK){
            ESP_LOGI(NVS_HANDLER_TAG, "ID successfully returned");
            return ESP_OK;
        }
        else{
            ESP_LOGW(NVS_HANDLER_TAG, "Can't get ID");
            return ESP_FAIL;
        }
    }
    else{
        ESP_LOGW(NVS_HANDLER_TAG, "Error with nvs handler");
        return ESP_FAIL;
    }
}

esp_err_t nvs_store_tokens(const char * refreshToken, const char * accessToken){
    nvs_handle_t nvsHandle = create_handle();
    if(nvsHandle != ESP_FAIL){
        esp_err_t err = nvs_set_str(nvsHandle, NVS_REFRESHTOKEN_KEY, refreshToken);
        err = nvs_set_str(nvsHandle, NVS_ACCESSTOKEN_KEY, accessToken);
        if(err == ESP_OK){
            nvs_commit(nvsHandle);
            ESP_LOGI(NVS_HANDLER_TAG, "Tokens Stored");
            nvs_close(nvsHandle);
            return ESP_OK;
        }
        else{
            ESP_LOGW(NVS_HANDLER_TAG, "Unable to set tokens");
            nvs_close(nvsHandle);
            return ESP_FAIL;
        }

    }
    else{
        ESP_LOGW(NVS_HANDLER_TAG, "Error with nvs handler");
        return ESP_FAIL;
    }
}

esp_err_t nvs_get_accessToken(char * accessToken){
    size_t size_access_token = sizeof(char) * 128 +1;
    nvs_handle_t nvsHandle = create_handle();
    if(nvsHandle != ESP_FAIL){
        esp_err_t err = nvs_get_str(nvsHandle, NVS_ACCESSTOKEN_KEY, accessToken, &size_access_token);
        nvs_close(nvsHandle);
        if(err == ESP_OK){
            ESP_LOGI(NVS_HANDLER_TAG, "Successfully returned access token");

        }
        else if(err == ESP_ERR_NVS_NOT_FOUND){
            ESP_LOGW(NVS_HANDLER_TAG, "Couldn't find any access token");
        }
        else if(err == ESP_ERR_NVS_INVALID_LENGTH){
            ESP_LOGW(NVS_HANDLER_TAG, "Char array to small to store access token ...");
        }
        else{
            ESP_LOGW(NVS_HANDLER_TAG, "Can't get access token from NVS");
        }
        return err;

    }
    else{
        ESP_LOGW(NVS_HANDLER_TAG, "Error with nvs handler");
        return ESP_FAIL;
    }


}