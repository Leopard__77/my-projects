#include <sys/cdefs.h>
#include "http_client.h"
#include "storage_handler.h"
#include "sensors_adc.h"
#include "main.h"
#include "mDNS.h"

#define BUFFER_RESIZE_FACTOR 2
#define BUFFER_SIZE 128
extern TaskHandle_t mainTask;
extern const char API_root_cert_pem_start[] asm("_binary_API_root_cert_pem_start");
const char *TAG_HTTP_CLIENT = "HTTP_CLIENT";

esp_err_t http_client_event_handler(esp_http_client_event_t *evt) {
    static int length_transferred;
    struct RequestBuffer *requestBuffer = (struct RequestBuffer *) evt->user_data;
    if (evt->event_id == HTTP_EVENT_ON_DATA) { //if data is being received
        ESP_LOGI(TAG_HTTP_CLIENT, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);

        if (requestBuffer->buffer == NULL) {
            ESP_LOGI(TAG_HTTP_CLIENT, "Creating buffer ...");
            char newBuffer[BUFFER_SIZE];
            requestBuffer->buffer = newBuffer;
            requestBuffer->size = BUFFER_SIZE;
        }
        if (length_transferred + evt->data_len > requestBuffer->size) {
            char newBuffer[BUFFER_SIZE * BUFFER_RESIZE_FACTOR];
            requestBuffer->size = BUFFER_SIZE * BUFFER_RESIZE_FACTOR;
            memcpy(newBuffer, requestBuffer->buffer, length_transferred); //copy everything to the bigger buffer
            ESP_LOGI(TAG_HTTP_CLIENT, "Buffer too small, creating bigger buffer ...");
        }
        if (requestBuffer->buffer != NULL) {
            ESP_LOGI(TAG_HTTP_CLIENT, "Transferring data to buffer ...");
            memcpy(requestBuffer->buffer + length_transferred, evt->data, evt->data_len);
            length_transferred += evt->data_len;
        } else {
            ESP_LOGE(TAG_HTTP_CLIENT, "Memory allocation for buffer has failed!");
            return ESP_FAIL;
        }
    }
    if (evt->event_id == HTTP_EVENT_ON_FINISH) {
        length_transferred = 0;
    }
    if(evt->event_id == HTTP_EVENT_ERROR){
        ESP_LOGW(TAG_HTTP_CLIENT, "HTTP Error");
        int status_code = esp_http_client_get_status_code(evt->client);
        ESP_LOGW(TAG_HTTP_CLIENT, "Status code %d ", status_code);
        esp_http_client_close(evt->client);
    }
    return ESP_OK;
}

void init_post_req_conf(esp_http_client_config_t *httpClientConfig, const char *apiPath, struct RequestBuffer *requestBuffer) {
    esp_http_client_config_t localHttpClientConfig = {
            .host = CONFIG_API_HOST,
            .path =  apiPath,
            .transport_type = HTTP_TRANSPORT_OVER_SSL,
            .method = HTTP_METHOD_POST,
            .cert_pem = API_root_cert_pem_start,
            .event_handler = http_client_event_handler,
            .user_data = requestBuffer
    };
    *httpClientConfig = localHttpClientConfig;
}

void exec_post_req(const esp_http_client_config_t *httpClientConfig, const char *accessToken, const char *data, void (*funcToExec)(char *jsonStr)) {
    esp_http_client_handle_t httpClientHandle = esp_http_client_init(httpClientConfig);
    if (httpClientHandle != NULL) {
        esp_http_client_set_post_field(httpClientHandle, data, strlen(data));
        esp_http_client_set_header(httpClientHandle, "Authorization", accessToken);
        if (esp_http_client_perform(httpClientHandle) == ESP_OK) {
            int status_code = esp_http_client_get_status_code(httpClientHandle);
            struct RequestBuffer *requestBufferUserData;
            switch (status_code) {
                case HttpStatus_Unauthorized: //access token invalid
                    ESP_LOGW(TAG_HTTP_CLIENT, "Status code %d clearing tokens ...", HttpStatus_Unauthorized);
                    erase_tokens();
                    break;
                case HttpStatus_Ok:
                    requestBufferUserData = (struct RequestBuffer *) httpClientConfig->user_data;
//                    ESP_LOG_BUFFER_CHAR(TAG_HTTP_CLIENT, requestBufferUserData->buffer,
//                                        strlen(requestBufferUserData->buffer));
                    if (funcToExec != NULL) {
                        funcToExec(requestBufferUserData->buffer);
                    }
                    break;
                default:
                    ESP_LOGW(TAG_HTTP_CLIENT, "Status code %d", HttpStatus_Unauthorized);
                    break;
            }
        }
        esp_http_client_cleanup(httpClientHandle);
    } else {
        ESP_LOGE(TAG_HTTP_CLIENT, "Error with http handle");
    }

}

void handle_json_new_plant(char *jsonStr) {
    cJSON *json = cJSON_Parse(jsonStr);
    if (json != NULL) {
        cJSON *plantid = cJSON_GetObjectItem(json, PLANTID_KEY);
        if (cJSON_IsNumber(plantid)) {
            esp_err_t errStoringId = nvs_store_id(plantid->valueint);
            if (errStoringId == ESP_OK) {
                updateHostname();
                xTaskNotify(mainTask,0, eNoAction); //Notify main task, we got an ID
            }
        } else {
            ERR_JSON_PARSE(TAG_HTTP_CLIENT);
        }
    } else {
        ERR_JSON_PARSE(TAG_HTTP_CLIENT);
    }
}

void handle_json_post_measurement(char *jsonStr) {
    cJSON *thresholdsJson = cJSON_Parse(jsonStr);
    if (thresholdsJson != NULL) {
        cJSON * lightThreshold = cJSON_GetObjectItem(thresholdsJson, JSON_THRESHOLD_LIGHT_KEY);
        cJSON * moistureThreshold = cJSON_GetObjectItem(thresholdsJson, JSON_THRESHOLD_MOISTURE_KEY);
        cJSON * lightStartTime = cJSON_GetObjectItem(thresholdsJson, JSON_THRESHOLD_LIGHT_START_TIME);
        cJSON * lightDuration = cJSON_GetObjectItem(thresholdsJson, JSON_THRESHOLD_LIGHT_DURATION);
        cJSON *tempThreshold = cJSON_GetObjectItem(thresholdsJson, JSON_THRESHOLD_TEMPERATURE_KEY);
        if (cJSON_IsNumber(lightThreshold) && cJSON_IsNumber(moistureThreshold) && cJSON_IsString(lightStartTime) && cJSON_IsNumber(lightDuration) && cJSON_IsNumber(tempThreshold)) {
            //parse time
            struct tm tm_parsed_data;
            tm_parsed_data.tm_sec = 0;
            char *s = strptime(lightStartTime->valuestring, "%Y-%m-%dT%H:%M%Z", &tm_parsed_data);
            if (s == NULL) {
                ESP_LOGE("DATE", "Cannot parse date");
            }
            else{
                esp_err_t espErr = nvs_store_thresholds(lightThreshold->valueint, moistureThreshold->valueint, tempThreshold->valueint, tm_parsed_data.tm_hour, tm_parsed_data.tm_min, lightDuration->valueint);
                if (espErr == ESP_OK) {
                    ESP_LOGI(TAG_HTTP_CLIENT, "New threshold: light: %d - moisture: %d - temp: %d lightStartHours: %d - lightStartMinutes: %d - duration: %d", lightThreshold->valueint, moistureThreshold->valueint, thresholdsJson->valueint, tm_parsed_data.tm_hour, tm_parsed_data.tm_min, lightDuration->valueint);
                }
                else {
                    ERR_JSON_PARSE(TAG_HTTP_CLIENT);
                }
            }
        }
        else{
            ERR_JSON_PARSE(TAG_HTTP_CLIENT);
        }
    }
    else{
        ERR_JSON_PARSE(TAG_HTTP_CLIENT);
    }
}

void register_plant() {
    char *newPlant = "{\"name\":\"ESP-TEST-POT\"}";
    char accessToken[64];
    esp_err_t errAccessToken = nvs_get_accessToken(accessToken);
    if (errAccessToken == ESP_ERR_NVS_INVALID_LENGTH) {
        char accessTokenLong[128];
        errAccessToken = nvs_get_accessToken(accessTokenLong);
    }
    if (errAccessToken == ESP_OK) {
        struct RequestBuffer requestBuffer;
        char buffer[BUFFER_SIZE] = {0};
        requestBuffer.buffer = buffer;
        requestBuffer.size = BUFFER_SIZE;
        esp_http_client_config_t httpClientConfig;
        init_post_req_conf(&httpClientConfig, CONFIG_API_PATH_PLANT_CREATE, &requestBuffer);
        ESP_LOGI(TAG_HTTP_CLIENT, "Path %s", httpClientConfig.path);
        exec_post_req(&httpClientConfig, accessToken, newPlant, handle_json_new_plant);
    } else {
        ESP_LOGE(TAG_HTTP_CLIENT, "Access Token error");
    }
}

_Noreturn void post_measurements(void *iTaskData) {
    struct MeasurementHandlers *measurementHandlers = (struct MeasurementHandlers *) iTaskData;

    u_int32_t id = 0;
    esp_err_t errId = nvs_get_id(&id);

    char accessToken[64];
    esp_err_t errAccessToken = nvs_get_accessToken(accessToken);
    if (errAccessToken == ESP_ERR_NVS_INVALID_LENGTH) {
        char accessTokenLong[128];
        errAccessToken = nvs_get_accessToken(accessTokenLong);
    }
    if (errId == ESP_OK && errAccessToken == ESP_OK) {
        SensorData_t sensorData;
        char data[128];
        esp_http_client_config_t httpClientConfig;
        esp_err_t espErr;
        struct RequestBuffer requestBuffer;
        char buffer[BUFFER_SIZE] = {0};
        requestBuffer.buffer = buffer;
        requestBuffer.size = BUFFER_SIZE;

        init_post_req_conf(&httpClientConfig, CONFIG_API_PATH_POST_DATA, &requestBuffer);
        for (;;) {
            espErr = readFromBuffer(&measurementHandlers->bufferHandles, &sensorData, READER_2);
            if(espErr == ESP_OK) {
                sprintf(data, "{\"plantId\":\"%d\","
                              "\"temperature\":\"%d\","
                              "\"light\":\"%d\","
                              "\"moisture\":\"%d\","
                              "\"waterLevel\":\"%d\"}", id, sensorData.temperature, sensorData.light, sensorData.moisture,sensorData.waterlvl);
               exec_post_req(&httpClientConfig, accessToken, data, handle_json_post_measurement);
            }
            else{
                xTaskNotifyWait(0x00, ULONG_MAX, NULL, portMAX_DELAY);
            }
        }
    }
}