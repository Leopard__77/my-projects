
#ifndef EE5_ESP32_BUFFER_H
#define EE5_ESP32_BUFFER_H
#define READER_1 0
#define READER_2 1

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "sensors_adc.h"

/***
 * A buffer is needed between the 3 concurrently running tasks, perform_adc (writer), post_measurement(reader) and perform_feedback(reader).
 * There a 2 main options to solve this, a queue or a messageBuffer, however:
 *
 * A message buffer can only have 1 writer and 1 reader at all times. Thus, the code would need 2 message buffers,
 * one between perform_adc (writer) and post_measurement(reader); and a second one between perform_adc (writer) and perform_feedback(reader).
 * These two buffers contain EXACTLY the same data
 *
 * The second option is a queue. This allows for multiple readers and writers but both readers need to be able to read the same data.
 * xQueueReceive() reads and deletes data immediately. xQueuePeek allows for reading only but will always "peek" the same data as long as data is not removed.
 * Therefore there will always be a conflict between the readers, there is a race condition in deleting the data from the buffer.
 *
 * To avoid all these disadvantages, we opted for a custom buffer implementation with binary semaphores and mutexes, based on the idea of Read Write Locks. As a side node,
 * binary semaphores can be more efficiently implemented using "Direct to Task Notifications" but in this application, this could possibly lead to writers starvation.
 * Because the writer is locked out while the readers are reading AND doing something with the data. Plus there seems to be no way to prefer the writer. Working with "actual"
 * semaphores allows us to increase TaskPriority of the writer, giving it preferences in acquiring a lock (https://www.freertos.org/RTOS-task-priority.html), thus
 * avoiding writer starvation. When the buffer is full the writer will stop writing and give readers priority, this way also reader starvation is avoided.
 *
 * */


struct BufferData_t {
    SensorData_t sensorData;
    bool readByReader1;
    bool readByReader2;
};
typedef struct BufferData_t BufferData_t;

struct BufferHandles_t {
    SemaphoreHandle_t semaphoreHandle;
    SemaphoreHandle_t mutexHandle;
    u_int8_t nrOfReaders;
    u_int16_t nrOfElements;
    BufferData_t *buffer;
    u_int16_t bufferSize;
    bool bufferFull;
    TaskHandle_t *taskToNotify;
};

typedef struct BufferHandles_t BufferHandles_t;

/***
 * Function to initialize the buffer, here the semaphore, mutex and other parameters from BufferHandles_t are initialized
 ***/
void initBufferLocks(BufferHandles_t *bufferHandles, BufferData_t *newBuffer, u_int16_t buffSize, TaskHandle_t *taskToNotify);

/***
 * This function is used to acquire a readLock, this allows multiple readers to read at the same time.
 * But FreeRTOS has no native support for readlocks, so the behaviour is simulated with semaphores. Simply put
 * We only allow one reader to take the semaphore but we keep a counter (nrOfReaders). If the readers is finished with reading,
 * the counter is decreased and if the counter reaches 0, the semaphore is given back.
 */
void takeBufferReadLock(BufferHandles_t *bufferHandles);

/***
 * If the counter nrOfReaders reaches 0 the semaphore is given back and a writer (or again reader(s)) can acquire the semaphore
 */
void giveBufferReadLock(BufferHandles_t *bufferHandles);

/***
 * Only one task should be allowed to writer into the buffer at a given moment in time. Thus again a semaphore lock is taken,
 * now no reader can acquire the semaphore and will be waiting until the writer is done. We also increase the priority of the writer
 * task in order to prevent writer starvation (tasks with high priority get the semaphore first).
 */
void takeBufferWriteLock(BufferHandles_t *bufferHandles, TaskHandle_t *taskHandle);

/***
 * In this function the writer returns the semaphore and it's priority is set back to normal
 */
void giveBufferWriteLock(BufferHandles_t *bufferHandles, TaskHandle_t *taskHandle);

/***
 * This function checks which data was not yet read by a certain reader (reading_task) and returns
 * the data. If all the data was already read by the given reader ESP_FAIL is returned and the task will be blocked
 * until the writer inserts data again. Of course, shared variables between the readers and writers are protected by a semaphore or mutex.
 */

esp_err_t readFromBuffer(BufferHandles_t *bufferHandles, SensorData_t *data, u_int8_t readingTask);

/***
 * This function is used to insert new data into the buffer. Firstly, the function checks if there is data that is read by
 * both reader. If yes, then this data is overwritten. If the buffer is full, ESP_FAIL is returned and the writer will wait until readers
 * have read some data. Of course, shared variables between the readers and writers are protected by a semaphore or mutex.
 */
esp_err_t writeToBuffer(BufferHandles_t *bufferHandles, SensorData_t *data, TaskHandle_t *taskHandle);

#endif //EE5_ESP32_BUFFER_H
