
#ifndef EE5_ESP32_MAIN_H
#define EE5_ESP32_MAIN_H
#include "buffer.h"

struct MeasurementHandlers{
    BufferHandles_t bufferHandles;
    TaskHandle_t post_measurements_task_handle;
    TaskHandle_t perform_adc_task_handle;
    TaskHandle_t perform_feedback_task_handle;
    esp_timer_handle_t timer_handle;
};

#endif //EE5_ESP32_MAIN_H
