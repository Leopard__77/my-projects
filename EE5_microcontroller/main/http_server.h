#include <esp_http_server.h>
#include "json_keys.h"
#include "http_client.h"

#ifndef EE5_ESP32_HTTP_SERVER_H
#define EE5_ESP32_HTTP_SERVER_H

#define TOKEN_ENDPOINT "/tokens"
#define PUMP_ENDPOINT "/pump"
#define LIGHTS_ENDPOINT "/lights"

/***
 * Function that is called when a request comes in on the corresponding endpoint. The incoming data is
 * parsed using cJSON and stored on the NVS.
 */

esp_err_t handle_json_tokens(char *jsonStr);

/***
 * Function that is called when a request comes in on the corresponding endpoint. The incoming data is
 * parsed using cJSON and depending on the provided status, the pump is turned on or off.
 */
esp_err_t handle_json_pump(char *jsonStr);

/***
 * Function that is called when a request comes in on the corresponding endpoint. The incoming data is
 * parsed using cJSON and depending on the provided percentage, the corresponding PWM value is calculated
 * and the brightness of the LED's are changed.
 */
esp_err_t handle_json_lights(char *jsonStr);

/***
 * This function is called when a request comes in, and determines which json parsing function should be
 * called depending on the endpoint. In order to prevent (mostly) duplicate code a string comparison is made
 * to determine the endpoint on which the request came in.
 */
esp_err_t handle_tokens_req(httpd_req_t *req);

/***
 * A generalised function to easily created endpoints on which the server should listen.
 */
esp_err_t init_endpoint(const char *endpointPath, const httpd_handle_t *httpdHandle);

/***
* Starts the webserver and initialises the endpoints.
 */
esp_err_t start_webserver(httpd_handle_t *httpdHandle);

/***
 * Obviously this function stops the HTTP web server.
 */
void stop_webserver(httpd_handle_t *httpdHandle);

#endif //EE5_ESP32_HTTP_SERVER_H
