#ifndef EE5_ESP32_HTTP_CLIENT_H
#define EE5_ESP32_HTTP_CLIENT_H

#include <sys/cdefs.h>
#include "esp_http_client.h"
#include "cJSON.h"
#include "error_macros.h"
#include "json_keys.h"

/***
 * This header file contains the functions (to be implemented) for all http requests to the API.
 */

struct RequestBuffer {
    u_int16_t size;
    char *buffer;
};

/***
 * The event handler which takes cares of certain events such as HTTP_EVENT_ON_DATA, HTTP_EVENT_ON_FINISHED
 * and HTTP_EVENT_ON_ERROR. When data is received some checks are done. Firstly, it is checked if the buffer exists,
 * else we create a new buffer to store the data. Next, it is checked if the buffer is big enough to store the incoming
 * data, else we create a new buffer which is bigger. Finally, if no errors occurred, we transfer the incoming data to the
 * buffer. If the HTTP_EVENT_ON_FINISH occurs, the variable "length_transferred" (which keeps track of the amount of data we already
 * received) is reset to 0. Last but not least, if HTTP_EVENT_ERROR occurs the http_client is closed
 */
esp_err_t http_client_event_handler(esp_http_client_event_t *evt);

/***
 * Here a POST request is prepared. The host, path to api, SSL, buffer and event handler are configured and stored
 * in the provided struct esp_http_client_config_t
 */
void init_post_req_conf(esp_http_client_config_t *httpClientConfig, const char *apiPath, struct RequestBuffer *requestBuffer);

/***
 * In exec_post_req, the actual POST request is performed and response are handled. E.g. if the access token is invalid.
 * At the end of the function the http request is cleaned up.
 */
void exec_post_req(const esp_http_client_config_t *httpClientConfig, const char *accessToken, const char *data, void (*funcToExec)(char *jsonStr));

/***
 * Handle the incoming data/respons (which is stored in the buffer). This repons should be JSON format and is then parsed
 * using the cJSON library. Registering a new plant to the API returns an ID, which is stored onto the non-volatile flash
 * memory. If no ID was stored on the NVS, the main task is blocked. This function then unblocks the main task.
 */
void handle_json_new_plant(char *jsonStr);

/***
 * When the ESP32 POST it's sensor measurements to the API, the API will returns thresholds that were changed since the last
 * POST request. These thresholds are then parsed using the cJSON library and stored on the NVS.
 */

void handle_json_post_measurement(char *jsonStr);

/***
 * If the ESP32 is booted for the first time it doesn't have an ID or the necessary access tokens to access the API.
 * Therefore it boots an HTTP server in order to receive the tokens from the android app. These tokens are then stored on the NVS. In this function,
 * the access tokens are read from the NVS and used to register the plant to the API.
 */
void register_plant();

/***
 * This functions is being run inside a task and keeps running "indefinitely". Firstly, it reads the ID and access tokens from the NVS
 * Next,it reads data from the buffer and performs and HTTP POST request to the API. If no sensor data is available in the buffer or it already
 * has read all the data, this tasks is blocked and waits on a notification from adc_perform.
 */
_Noreturn void post_measurements(void *iTaskData);

#endif //EE5_ESP32_HTTP_CLIENT_H
