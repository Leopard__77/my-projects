#include <sys/cdefs.h>
#include <esp32/pm.h>
#include "WiFi_prov.c"
#include "http_server.h"
#include "mDNS.c"
#include "sensors_adc.h"
#include "storage_handler.h"
#include "main.h"

TaskHandle_t mainTask;
const char *TAG_MAIN = "Main Task";
static QueueHandle_t gpio_evt_queue = NULL;

void run_measurement(void *args) {
    //unblock perform_adc to perform periodic measurements
    xTaskNotifyGive(*(TaskHandle_t *) args);
}

static void IRAM_ATTR gpio_isr_handler(void* arg)
{
    uint32_t gpio_num = (uint32_t) arg;
    xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

_Noreturn void gpio_task(){
    uint32_t io_num;
    for(;;) {
        if(xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
            ESP_LOGW(TAG_MAIN, "WIPING ALL DATA!");
            wifi_prov_mgr_reset_provisioning();
            erase_id();
            erase_tokens();
            esp_restart();
        }
    }
}

void factory_reset(){
    gpio_config_t gpioConfig = {
            .intr_type = GPIO_INTR_NEGEDGE,
            .mode = GPIO_MODE_INPUT,
            .pin_bit_mask = (1ULL<<GPIO_NUM_0)
    };
    gpio_config(&gpioConfig);
    gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));
    xTaskCreate(gpio_task, "gpio_task", 2048, NULL, 10, NULL);
    gpio_install_isr_service(0);
    gpio_isr_handler_add(0, gpio_isr_handler, (void*) 0);

}

void app_main(void) {
   // esp_log_level_set("*", ESP_LOG_WARN); //suppress informational messages
    mainTask = xTaskGetCurrentTaskHandle();
    static struct MeasurementHandlers measurementHandlers;
    factory_reset();
    setup_Wifi_prov(mainTask, &measurementHandlers);
    /* Wait for Wi-Fi connection */
    xTaskNotifyWait(0x00, ULONG_MAX, NULL, portMAX_DELAY);
    httpd_handle_t httpdHandle;
    esp_err_t error = start_webserver(&httpdHandle);
    u_int32_t id = 0;
    char accessToken[128];
    if (error == ESP_OK) {
        if (nvs_get_accessToken(accessToken) != ESP_OK || nvs_get_id(&id) != ESP_OK) {
            //if no tokens or no ID was found -> wait until tokens are received and ESP received an ID
            xTaskNotifyWait(0x00, ULONG_MAX, NULL, portMAX_DELAY);
        }
        updateHostname();
        static BufferData_t buffer[12];//static because app_main exits eventually, other option is to pass the struct to every task (not as a pointer) but this is redundant
        initBufferLocks(&measurementHandlers.bufferHandles, buffer, 12, &measurementHandlers.perform_adc_task_handle);
        xTaskCreate(perform_adc, "perform_adc", 3000, &measurementHandlers, 3, &measurementHandlers.perform_adc_task_handle);
        xTaskCreate(perform_feedback, "perform_feedback", 3000, &measurementHandlers, 3, &measurementHandlers.perform_feedback_task_handle);
        xTaskCreate(post_measurements, "post_data", 5000, &measurementHandlers, 3, &measurementHandlers.post_measurements_task_handle);

        const esp_timer_create_args_t timerCreateArgs = {
                .callback = &run_measurement,
                .name = "Perform ADC",
                .arg = &measurementHandlers.perform_adc_task_handle

        };
        ESP_ERROR_CHECK(esp_timer_create(&timerCreateArgs, &measurementHandlers.timer_handle));
        esp_timer_start_periodic(measurementHandlers.timer_handle, CONFIG_PERIOD_MEASUREMENT);
        ESP_LOGI(TAG_MAIN, "Starting measurements ... ");
    } else {
        ESP_LOGW(TAG_MAIN, "Couldn't start web server");
        stop_webserver(&httpdHandle);
        if (nvs_get_accessToken(accessToken) == ESP_ERR_NVS_NOT_FOUND) {
            //No access token was found & esp can't receive new access tokens -> reset
            ESP_LOGE(TAG_MAIN, "ESP can't receive access tokens, restarting ...");
        }
        esp_restart();

    }
}