
#include "buffer.h"

void initBufferLocks(BufferHandles_t * bufferHandles, BufferData_t * newBuffer, u_int16_t buffSize, TaskHandle_t * taskToNotify){
    bufferHandles->semaphoreHandle = xSemaphoreCreateBinary();
    bufferHandles->mutexHandle = xSemaphoreCreateMutex();
    bufferHandles->nrOfReaders = 0;
    bufferHandles->taskToNotify = taskToNotify;
    bufferHandles->bufferFull = false;
    u_int16_t index = 0;
    bufferHandles->buffer = newBuffer;
    while(index < buffSize){
        //flag all element as "delete" (read by both readers)
        bufferHandles->buffer[index].readByReader1 = true;
        bufferHandles->buffer[index].readByReader2 = true;
        index++;
    }
    bufferHandles->bufferSize = buffSize;
    xSemaphoreGive(bufferHandles->semaphoreHandle);
    xSemaphoreGive(bufferHandles->mutexHandle);
}

void takeBufferReadLock(BufferHandles_t * bufferHandles){
    xSemaphoreTake(bufferHandles->mutexHandle, portMAX_DELAY);
    if(bufferHandles->nrOfReaders == 0){
        xSemaphoreTake(bufferHandles->semaphoreHandle, portMAX_DELAY);
    }
    bufferHandles->nrOfReaders++;
    xSemaphoreGive(bufferHandles->mutexHandle);
}

void giveBufferReadLock(BufferHandles_t * bufferHandles){
    xSemaphoreTake(bufferHandles->mutexHandle, portMAX_DELAY);
    bufferHandles->nrOfReaders--;
    if(bufferHandles->nrOfReaders == 0){
        xSemaphoreGive(bufferHandles->semaphoreHandle);
    }
    xSemaphoreGive(bufferHandles->mutexHandle);
}

void takeBufferUpdateLock(BufferHandles_t * bufferHandles){
    xSemaphoreTake(bufferHandles->mutexHandle, portMAX_DELAY);
}

void giveBufferUpdateLock(BufferHandles_t * bufferHandles){
    xSemaphoreGive(bufferHandles->mutexHandle);
}

void takeBufferWriteLock(BufferHandles_t * bufferHandles, TaskHandle_t * taskHandle){
    vTaskPrioritySet(*taskHandle, 5);
    xSemaphoreTake(bufferHandles->semaphoreHandle, portMAX_DELAY);
}

void giveBufferWriteLock(BufferHandles_t * bufferHandles, TaskHandle_t * taskHandle){
    vTaskPrioritySet(*taskHandle, 3);
    xSemaphoreGive(bufferHandles->semaphoreHandle);
}

//mutexes are needed because 2 reading tasks update variables
esp_err_t readFromBuffer(BufferHandles_t * bufferHandles,  SensorData_t * data, u_int8_t readingTask){
    takeBufferReadLock(bufferHandles);
//    ESP_LOGI("BUFFER", "Reader acquired lock");
    bool found = false;
    u_int32_t index = 0;
    BufferData_t * currentBufferData = NULL;
    while(found == false && index < bufferHandles->bufferSize){
        currentBufferData = &bufferHandles->buffer[index];
        if( (readingTask == READER_1 && currentBufferData->readByReader1 == false) ^ (readingTask == READER_2 && currentBufferData->readByReader2 == false)){
            *data = currentBufferData->sensorData;
            found = true;
            if(readingTask == READER_1) {
//                ESP_LOGI("BUFFER", "Reader1 reads data");
                takeBufferUpdateLock(bufferHandles);
//                ESP_LOGI("BUFFER", "Reader1 acquired second lock");
                currentBufferData->readByReader1 = true;
//                ESP_LOGI("BUFFER", "Reader1 unlocks second lock");
                giveBufferUpdateLock(bufferHandles);
            }
            else {
//                ESP_LOGI("BUFFER", "Reader2 reads data");
                takeBufferUpdateLock(bufferHandles);
//                ESP_LOGI("BUFFER", "Reader2 acquired second lock");
                currentBufferData->readByReader2 = true;
//                ESP_LOGI("BUFFER", "Reader2 unlocks second lock");
                giveBufferUpdateLock(bufferHandles);
            }

            //if the buffer is full then the writer is blocking, if both reader have read some data -> alert the writer and unlock it
            if(bufferHandles->bufferFull == true && currentBufferData->readByReader1 == true && currentBufferData->readByReader2 == true){
                xTaskNotify(*bufferHandles->taskToNotify,0,eNoAction);
                takeBufferUpdateLock(bufferHandles);
                bufferHandles->bufferFull = false;
                giveBufferUpdateLock(bufferHandles);
            }
        }
        else{
            index++;
        }
    }
//    ESP_LOGI("BUFFER", "Reader unlocks");
    giveBufferReadLock(bufferHandles);
    if(found == false){
        return ESP_FAIL;
    }
    else{
        return ESP_OK;
    }

}

//no mutexes are needed because when there is only one writer when the writing tasks holds the semaphore
esp_err_t writeToBuffer(BufferHandles_t * bufferHandles, SensorData_t * data, TaskHandle_t * taskHandle){
    takeBufferWriteLock(bufferHandles, taskHandle);
//    ESP_LOGI("BUFFER", "writer acquired lock");
    bool found = false;
    u_int32_t index = 0;
    BufferData_t * currentBufferData = NULL;
    while(found == false && index < bufferHandles->bufferSize){
        currentBufferData = &bufferHandles->buffer[index];
        if( currentBufferData->readByReader1 == true && currentBufferData->readByReader2 == true){
//            ESP_LOGI("BUFFER", "Writer inserting data ..");
            currentBufferData->sensorData = *data;
            currentBufferData->readByReader1 = false;
            currentBufferData->readByReader2 = false;
            found = true;
        }
        else{
            index++;
        }
    }

    if(found == false){
        ESP_LOGE("BUFFER", "Buffer full");
        bufferHandles->bufferFull = true;
        giveBufferWriteLock(bufferHandles, taskHandle);
        return ESP_FAIL;
    }
    else{
        giveBufferWriteLock(bufferHandles, taskHandle);
        return ESP_OK;
    }
//    ESP_LOGI("BUFFER", "Writer unlocks");

}