//
// Created by atlas on 3/17/22.
//

#ifndef EE5_ESP32_ERROR_MACROS_H
#define EE5_ESP32_ERROR_MACROS_H
#include <esp_log.h>
#include<esp_log.h>
#define ERR_JSON_PARSE(TAG) 									\
		do {												\
				ESP_LOGE(TAG, "JSON parsing failed!"); 	                \
		} while(0)

#endif //EE5_ESP32_ERROR_MACROS_H
