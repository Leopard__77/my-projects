#include <driver/gpio.h>
#include <driver/ledc.h>
#include "http_server.h"
#include "storage_handler.h"
#include "sensors_adc.h"
#include <soc/adc_channel.h>
static const char * HTTP_SERVER_TAG = "HTTP_SERVER";

esp_err_t handle_json_tokens(char * jsonStr){
    cJSON *json = cJSON_Parse(jsonStr);
    if (json != NULL) {
        cJSON *accessTokenJson = cJSON_GetObjectItem(json, ACCESSTOKEN_KEY);
        cJSON *refreshTokenJson = cJSON_GetObjectItem(json, REFRESHTOKEN_KEY);
        if (cJSON_IsString(accessTokenJson) && cJSON_IsString(refreshTokenJson) &&
            accessTokenJson->valuestring != NULL && refreshTokenJson != NULL) {
            return nvs_store_tokens(cJSON_GetObjectItem(json, REFRESHTOKEN_KEY)->valuestring, cJSON_GetObjectItem(json, ACCESSTOKEN_KEY)->valuestring);
        } else {
            ERR_JSON_PARSE(HTTP_SERVER_TAG);
            return ESP_FAIL;
        }
    } else {
        ERR_JSON_PARSE(HTTP_SERVER_TAG);
        return ESP_FAIL;
    }
}

esp_err_t handle_json_pump(char * jsonStr){
    cJSON * json = cJSON_Parse(jsonStr);
    if(json!= NULL){
        cJSON * status = cJSON_GetObjectItem(json, PUMP_STATUS_KEY);
        if(cJSON_IsNumber(status)){
            u_int32_t waterlvl = adc1_get_raw(ADC1_GPIO34_CHANNEL);
            u_int32_t value = 1;
            u_int32_t threshold = 0;
            feedback_moisture(&value, &threshold, &waterlvl);
            return ESP_OK;
        }
        else {
            ERR_JSON_PARSE(HTTP_SERVER_TAG);
            return ESP_FAIL;
        }
    } else {
        ERR_JSON_PARSE(HTTP_SERVER_TAG);
        return ESP_FAIL;
    }
}

esp_err_t handle_json_lights(char * jsonStr){
    cJSON * json = cJSON_Parse(jsonStr);
    if(json!= NULL){
        cJSON * lvl = cJSON_GetObjectItem(json, LIGHTS_STATUS_KEY);
        if(cJSON_IsNumber(lvl)){
            ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, 32-lvl->valueint/100*32);//32-x because 32 is off
            ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);
            return ESP_OK;
        }
        else {
            ERR_JSON_PARSE(HTTP_SERVER_TAG);
            return ESP_FAIL;
        }
    } else {
        ERR_JSON_PARSE(HTTP_SERVER_TAG);
        return ESP_FAIL;
    }
}

esp_err_t handle_tokens_req(httpd_req_t * req){
    static u_int8_t retries = 0;
    char buffer [128];
    int bytes_received = httpd_req_recv(req, buffer, req->content_len);
    if(bytes_received <= 0){ //connection closed unexpectedly
        ESP_LOGI(HTTP_SERVER_TAG, "Connection closed unexpectedly");
        if(bytes_received == HTTPD_SOCK_ERR_TIMEOUT){
            retries++;
            bytes_received = httpd_req_recv(req, buffer, req->content_len);
            if(bytes_received == HTTPD_SOCK_ERR_TIMEOUT && retries == 2){ //after 2 retries stop
                retries = 0;
                httpd_resp_send_408(req);
            }
        }
        if(bytes_received == HTTPD_SOCK_ERR_INVALID){
            httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Invalid arguments");
        }
        return ESP_FAIL;
    }
    else{
        esp_err_t error = ESP_FAIL;
        bool token_recv = false; //just to avoid a double string comparison (performance penalty) because we want to first sent a response then execute register_plant() if a request came in on "tokens"
        if(strcmp(req->uri, TOKEN_ENDPOINT) == 0){
            error = handle_json_tokens(buffer);
            token_recv = true;
        }
        else if(strcmp(req->uri, PUMP_ENDPOINT) == 0){
            error = handle_json_pump(buffer);
            ESP_LOG_BUFFER_CHAR(HTTP_SERVER_TAG, buffer, strlen(buffer));
        }
        else if(strcmp(req->uri, LIGHTS_ENDPOINT) == 0){
            error = handle_json_lights(buffer);
            ESP_LOG_BUFFER_CHAR(HTTP_SERVER_TAG, buffer, strlen(buffer));
        }
        char *response = NULL;
        httpd_resp_set_type(req, "application/json");

        if(error == ESP_OK){
            httpd_resp_set_status(req, HTTPD_200);
            response = "{\"status\":\"OK\"}";
            httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
            u_int32_t id = 0;
            if(token_recv && nvs_get_id(&id) == ESP_FAIL){ //if tokens were received but there is no ID yet -> register the plant
                ESP_LOGI(HTTP_SERVER_TAG, "Registering new plant");
                token_recv = false;
                register_plant();
            }
        }
        else{
            response = "{\"status\":\"Invalid arguments! Check keys & format\"}";
            httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, response);
        }
       return ESP_OK;
    }
}

esp_err_t init_endpoint(const char * endpointPath, const httpd_handle_t * httpdHandle){
    httpd_uri_t httpdUriPost = {
            .uri = endpointPath,
            .method = HTTP_POST,
            .handler = handle_tokens_req,
            .user_ctx = NULL
    };
    return httpd_register_uri_handler(*httpdHandle, &httpdUriPost);
}

esp_err_t start_webserver(httpd_handle_t * httpdHandle){
    ESP_LOGI(HTTP_SERVER_TAG, "Starting webserver ...");
    httpd_config_t httpdConfig = HTTPD_DEFAULT_CONFIG();
    if(httpd_start(httpdHandle,&httpdConfig) == ESP_OK){
        ESP_ERROR_CHECK(init_endpoint(TOKEN_ENDPOINT, httpdHandle)); //if token endpoint can't be setup esp can't function properly other endpoints are not as vital -> don't abort
        ESP_ERROR_CHECK_WITHOUT_ABORT(init_endpoint(PUMP_ENDPOINT, httpdHandle));
        ESP_ERROR_CHECK_WITHOUT_ABORT(init_endpoint(LIGHTS_ENDPOINT, httpdHandle));
        return ESP_OK;
    }
    else{
        return ESP_FAIL;
    }
}

void stop_webserver(httpd_handle_t * httpdHandle){
    if(httpdHandle != NULL){
        httpd_stop(httpdHandle);
    }
}
