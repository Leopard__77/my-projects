#include <sys/cdefs.h>
#include <soc/adc_channel.h>
#include <lwip/apps/sntp.h>
#include <esp_sntp.h>
#include <math.h>
#include "sensors_adc.h"
#include "driver/ledc.h"
#include "main.h"
#include "storage_handler.h"
#include "driver/mcpwm.h"

static const char *TAG_2 = "ADC SINGLE";
static esp_timer_handle_t pump_timer_handle;


void init_lights() {
    ledc_timer_config_t timerConfig = {
            .speed_mode = LEDC_LOW_SPEED_MODE,
            .timer_num = LEDC_TIMER_0,
            .duty_resolution = LEDC_TIMER_5_BIT,
            .freq_hz = 1000,
            .clk_cfg = LEDC_AUTO_CLK
    };
    ESP_ERROR_CHECK(ledc_timer_config(&timerConfig));
    ledc_channel_config_t ledcChannelConfig = {
            .speed_mode = LEDC_LOW_SPEED_MODE,
            .channel = LEDC_CHANNEL_0,
            .timer_sel = LEDC_TIMER_0,
            .intr_type = LEDC_INTR_DISABLE,
            .gpio_num = CONFIG_PWM_PIN_LIGHTS, //TODO DONT HARDCODE
            .duty = 32, // max 32 -> off (inverse)
            .hpoint = 0
    };
    ESP_ERROR_CHECK(ledc_channel_config(&ledcChannelConfig));
}

void init_mcpwm_signal(mcpwm_io_signals_t io_signal, mcpwm_timer_t timer, int gpioPin, u_int32_t frequency, float initialDutyCycle) {
    ESP_ERROR_CHECK(mcpwm_gpio_init(MCPWM_UNIT_0, io_signal, gpioPin));
    mcpwm_config_t pwm_config = {
            .frequency = frequency,
            .cmpr_a = 0,
            .counter_mode=MCPWM_UP_COUNTER,
            .duty_mode=MCPWM_DUTY_MODE_0
    };
    ESP_ERROR_CHECK(mcpwm_init(MCPWM_UNIT_0, timer, &pwm_config));
    ESP_ERROR_CHECK(mcpwm_set_duty(MCPWM_UNIT_0, timer, MCPWM_GEN_A, initialDutyCycle)); //tussen 10 en 27%
}

esp_err_t setStatusSensors(int newLvl) {
    if (gpio_set_level(GPIO_NUM_9, newLvl) == ESP_OK
        && gpio_set_level(GPIO_NUM_18, newLvl) == ESP_OK
        && gpio_set_level(GPIO_NUM_4, newLvl) == ESP_OK
        && gpio_set_level(GPIO_NUM_25, newLvl) == ESP_OK) {
        return ESP_OK;
    }
    return ESP_FAIL;
}

_Noreturn void perform_adc(void *iTaskData) {
    struct MeasurementHandlers *measurementHandlers = (struct MeasurementHandlers *) iTaskData;
    //configure GPIO pins to enable sensors
    ESP_ERROR_CHECK(gpio_set_direction(GPIO_NUM_18, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_direction(GPIO_NUM_22, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_direction(GPIO_NUM_9, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_direction(GPIO_NUM_4, GPIO_MODE_OUTPUT));
    ESP_ERROR_CHECK(gpio_set_direction(GPIO_NUM_25, GPIO_MODE_OUTPUT));

    //turn all sensors off
    setStatusSensors(0);

    //configure GPIO pins to perform ADC
    ESP_ERROR_CHECK(adc1_config_width(ADC_WIDTH_BIT_12));
    ESP_ERROR_CHECK(adc1_config_channel_atten(ADC1_GPIO36_CHANNEL, ADC_ATTEN_DB_11));
    ESP_ERROR_CHECK(adc1_config_channel_atten(ADC1_GPIO37_CHANNEL, ADC_ATTEN_DB_11));
    ESP_ERROR_CHECK(adc1_config_channel_atten(ADC1_GPIO34_CHANNEL, ADC_ATTEN_DB_11));
    ESP_ERROR_CHECK(adc1_config_channel_atten(ADC1_GPIO35_CHANNEL, ADC_ATTEN_DB_11));
    ESP_ERROR_CHECK(adc1_config_channel_atten(ADC1_GPIO32_CHANNEL, ADC_ATTEN_DB_11));

    SensorData_t sensorData;
    esp_err_t espErr;
    for (;;) {
        xTaskNotifyWait(0x00, ULONG_MAX, NULL, portMAX_DELAY);
        esp_timer_stop(measurementHandlers->timer_handle);
        if (setStatusSensors(1) == ESP_OK) {
            sensorData.light = adc1_get_raw(ADC1_GPIO36_CHANNEL);
            sensorData.temperature = adc1_get_raw(ADC1_GPIO37_CHANNEL);
            sensorData.waterlvl = adc1_get_raw(ADC1_GPIO34_CHANNEL);
            sensorData.moisture = adc1_get_raw(ADC1_GPIO35_CHANNEL);
            sensorData.light_led = adc1_get_raw(ADC1_GPIO32_CHANNEL);
            ESP_LOGI(TAG_2, "Temp: %d - Light: %d - Light_led: %d - Moisture: %d - waterlvl: %d", sensorData.temperature, sensorData.light, sensorData.light_led, sensorData.moisture, sensorData.waterlvl);

            espErr = writeToBuffer(&measurementHandlers->bufferHandles, &sensorData, &measurementHandlers->perform_adc_task_handle);
            if (espErr == ESP_OK) {
                xTaskNotify(measurementHandlers->post_measurements_task_handle, 0, eNoAction);
                xTaskNotify(measurementHandlers->perform_feedback_task_handle, 0, eNoAction);
            } else {
                //buffer is full aka readers not fast enough -> wait for the readers
                esp_timer_stop(measurementHandlers->timer_handle); //stop de timer
                xTaskNotifyWait(0x00, ULONG_MAX, NULL, portMAX_DELAY);
                ESP_LOGI("BUFFER", "Unblocking ...");
                //data can be inserted again -> start timer again + don't check if buffer full, notification only arrives if both readers have read some data
                writeToBuffer(&measurementHandlers->bufferHandles, &sensorData, &measurementHandlers->perform_adc_task_handle);
                esp_timer_start_periodic(measurementHandlers->timer_handle, CONFIG_PERIOD_MEASUREMENT);
            }
            //setStatusSensors(0);
        } else {
            ESP_LOGE(TAG_2, "Couldn't enable all sensors!");
        }
        esp_timer_start_periodic(measurementHandlers->timer_handle, CONFIG_PERIOD_MEASUREMENT);

    }
}


void feedback_moisture(const u_int32_t *moistureValue, const u_int32_t *moistureThreshold, const u_int32_t *waterlvl) {
    if (*moistureValue > *moistureThreshold && *waterlvl > (u_int32_t) 500) {
        ESP_ERROR_CHECK(mcpwm_start(MCPWM_UNIT_0, MCPWM_TIMER_2));
        ESP_ERROR_CHECK(mcpwm_set_signal_high(MCPWM_UNIT_0, MCPWM_TIMER_2, MCPWM_GEN_A));
        ESP_ERROR_CHECK(mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_2, MCPWM_GEN_A, 50));
        esp_timer_start_once(pump_timer_handle, 4000000);
        ESP_LOGW(TAG_2, "Pump ON");
    }
}

void stop_pump() {
    ESP_ERROR_CHECK(mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_2, MCPWM_GEN_A, 0));
    ESP_ERROR_CHECK(mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_2, MCPWM_GEN_A));
    ESP_ERROR_CHECK(mcpwm_stop(MCPWM_UNIT_0, MCPWM_TIMER_2));
    //ESP_ERROR_CHECK(mcpwm_set_signal_low(MCPWM_UNIT_0, MCPWM_TIMER_2, MCPWM_GEN_A));

    ESP_LOGW(TAG_2, "PUMP OFF");
}

void feedback_light(const u_int32_t *lightValue, const u_int32_t *lightThreshold) {
    u_int32_t currentDuty = ledc_get_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);
    if (*lightValue > *lightThreshold && (currentDuty - 1) >= 0) {
        ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, --currentDuty);//brighter
        ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);
    } else if ((currentDuty + 1) <= 32) {
        ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, ++currentDuty);//dimmer
        ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);
    }
}

void feedback_temperature(const u_int32_t *tempValue, const u_int32_t *tempThreshold) {
    if (*tempValue < *tempThreshold) {
        ESP_ERROR_CHECK(mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_GEN_A, 27));
        ESP_ERROR_CHECK(mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_GEN_A, 50));
    } else {
        ESP_ERROR_CHECK(mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_GEN_A, 10));
        ESP_ERROR_CHECK(mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_GEN_A, 0));
    }
}

static bool IRAM_ATTR timer_group_isr_callback(gptimer_handle_t timer, const gptimer_alarm_event_data_t *edata, void *user_data) {
    BaseType_t high_task_awoken = pdFALSE;
    QueueHandle_t queue = (QueueHandle_t) user_data;
    // stop timer immediately
    gptimer_stop(timer);
    // Retrieve count value and send to queue
    static bool recv; //default false, if false the timer is counting until the lights can be turned on; if true the timer is counting the duration the lights can be on
    recv = !recv;
    xQueueSendFromISR(queue, &recv, &high_task_awoken);
    // return whether we need to yield at the end of ISR
    gptimer_set_raw_count(timer, 0);
    return (high_task_awoken == pdTRUE);
}

void updateTimer(gptimer_handle_t * gptimer, struct tm *currentTimeHM, struct tm *thresholdTimeHM, const bool *timerValue, const u_int8_t * duration) {
    ESP_ERROR_CHECK(gptimer_stop(*gptimer));
    gptimer_alarm_config_t alarmConfig = {
            .reload_count = 0
    };
    //initialize counters;
    int diff = 0;
    u_int64_t counter = 0;
    if (*timerValue == false){//initialize timer to alarm when lights should go on
        ESP_LOGI("TIMER", "Setting threshold");
        diff = (thresholdTimeHM->tm_hour * 3600 + thresholdTimeHM->tm_min * 60) - (currentTimeHM->tm_hour * 3600 + currentTimeHM->tm_min * 60);
        if (diff <= 0) {
            counter = (u_int64_t) 24 * 60 * 60 + diff; // if difference is negative, meaning that the threshold time already passed -> add day

        } else {
            counter = (u_int64_t) diff;
        }
        ESP_LOGI("Difference", ": %llu", counter);

    }
    else{//initialze timer to alarm when the lights should go off after x amount of time
        ESP_LOGI("TIMER", "Setting duration");
        counter = (*duration)*3600;
        ESP_LOGI("Counter", ": %llu", counter);
    }
    counter = counter * 10000;
    alarmConfig.alarm_count = counter;
    ESP_ERROR_CHECK(gptimer_set_alarm_action(*gptimer, &alarmConfig));
    ESP_ERROR_CHECK(gptimer_start(*gptimer));
}

_Noreturn void timerTask(void *taskData) {

    sntp_setservername(0, "pool.ntp.org");
    sntp_set_sync_mode(SNTP_SYNC_MODE_IMMED);
    sntp_init();
    time_t now = 0;
    int retry = 0;
    const int retry_count = 30;
    u_int8_t sntpStatus = sntp_get_sync_status();
    while (sntpStatus == SNTP_SYNC_STATUS_RESET && ++retry < retry_count) {
        ESP_LOGI("Main", "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        sntpStatus = sntp_get_sync_status();
    }
    //DEBUG
    struct tm test;
    time(&now);
    test = *gmtime(&now);
    ESP_LOGI("TIME", "Current ESP time is %s", asctime(&test));
    if (sntpStatus == SNTP_SYNC_STATUS_COMPLETED) {

        QueueHandle_t *queueHandle = (QueueHandle_t *) taskData;
        QueueHandle_t queue = xQueueCreate(10, sizeof(bool));
        gptimer_handle_t gptimer = NULL;
        gptimer_config_t timerConfig = {
                .direction = GPTIMER_COUNT_UP,
                .resolution_hz = 10000,
        };
        ESP_ERROR_CHECK(gptimer_new_timer(&timerConfig, &gptimer));
        gptimer_event_callbacks_t cbs = {
                .on_alarm = timer_group_isr_callback,
        };
        ESP_ERROR_CHECK(gptimer_register_event_callbacks(gptimer, &cbs, queue));

        struct tm currentTime = {0};
        struct tm currentTimeHM = {0}; // needed to compare Hours and Minutes, not dates
        u_int8_t lightStartHours, lightStartMinutes, lightDuration;
        esp_err_t nvsErr;
        struct tm thresholdHM = {0};

        bool timerValue = false;
        bool prevTimerValue = false;
        u_int8_t currentDuration = 0;
        while (1) {

            nvsErr = nvs_get_time_thresholds(&lightStartHours, &lightStartMinutes, &lightDuration);
            if (nvsErr == ESP_OK) {
                if(thresholdHM.tm_hour != lightStartHours || thresholdHM.tm_min != lightStartMinutes || prevTimerValue != timerValue || lightDuration != currentDuration){ // update the timer only when thresholds have changes
                    ESP_LOGI("TIMER", "Thresholds have changed");
                    time(&now);
                    currentTime = *gmtime(&now);

                    currentTimeHM.tm_hour = currentTime.tm_hour;
                    currentTimeHM.tm_min = currentTime.tm_min;

                    thresholdHM.tm_hour = lightStartHours;
                    thresholdHM.tm_min = lightStartMinutes;
                    ESP_LOGI("Thresholds", "Hour: %d - Min: %d", thresholdHM.tm_hour, thresholdHM.tm_min);
                    ESP_LOGI("Current", "Hour: %d - Min: %d", currentTimeHM.tm_hour, currentTimeHM.tm_min);

                    updateTimer(&gptimer, &currentTimeHM, &thresholdHM, &timerValue, &lightDuration);
                    prevTimerValue = timerValue;
                    currentDuration = lightDuration;
                }

            } else {
                ESP_LOGE(TAG_2, "No thresholds to initialize timers");
                vTaskDelay(10000 / portTICK_PERIOD_MS);
            }
            if (xQueueReceive(queue, &timerValue, pdMS_TO_TICKS(5000))) {
                ESP_LOGI("TIMER", "Sending to queue");
                xQueueSend(*queueHandle, &timerValue, 0);
            }

        }
    } else {
        ESP_LOGE(TAG_2, "Can't sync time with servers, rebooting ...");
        esp_restart();
    }
}

_Noreturn void perform_feedback(void *iTaskData) {
    struct MeasurementHandlers *measurementHandlers = (struct MeasurementHandlers *) iTaskData;
    SensorData_t sensorData;
    esp_err_t espErr;
    u_int32_t lightThreshold;
    u_int32_t moistureThreshold;
    u_int32_t temperatureThreshold;
    //to disable the pump after 1s
    const esp_timer_create_args_t timerCreateArgs = {
            .callback = &stop_pump,
            .name = "Stop pump",
    };
    ESP_ERROR_CHECK(esp_timer_create(&timerCreateArgs, &pump_timer_handle));
    init_lights();
    //init pump
    init_mcpwm_signal(MCPWM2A, MCPWM_TIMER_2, CONFIG_PWM_PIN_PUMP, 1000, 0);
    //init fan
    init_mcpwm_signal(MCPWM1A, MCPWM_TIMER_1, CONFIG_PWM_PIN_FAN, 1000, 0);
    //init servo
    init_mcpwm_signal(MCPWM0A, MCPWM_TIMER_0, CONFIG_PWM_PIN_SERVO, 125, 10);


    QueueHandle_t queue = xQueueCreate(10, sizeof(bool)); //create to listen for events from timer task
    xTaskCreate(timerTask, "timerTask", 4000, (void *) &queue, 3, NULL);
    bool timerEvent = false;
    for (;;) {
        espErr = readFromBuffer(&measurementHandlers->bufferHandles, &sensorData, READER_1);
        if (espErr == ESP_OK) {
            esp_err_t errNVS = nvs_get_thresholds(&lightThreshold, &moistureThreshold, &temperatureThreshold);
            if (errNVS == ESP_OK) {
                xQueueReceive(queue, &timerEvent, 0);
                if (timerEvent == true) { //Timer threshold/interval is reached, lights can be controlled now
                    ESP_LOGI("FEEDBACK", "LIGHTS MAY PERFORM FEEDBACK");
                    feedback_light(&sensorData.light, &lightThreshold);
                }
                else{
                    ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, 32);
                    ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);
                };
                feedback_moisture(&sensorData.moisture, &moistureThreshold, &sensorData.waterlvl);
                feedback_temperature(&sensorData.temperature, &temperatureThreshold);

            }
        } else {
            xTaskNotifyWait(0x00, ULONG_MAX, NULL, portMAX_DELAY);
        }
    }
}


