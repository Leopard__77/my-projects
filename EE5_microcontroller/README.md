# EE5 microcontroller

This is the microcontroller git of the flower pot. It is to be paired with the app. The repo can be found here: https://gitlab.groept.be/antoineleopoldj.bauer/ee5-app

# Water level sensor

100% -> +- 2600
75% -> 2000
50% -> 1100
25% -> 800-900
0% -> 750

# Pin layout

| Pin | Function               |
|-----|------------------------|
| 2   | PWM LED's on LED board |
| 4   | Enable water level     |
| 9   | Enable thermistor      |
| 18  | Enable LDR             |
| 19  | PWM servo motor        |
| 22  | PWM pump               |
| 23  | PWM Fan                |
| 25  | enable LDR LED board   |
| 32  | ADC LDR LED board      |
| 35  | ADC moisture           |
| 36  | ADC LDR                | 
| 37  | ADC thermistor         |
| 34  | ADC water level        |







# HTTP Endpoints
```[IP]/tokens```
Required body:
```
    {
        "AccessToken":"XXXXXX",
        "RefreshToken":"XXXXXX"
    }
```

```[IP]/pump``` Set status to 1 (pump ON) or 0 (pump OFF)
```
    {
        "Status":1,
    }
```

```[IP]/lights``` Set level to a percentage e.g. 50% light is defined like this:
```
    {
        "Level":50,
    }
```
## How to add ESP to clion

1. Follow instructions on https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html#introduction. Make sure to get "Hello World" working.
2. Open the project in clion
3. Go to "File->Settings->Build,Execution,Deployment->CMake"
4. Add environment variable ```IDF_PATH``` with value ```/home/$USER/esp/esp-idf``` (replace $USER with your username)
5. In the list of variables search ```PATH``` & copy the value
6. Add environment variable ```PATH``` with value
```/home/$USER/esp/esp-idf/components/esptool_py/esptool:/home/$USER/esp/esp-idf/components/espcoredump:/home/$USER/esp/esp-idf/components/partition_table:/home/$USER/esp/esp-idf/components/app_update:/home/$USER/.espressif/tools/xtensa-esp32-elf/esp-2021r2-8.4.0/xtensa-esp32-elf/bin:/home/$USER/.espressif/tools/esp32ulp-elf/2.28.51-esp-20191205/esp32ulp-elf-binutils/bin:/home/$USER/.espressif/tools/openocd-esp32/v0.11.0-esp32-20211220/openocd-esp32/bin:/home/$USER/.espressif/python_env/idf5.0_py3.10_env/bin:/home/$USER/esp/esp-idf/tools:``` (replace $USER with your username) and concatenate it with the value you copied
