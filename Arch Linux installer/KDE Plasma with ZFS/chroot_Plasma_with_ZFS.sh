 #!/bin/bash
set -e
set -o pipefail

sed -i -e "/ParallelDownloads/s/^#//" /etc/pacman.conf
sed -i '/^#\[multilib\]/{N;s/#//g}' /etc/pacman.conf
sed -i -e "/include\ \"\/usr\/share\/nano\/\*\.nanorc\"/s/^#//" /etc/nanorc

sed -i -e "/en_US.UTF-8 UTF-8/s/^#//" /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/Europe/Brussels /etc/localtime
hwclock --systohc
echo LANG=en_US.UTF-8 >> locale.conf
echo KEYMAP=be-latin1 >> /etc/vconsole.conf
echo atlas >> /etc/hostname

echo "xrandr --output DisplayPort-0 --mode 2560x1440 --refresh 120.00"  >> /usr/share/sddm/scripts/Xsetup

rm -f /etc/zfs/zpool.cache
touch /etc/zfs/zpool.cache
chmod a-w /etc/zfs/zpool.cache
chattr +i /etc/zfs/zpool.cache

mkinitcpio -P

#GRUB setup
echo 'export ZPOOL_VDEV_NAME_PATH=YES' >> /etc/profile.d/zpool_vdev_name_path.sh
source /etc/profile.d/zpool_vdev_name_path.sh

## GRUB fails to detect rpool name, hard code as "rpool"
sed -i "s|rpool=.*|rpool=rpool|"  /etc/grub.d/10_linux

echo GRUB_CMDLINE_LINUX=\"zfs_import_dir=/dev/disk/by-id/\" >> /etc/default/grub
sed -i "s/\(GRUB_TIMEOUT *= *\).*/\10/" /etc/default/grub
echo "GRUB_HIDDEN_TIMEOUT=0" >>  /etc/default/grub

grub-install --target x86_64-efi --efi-directory \
    /boot/efi --bootloader-id arch --removable

grub-mkconfig -o /boot/grub/grub.cfg

sleep 1

#Setting root password
echo root:$1 | chpasswd

#create user & set password
useradd -G audio,video,wheel,storage atlas
chown atlas:atlas /home/atlas
echo '%wheel ALL=(ALL:ALL) ALL' | sudo EDITOR='tee -a' visudo
echo atlas:$1 | chpasswd

#install yay
cd /home/atlas/
pacman -S --needed git base-devel --noconfirm && pacman -S go --noconfirm &&  sudo -u atlas git clone https://aur.archlinux.org/yay.git && cd yay && sudo -u atlas makepkg -si --noconfirm

#install & configure ohmyzsh
cd /home/atlas/
sudo -u atlas sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
sed -i "s/\(plugins *= *\).*/\1\(git\ sudo\)/" /home/atlas/.zshrc
echo "source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh" >> /home/atlas/.zshrc
sudo -u atlas git clone https://aur.archlinux.org/zsh-autosuggestions-git.git
cd zsh-autosuggestions-git
sudo -u atlas makepkg -si --noconfirm

#install tools for gaming
pacman -S steam lib32-gamemode gamemode lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader --noconfirm
#yay -S mangohud lib32-mangohud --noconfirm

#cleanup
cd /home/atlas
rm -r yay
rm -r zsh-autosuggestions-git

#set default shell for user
usermod --shell /bin/zsh atlas



ufw enable
ufw default deny

#Enable services
systemctl enable sddm
systemctl enable NetworkManager
systemctl enable zfs-scrub-monthly@rpool.timer
systemctl enable zfs-scrub-monthly@bpool.timer
systemctl enable zfs-trim@rpool.timer
systemctl enable zfs-trim@bpool.timer
systemctl enable ufw

exit
