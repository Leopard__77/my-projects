#!/bin/bash
set -e 
set -o pipefail

TXTCOLOR='\e[0;36m'
NRMLTXT='\e[0;37m'

function printText(){
  echo -e "${TXTCOLOR}$1${NRMLTXT}"
}

loadkeys be-latin1
if [ -z "$DISK" ]; 
	then echo "env DISK not set";
        exit; 
else 
	echo "DISK is set to '$DISK'"; fi

if [ -z "$INST_PARTSIZE_RPOOL" ]; 
	then echo "env INST_PARTSIZE_RPOOL not set";
	exit; 
else 
	echo "DISK is set to '$INST_PARTSIZE_RPOOL'"; fi

if [ "$(cat /proc/cpuinfo | grep AuthenticAMD -c)" != "0" ]; then
    MICROCODE=amd-ucode
elif ["$(cat /proc/cpuinfo | grep GenuineIntel -c)" != "0"]; then
    MICROCODE=intel-ucode
else
    printText "CPU was not detected..."
    exit 1
  fi

echo -n Password: 
read -s password

sed -i -e "/ParallelDownloads/s/^#//" /etc/pacman.conf
sed -i '/^#\[multilib\]/{N;s/#//g}' /etc/pacman.conf

curl -L https://archzfs.com/archzfs.gpg |  pacman-key -a -
pacman-key --lsign-key $(curl -L https://git.io/JsfVS)
curl -L https://git.io/Jsfw2 > /etc/pacman.d/mirrorlist-archzfs

tee -a /etc/pacman.conf <<- 'EOF'

#[archzfs-testing]
#Include = /etc/pacman.d/mirrorlist-archzfs

[archzfs]
Include = /etc/pacman.d/mirrorlist-archzfs
EOF

wipefs --all $DISK
sgdisk --zap-all $DISK
sgdisk     -n1:1M:+512M   -t1:EF00 $DISK
sgdisk     -n2:0:+1G      -t2:BF01 $DISK
sgdisk     -n3:0:+${INST_PARTSIZE_RPOOL}G -t3:BF00 $DISK

sleep 5

zpool create -f \
    -o ashift=12 \
    -o autotrim=on -d \
    -o cachefile=/etc/zfs/zpool.cache \
    -o feature@async_destroy=enabled \
    -o feature@bookmarks=enabled \
    -o feature@embedded_data=enabled \
    -o feature@empty_bpobj=enabled \
    -o feature@enabled_txg=enabled \
    -o feature@extensible_dataset=enabled \
    -o feature@filesystem_limits=enabled \
    -o feature@hole_birth=enabled \
    -o feature@large_blocks=enabled \
    -o feature@livelist=enabled \
    -o feature@lz4_compress=enabled \
    -o feature@spacemap_histogram=enabled \
    -o feature@zpool_checkpoint=enabled \
    -O devices=off \
    -O acltype=posixacl -O xattr=sa \
    -O compression=lz4 \
    -O normalization=formD \
    -O relatime=on \
    -O canmount=off -O mountpoint=/boot -R /mnt \
    bpool ${DISK}-part2

zpool create -f \
    -o ashift=12 \
    -o autotrim=on \
    -O acltype=posixacl -O xattr=sa -O dnodesize=auto \
    -O compression=lz4 \
    -O normalization=formD \
    -O relatime=on \
    -O canmount=off -O mountpoint=/ -R /mnt \
    rpool ${DISK}-part3

zfs create \
 -o canmount=off \
 -o mountpoint=none \
 rpool/archlinux

zfs create -o canmount=on -o mountpoint=/     rpool/archlinux/root
zfs create -o canmount=on -o mountpoint=/home rpool/archlinux/home
zfs create -o canmount=off -o mountpoint=none rpool/archlinux/home/atlas
zfs create -o canmount=on -o mountpoint=/home/atlas/Documents -o copies=2 rpool/archlinux/home/atlas/Documents
zfs create -o canmount=on -o mountpoint=/home/atlas/Pictures -o copies=2 rpool/archlinux/home/atlas/Pictures
zfs create -o canmount=on -o mountpoint=/home/atlas/Scripts -o copies=2 rpool/archlinux/home/atlas/Scripts
zfs create -o canmount=on -o mountpoint=/home/atlas/Games -o compression=lz4 -o recordsize=1M -o casesensitivity=insensitive  rpool/archlinux/home/atlas/Games
zfs create -o canmount=off -o mountpoint=/var  rpool/archlinux/var
zfs create -o canmount=on  rpool/archlinux/var/lib
zfs create -o canmount=on  rpool/archlinux/var/log

zfs create -o canmount=off -o mountpoint=none bpool/archlinux
zfs create -o canmount=on -o mountpoint=/boot bpool/archlinux/root

mkfs.fat -F 32  -n EFI ${DISK}-part1
sleep 5
mkdir -p /mnt/boot/efi
mount ${DISK}-part1 /mnt/boot/efi

pacstrap /mnt base linux linux-firmware grub efibootmgr nano networkmanager git dkms linux-headers zfs-dkms zfs-utils noto-fonts xorg-server plasma-meta firefox konsole dolphin spectacle sudo gparted gimp htop ark signal-desktop openssh nextcloud-client ufw zsh thunderbird $MICROCODE

echo $(echo $DISK | cut -f1 -d\ )-part1 /boot/efi vfat noauto,umask=0022,fmask=0022,dmask=0022 0 1 >> /mnt/etc/fstab

mv /mnt/etc/mkinitcpio.conf /mnt/etc/mkinitcpio.conf.original
tee /mnt/etc/mkinitcpio.conf <<EOF
HOOKS=(base udev autodetect modconf block keyboard zfs filesystems)
EOF

zgenhostid -f -o /mnt/etc/hostid

systemctl enable zfs-import-scan.service zfs-mount zfs-import.target zfs-zed zfs.target --root=/mnt

curl -L https://archzfs.com/archzfs.gpg |  pacman-key -a - --gpgdir /mnt/etc/pacman.d/gnupg
pacman-key --lsign-key --gpgdir /mnt/etc/pacman.d/gnupg $(curl -L https://git.io/JsfVS)
curl -L https://git.io/Jsfw2 > /mnt/etc/pacman.d/mirrorlist-archzfs

tee -a /mnt/etc/pacman.conf <<- 'EOF'

#[archzfs-testing]
#Include = /etc/pacman.d/mirrorlist-archzfs

[archzfs]
Include = /etc/pacman.d/mirrorlist-archzfs
EOF

sleep 1

cp ./chroot_desktop.sh /mnt
cp ./configs/00-keyboard.conf /mnt/etc/X11/xorg.conf.d/
cp ./configs/20-amdgpu.conf /mnt/etc/X11/xorg.conf.d/
cp ./configs/zfs-trim\@.timer /mnt/etc/systemd/system/
cp ./configs/zfs-trim\@.service /mnt/etc/systemd/system/
cp ./configs/gamemode.ini /mnt/etc/


arch-chroot /mnt /chroot_desktop.sh $password

sleep 5

mkdir -p /mnt/etc/pacman.d/hooks/
cp ./configs/snapshot.hook /mnt/etc/pacman.d/hooks/

rm /mnt/chroot_desktop.sh
umount -Rl /mnt
zpool export -a

