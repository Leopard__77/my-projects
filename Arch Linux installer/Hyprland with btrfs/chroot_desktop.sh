 #!/bin/bash
set -e
set -o pipefail

sed -i -e "/ParallelDownloads/s/^#//" /etc/pacman.conf
sed -i '/^#\[multilib\]/{N;s/#//g}' /etc/pacman.conf
sed -i -e "/include\ \"\/usr\/share\/nano\/\*\.nanorc\"/s/^#//" /etc/nanorc

sed -i -e "/en_US.UTF-8 UTF-8/s/^#//" /etc/locale.gen
locale-gen

ln -sf /usr/share/zoneinfo/Europe/Brussels /etc/localtime
hwclock --systohc
echo LANG=en_US.UTF-8 >> locale.conf
echo KEYMAP=be-latin1 >> /etc/vconsole.conf
echo atlas >> /etc/hostname

#Setting root password
echo root:$1 | chpasswd

#create user & set password
useradd -m -G audio,video,wheel,storage atlas
chown -R atlas:atlas /home/atlas
echo '%wheel ALL=(ALL:ALL) ALL' | sudo EDITOR='tee -a' visudo
echo atlas:$1 | chpasswd

#install yay
cd /home/atlas/
pacman -S --needed git base-devel --noconfirm && pacman -S go --noconfirm &&  sudo -u atlas git clone https://aur.archlinux.org/yay.git && cd yay && sudo -u atlas makepkg -si --noconfirm


pacman -S nemo alacritty hyprland ttf-font-awesome noto-fonts pipewire wireplumber qt5-wayland qt6-wayland polkit-kde-agent xdg-desktop-portal-hyprland zsh firefox libva-mesa-driver libva-utils --noconfirm

sudo -u atlas yay -S rofi-lbonn-wayland-git waybar-hyprland-git sddm-git 


#install & configure ohmyzsh
cd /home/atlas/
sudo -u atlas sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
sed -i "s/\(plugins *= *\).*/\1\(git\ sudo\)/" /home/atlas/.zshrc
echo "source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh" >> /home/atlas/.zshrc
sudo -u atlas git clone https://aur.archlinux.org/zsh-autosuggestions-git.git
cd zsh-autosuggestions-git
sudo -u atlas makepkg -si --noconfirm

#install tools for gaming
pacman -S lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader --no-confirm

## wine + dependencies
pacman -S wine-staging winetricks
pacman -S giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo libxcomposite lib32-libxcomposite libxinerama lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader cups samba dosbox

## tools
pacman -S mangohud lib32-mangohud gamemode lib32-gamemode

#cleanup
cd /home/atlas
rm -r yay
rm -r zsh-autosuggestions-git

#set default shell for user
usermod --shell /bin/zsh atlas

#Enable services
systemctl enable sddm
systemctl enable NetworkManager

chown -R atlas:atlas /home/atlas
exit
