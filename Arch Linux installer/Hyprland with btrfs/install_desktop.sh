#!/bin/bash
set -e 
set -o pipefail

TXTCOLOR='\e[0;36m'
NRMLTXT='\e[0;37m'

function printText(){
  echo -e "${TXTCOLOR}$1${NRMLTXT}"
}

loadkeys be-latin1
if [ -z "$DISK" ]; 
	then echo "env DISK not set";
        exit; 
else 
	echo "DISK is set to '$DISK'"; fi

#if [ -z "$ROOT_SIZE" ]; 
#	then echo "env ROOT_SIZE not set";
#	exit; 
#else 
#	echo "DISK is set to '$ROOT_SIZE'"; fi

#if [ "$(cat /proc/cpuinfo | grep AuthenticAMD -c)" != "0" ]; then
#    MICROCODE=amd-ucode
#elif ["$(cat /proc/cpuinfo | grep GenuineIntel -c)" != "0"]; then
#    MICROCODE=intel-ucode
#else
#    printText "CPU was not detected..."
#    exit 1
#  fi

echo -n Password: 
read -s password

sed -i -e "/ParallelDownloads/s/^#//" /etc/pacman.conf
sed -i '/^#\[multilib\]/{N;s/#//g}' /etc/pacman.conf

timedatectl

sed -e 's/\s*\([\+0-9a-zA-Z]*\).*/\1/' << EOF | fdisk ${DISK}
	o # clear the in memory partition table
	g # create gpt
	n # new partition
	1 # partition number 1
	 # default - start at beginning of disk 
	+256M # 256 MB boot parttion
	n # new partition
	2 # partion number 2
	# default, start immediately after preceding partition 
	+200G
	t
	1
	1
	t
	2
	23
	p
	w # write the partition table
	q # and we're done
EOF

sleep 5

mkfs.fat -F 32 "${DISK}"-part1
mkfs.btrfs -L Alexandria "${DISK}"-part2 -f

#mounts
mount "${DISK}"-part2 /mnt
cd /mnt
btrfs subvolume create @Arch-Linux-HyprLand
btrfs subvolume create @Arch-Linux-HyprLand/root
btrfs subvolume create @Arch-Linux-HyprLand/home
btrfs subvolume create @Arch-Linux-HyprLand/snapshots

cd /root
umount /mnt
mount -o subvol=@Arch-Linux-HyprLand/root,compress=lzo "${DISK}"-part2 /mnt
mkdir /mnt/{home,boot}
mount -o subvol=@Arch-Linux-HyprLand/home,compress=lzo "${DISK}"-part2 /mnt/home

mkdir -p /mnt/boot/EFI
mount "${DISK}"-part1 /mnt/boot/EFI

# install basics

pacstrap -K /mnt base linux linux-firmware amd-ucode networkmanager nano sudo git efibootmgr btrfs-progs

genfstab -U /mnt >> /mnt/etc/fstab

cd /root
cp chroot_desktop.sh /mnt

arch-chroot /mnt /chroot_desktop.sh $password

sleep 5

USER_CONFIG_PATH = /mnt/home/atlas/.config/
mkdir -p $USER_CONFIG_PATH

cd configs/user
cp -r * $USER_CONFIG_PATH

cd /root/configs/bootloader
cp -r BOOT /mnt/boot/EFI

cd /root

rm /mnt/chroot_desktop.sh
umount -Rl /mnt

